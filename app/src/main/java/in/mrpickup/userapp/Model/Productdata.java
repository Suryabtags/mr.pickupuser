package in.mrpickup.userapp.Model;


import android.os.Parcel;
import android.os.Parcelable;

public class Productdata implements Parcelable {

    public static final Creator<Productdata> CREATOR = new Creator<Productdata>() {
        @Override
        public Productdata createFromParcel(Parcel in) {
            return new Productdata(in);
        }

        @Override
        public Productdata[] newArray(int size) {
            return new Productdata[size];
        }
    };
    String data;
    String id, mUnitId1;
    String subdata, mQuantity, mUnitId;

    public Productdata(String data, String subData, String Quantity, String Unitname, String id, String mUnitId) {
        this.data = data;
        this.id = id;
        this.subdata = subData;
        this.mQuantity = Quantity;
        this.mUnitId = Unitname;
        this.mUnitId1 = mUnitId;
    }

    public Productdata(Parcel in) {
        data = in.readString();
        id = in.readString();
        subdata = in.readString();
        mQuantity = in.readString();
        mUnitId = in.readString();
        mUnitId1 = in.readString();
    }

    public String getdata() {
        return data;
    }

    public void setdata(String data2) {
        data = data2;
    }

    public String getmId() {
        return id;
    }

    public void setmId(String mId1) {
        this.id = mId1;
    }

    public String getSubdata() {
        return subdata;
    }

    public void setSubdata(String subdata) {
        this.subdata = subdata;
    }

    public String getmUnitId1() {
        return mUnitId1;
    }

    public void setmUnitId1(String mUnitId1) {
        this.mUnitId1 = mUnitId1;
    }

    public String getmQuantity() {
        return mQuantity;
    }

    public void setmQuantity(String mQuantity) {
        this.mQuantity = mQuantity;
    }

    public String getmUnitId() {
        return mUnitId;
    }

    public void setmUnitId(String mUnitId) {
        this.mUnitId = mUnitId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(data);
        dest.writeString(id);
        dest.writeString(subdata);
        dest.writeString(mQuantity);
        dest.writeString(mUnitId);
        dest.writeString(mUnitId1);
    }
}

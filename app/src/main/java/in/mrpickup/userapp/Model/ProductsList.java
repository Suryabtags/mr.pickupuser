package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ProductsList {


    @SerializedName("categories")
    private ArrayList<CategoryList> categories = new ArrayList<>();

    public ArrayList<CategoryList> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoryList> categories) {
        this.categories = categories;
    }
}

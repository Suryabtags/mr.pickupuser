package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 1/4/2018.
 */

public class ImageListItem {

    @SerializedName("id")
    private int mId;

    @SerializedName("image")
    private String mImage;
    @SerializedName("type")
    private String mType;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmImage() {
        return mImage;
    }

    public void setmImage(String mImage) {
        this.mImage = mImage;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }
}

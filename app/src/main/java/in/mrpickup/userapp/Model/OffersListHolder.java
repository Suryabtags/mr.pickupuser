package in.mrpickup.userapp.Model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.mrpickup.userapp.R;


public class OffersListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextviewOffersName, mTextViewOfferPrice, mTextviewOfferReason, mTextViewOfferType;

    public OffersListHolder(View itemView) {
        super(itemView);
        mTextviewOffersName = itemView.findViewById(R.id.Offer_Name);
        mTextViewOfferPrice = itemView.findViewById(R.id.Offer_Price);
        mTextviewOfferReason = itemView.findViewById(R.id.Offer_Reason);
        mTextViewOfferType = itemView.findViewById(R.id.type);

    }

    @Override
    public void onClick(View view) {

    }
}

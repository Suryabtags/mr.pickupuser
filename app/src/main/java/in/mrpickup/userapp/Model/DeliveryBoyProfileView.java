package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 10/21/2017.
 */

public class DeliveryBoyProfileView {


    @SerializedName("employee_deatils")
    private Employee employee;


    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public class Employee {


        @SerializedName("id")
        private int mId;
        @SerializedName("first_name")
        private String mFirstName;
        @SerializedName("username")
        private String mUsername;

        @SerializedName("email")
        private String mEmail;
        @SerializedName("userprofile")
        private UserProfile userprofile;
        @SerializedName("useraddress")
        private UserAddress useraddress;

        public int getmId() {
            return mId;
        }

        public void setmId(int mId) {
            this.mId = mId;
        }

        public String getmFirstName() {
            return mFirstName;
        }

        public void setmFirstName(String mFirstName) {
            this.mFirstName = mFirstName;
        }

        public String getmUsername() {
            return mUsername;
        }

        public void setmUsername(String mUsername) {
            this.mUsername = mUsername;
        }

        public String getmEmail() {
            return mEmail;
        }

        public void setmEmail(String mEmail) {
            this.mEmail = mEmail;
        }

        public UserProfile getUserprofile() {
            return userprofile;
        }

        public void setUserprofile(UserProfile userprofile) {
            this.userprofile = userprofile;
        }

        public UserAddress getUseraddress() {
            return useraddress;
        }

        public void setUseraddress(UserAddress useraddress) {
            this.useraddress = useraddress;
        }
    }

    public class UserProfile {
        @SerializedName("id")
        private String id;

        @SerializedName("profile_pic")
        private String profile_pic;
        @SerializedName("dob")
        private String dob;
        @SerializedName("rating")
        private String mRatings;

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getmRatings() {
            return mRatings;
        }

        public void setmRatings(String mRatings) {
            this.mRatings = mRatings;
        }
    }

    public class UserAddress {

        @SerializedName("id")
        private String id;

        @SerializedName("street")
        private String street;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("country")
        private String country;
        @SerializedName("area")
        private String mArea;
        @SerializedName("zipcode")
        private String mZipcode;

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getmArea() {
            return mArea;
        }

        public void setmArea(String mArea) {
            this.mArea = mArea;
        }

        public String getmZipcode() {
            return mZipcode;
        }

        public void setmZipcode(String mZipcode) {
            this.mZipcode = mZipcode;
        }
    }


}

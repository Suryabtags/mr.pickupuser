package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 11/18/2017
 */

public class FcmToken {

    @SerializedName("client")
    private String mClient;
    @SerializedName("device_token")
    private String mToken;

    public FcmToken(String client, String token) {
        mClient = client;
        mToken = token;
    }

    public String getClient() {
        return mClient;
    }

    public void setClient(String client) {
        mClient = client;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }
}

package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class CategoryList {
    @SerializedName("category")
    private Category mCategory;

    @SerializedName("products")
    private ArrayList<ProductName> Products = new ArrayList<>();

    public ArrayList<ProductName> getProducts() {
        return Products;
    }

    public void setProducts(ArrayList<ProductName> products) {
        Products = products;
    }

    public Category getmCategory() {
        return mCategory;
    }

    public void setmCategory(Category mCategory) {
        this.mCategory = mCategory;
    }

    public class Category {

        @SerializedName("name")
        private String nName;

        @SerializedName("id")
        private String mId;

        public String getnName() {
            return nName;
        }

        public void setnName(String nName) {
            this.nName = nName;
        }

        public String getmId() {
            return mId;
        }

        public void setmId(String mId) {
            this.mId = mId;
        }
    }
}

package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 10/21/2017.
 */

public class OffersList {


    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mOfferName;
    @SerializedName("from_price")
    private int mFromPrice;
    @SerializedName("to_price")
    private int mToPrice;
    @SerializedName("offer_km")
    private int offer_km;
    @SerializedName("offer_available")
    private String mOffersAvailable;
    @SerializedName("is_active")
    private boolean mIsActive;

    @SerializedName("create_on")
    private String mCreatedOn;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmOfferName() {
        return mOfferName;
    }

    public void setmOfferName(String mOfferName) {
        this.mOfferName = mOfferName;
    }

    public int getmFromPrice() {
        return mFromPrice;
    }

    public void setmFromPrice(int mFromPrice) {
        this.mFromPrice = mFromPrice;
    }

    public int getmToPrice() {
        return mToPrice;
    }

    public void setmToPrice(int mToPrice) {
        this.mToPrice = mToPrice;
    }

    public int getOffer_km() {
        return offer_km;
    }

    public void setOffer_km(int offer_km) {
        this.offer_km = offer_km;
    }

    public String getmOffersAvailable() {
        return mOffersAvailable;
    }

    public void setmOffersAvailable(String mOffersAvailable) {
        this.mOffersAvailable = mOffersAvailable;
    }

    public boolean ismIsActive() {
        return mIsActive;
    }

    public void setmIsActive(boolean mIsActive) {
        this.mIsActive = mIsActive;
    }

    public String getmCreatedOn() {
        return mCreatedOn;
    }

    public void setmCreatedOn(String mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }
}

package in.mrpickup.userapp.Model;

/**
 * Created by Bobby on 28/06/17
 */

public class PaymentSuccessEvent {

    private final String mMessage;

    public PaymentSuccessEvent(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }
}

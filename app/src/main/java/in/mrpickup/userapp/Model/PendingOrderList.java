package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by admin on 1/4/2018.
 */

public class PendingOrderList {


    @SerializedName("prev_url")
    private int mPrevUrl;
    @SerializedName("count")
    private int count;
    @SerializedName("next_url")
    private String next_url;

    @SerializedName("page_size")
    private int page_size;
    @SerializedName("results")
    private ArrayList<PendingOrderListItem> mResult = new ArrayList<>();

    public int getmPrevUrl() {
        return mPrevUrl;
    }

    public void setmPrevUrl(int mPrevUrl) {
        this.mPrevUrl = mPrevUrl;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNext_url() {
        return next_url;
    }

    public void setNext_url(String next_url) {
        this.next_url = next_url;
    }

    public ArrayList<PendingOrderListItem> getmResult() {
        return mResult;
    }

    public void setmResult(ArrayList<PendingOrderListItem> mResult) {
        this.mResult = mResult;
    }


    public int getPage_size() {
        return page_size;
    }

    public void setPage_size(int page_size) {
        this.page_size = page_size;
    }
}

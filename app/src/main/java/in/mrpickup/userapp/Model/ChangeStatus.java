package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 1/6/2018.
 */

public class ChangeStatus {


    @SerializedName("status")
    private String mStatus;


    public ChangeStatus(String status) {
        mStatus = status;

    }


    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }
}

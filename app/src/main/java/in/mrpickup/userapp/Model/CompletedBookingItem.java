package in.mrpickup.userapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;


public class CompletedBookingItem implements Serializable, Parcelable {


    public static final Creator<CompletedBookingItem> CREATOR = new Creator<CompletedBookingItem>() {
        @Override
        public CompletedBookingItem createFromParcel(Parcel in) {
            return new CompletedBookingItem(in);
        }

        @Override
        public CompletedBookingItem[] newArray(int size) {
            return new CompletedBookingItem[size];
        }
    };
    private int mId, mStatus;
    private double mCost;
    private String mName, mPhone, mPrice, mProfilePic, mAddress, mDestAddr,
            mDate, empId, mPname, mDname, mStoreName, mPaymentType, mDescription, Distance, deliverycost,
            mProductWeight, deliveryType;
    private float mrate;
    private double mProductCost, WaitingCost;
    private boolean picktype, mIsPaid;
    private int Quantity, Units, subcatId, catId;
    private String SubCatName, CatName;
    private String pickImage, delImages;

    public CompletedBookingItem(int id, float rate, String name, String phone, String address, String destAddr, int status,
                                String price, String date, String profile, String empid, String pname, String dname,
                                String storename, String mpaymentType, double productcost,
                                boolean type, boolean is_paid, String dtype, String Desc, String mDistance,
                                String mdeliverycost, String productweight, int mquantity, int units, String subcatname, int subcatid,
                                String catname, int catid, String pimage, String dimage, double waitingcost) {
        mId = id;
        mName = name;
        mPhone = phone;
        mAddress = address;
        mDestAddr = destAddr;
        mStatus = status;
        mPrice = price;
        mrate = rate;
        mDate = date;
        mProfilePic = profile;
        empId = empid;
        mPname = pname;
        mDname = dname;
        mStoreName = storename;
        mPaymentType = mpaymentType;
        mProductCost = productcost;
        picktype = type;
        mIsPaid = is_paid;
        deliveryType = dtype;
        mDescription = Desc;
        Distance = mDistance;
        deliverycost = mdeliverycost;
        mProductWeight = productweight;
        Quantity = mquantity;
        Units = units;
        SubCatName = subcatname;
        subcatId = subcatid;
        catId = catid;
        CatName = catname;
        pickImage = pimage;
        delImages = dimage;
        WaitingCost = waitingcost;
    }

    protected CompletedBookingItem(Parcel in) {
        mId = in.readInt();
        mStatus = in.readInt();
        mCost = in.readDouble();
        mName = in.readString();
        mPhone = in.readString();
        mPrice = in.readString();
        mProfilePic = in.readString();
        mAddress = in.readString();
        mDestAddr = in.readString();
        mDate = in.readString();
        empId = in.readString();
        mPname = in.readString();
        mDname = in.readString();
        mStoreName = in.readString();
        mPaymentType = in.readString();
        mDescription = in.readString();
        Distance = in.readString();
        deliverycost = in.readString();
        mProductWeight = in.readString();
        deliveryType = in.readString();
        mrate = in.readFloat();
        mProductCost = in.readDouble();
        WaitingCost = in.readDouble();
        picktype = in.readByte() != 0;
        mIsPaid = in.readByte() != 0;
        Quantity = in.readInt();
        Units = in.readInt();
        subcatId = in.readInt();
        catId = in.readInt();
        SubCatName = in.readString();
        CatName = in.readString();
        pickImage = in.readString();
        delImages = in.readString();
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public double getmCost() {
        return mCost;
    }

    public void setmCost(double mCost) {
        this.mCost = mCost;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getmPrice() {
        return mPrice;
    }

    public void setmPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public String getmProfilePic() {
        return mProfilePic;
    }

    public void setmProfilePic(String mProfilePic) {
        this.mProfilePic = mProfilePic;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public float getMrate() {
        return mrate;
    }

    public void setMrate(float mrate) {
        this.mrate = mrate;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public int getUnits() {
        return Units;
    }

    public void setUnits(int units) {
        Units = units;
    }

    public int getSubcatId() {
        return subcatId;
    }

    public void setSubcatId(int subcatId) {
        this.subcatId = subcatId;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getSubCatName() {
        return SubCatName;
    }

    public void setSubCatName(String subCatName) {
        SubCatName = subCatName;
    }

    public String getCatName() {
        return CatName;
    }

    public void setCatName(String catName) {
        CatName = catName;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getDeliverycost() {
        return deliverycost;
    }

    public void setDeliverycost(String deliverycost) {
        this.deliverycost = deliverycost;
    }

    public String getmProductWeight() {
        return mProductWeight;
    }

    public void setmProductWeight(String mProductWeight) {
        this.mProductWeight = mProductWeight;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public boolean ismIsPaid() {
        return mIsPaid;
    }

    public void setmIsPaid(boolean mIsPaid) {
        this.mIsPaid = mIsPaid;
    }

    public boolean isPicktype() {
        return picktype;
    }

    public void setPicktype(boolean picktype) {
        this.picktype = picktype;
    }

    public double getmProductCost() {
        return mProductCost;
    }

    public void setmProductCost(double mProductCost) {
        this.mProductCost = mProductCost;
    }

    public String getmPaymentType() {
        return mPaymentType;
    }

    public void setmPaymentType(String mPaymentType) {
        this.mPaymentType = mPaymentType;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getmStoreName() {
        return mStoreName;
    }

    public void setmStoreName(String mStoreName) {
        this.mStoreName = mStoreName;
    }

    public String getmPname() {
        return mPname;
    }

    public void setmPname(String mPname) {
        this.mPname = mPname;
    }

    public String getmDname() {
        return mDname;
    }

    public void setmDname(String mDname) {
        this.mDname = mDname;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }


    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public double getCost() {
        return mCost;
    }

    public void setCost(double cost) {
        mCost = cost;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getProfilePic() {
        return mProfilePic;
    }

    public void setProfilePic(String profilePic) {
        mProfilePic = profilePic;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }


    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {

        mDate = date;
    }

    public String getmDestAddr() {
        return mDestAddr;
    }

    public void setmDestAddr(String mDestAddr) {
        this.mDestAddr = mDestAddr;
    }


    public int getmStatus() {
        return mStatus;
    }

    public void setmStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public float getmrate() {
        return mrate;
    }

    public void setmrate(float mrate) {
        this.mrate = mrate;
    }



    public String getPickImage() {
        return pickImage;
    }

    public void setPickImage(String pickImage) {
        this.pickImage = pickImage;
    }

    public String getDelImages() {
        return delImages;
    }

    public void setDelImages(String delImages) {
        this.delImages = delImages;
    }

    public double getWaitingCost() {
        return WaitingCost;
    }

    public void setWaitingCost(double waitingCost) {
        WaitingCost = waitingCost;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeInt(mStatus);
        dest.writeDouble(mCost);
        dest.writeString(mName);
        dest.writeString(mPhone);
        dest.writeString(mPrice);
        dest.writeString(mProfilePic);
        dest.writeString(mAddress);
        dest.writeString(mDestAddr);
        dest.writeString(mDate);
        dest.writeString(empId);
        dest.writeString(mPname);
        dest.writeString(mDname);
        dest.writeString(mStoreName);
        dest.writeString(mPaymentType);
        dest.writeString(mDescription);
        dest.writeString(Distance);
        dest.writeString(deliverycost);
        dest.writeString(mProductWeight);
        dest.writeString(deliveryType);
        dest.writeFloat(mrate);
        dest.writeDouble(mProductCost);
        dest.writeDouble(WaitingCost);
        dest.writeByte((byte) (picktype ? 1 : 0));
        dest.writeByte((byte) (mIsPaid ? 1 : 0));
        dest.writeInt(Quantity);
        dest.writeInt(Units);
        dest.writeInt(subcatId);
        dest.writeInt(catId);
        dest.writeString(SubCatName);
        dest.writeString(CatName);
        dest.writeString(pickImage);
        dest.writeString(delImages);
    }
}

package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 1/3/2018.
 */

public class Rates {


    @SerializedName("id")
    private int mId;
    @SerializedName("from_weight")
    private int mFromWeight;
    @SerializedName("to_weight")
    private int mToWeight;
    @SerializedName("distance_cost")
    private int mDistanceCost;

    @SerializedName("extra_km")
    private double mExtraKm;
    @SerializedName("extra_kg")
    private double mExtraKg;
    @SerializedName("is_active")
    private boolean mIsActive;
    @SerializedName("create_on")
    private String mCreatedOn;
    @SerializedName("delivery_type")
    private String mDeliveryType;
    @SerializedName("max_km")
    private String mMaximumKilometer;
    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getmFromWeight() {
        return mFromWeight;
    }

    public void setmFromWeight(int mFromWeight) {
        this.mFromWeight = mFromWeight;
    }

    public int getmToWeight() {
        return mToWeight;
    }

    public void setmToWeight(int mToWeight) {
        this.mToWeight = mToWeight;
    }

    public int getmDistanceCost() {
        return mDistanceCost;
    }

    public void setmDistanceCost(int mDistanceCost) {
        this.mDistanceCost = mDistanceCost;
    }

    public double getmExtraKm() {
        return mExtraKm;
    }

    public void setmExtraKm(double mExtraKm) {
        this.mExtraKm = mExtraKm;
    }

    public double getmExtraKg() {
        return mExtraKg;
    }

    public void setmExtraKg(float mExtraKg) {
        this.mExtraKg = mExtraKg;
    }

    public boolean ismIsActive() {
        return mIsActive;
    }

    public void setmIsActive(boolean mIsActive) {
        this.mIsActive = mIsActive;
    }

    public String getmCreatedOn() {
        return mCreatedOn;
    }

    public void setmCreatedOn(String mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }


    public String getmDeliveryType() {
        return mDeliveryType;
    }

    public void setmDeliveryType(String mDeliveryType) {
        this.mDeliveryType = mDeliveryType;
    }


    public String getmMaximumKilometer() {
        return mMaximumKilometer;
    }

    public void setmMaximumKilometer(String mMaximumKilometer) {
        this.mMaximumKilometer = mMaximumKilometer;
    }
}

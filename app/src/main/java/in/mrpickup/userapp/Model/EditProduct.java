package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by admin on 3/10/2018.
 */

public class EditProduct {

    @SerializedName("products")
    private ArrayList<ProductsEdit> mProducts = new ArrayList<>();
    @SerializedName("deleted_product")
    private ArrayList<Integer> deleted_product = new ArrayList<>();


    public EditProduct(ArrayList<ProductsEdit> Products) {
        mProducts = Products;
    }

    public ArrayList<ProductsEdit> getmProducts() {
        return mProducts;
    }

    public void setmProducts(ArrayList<ProductsEdit> mProducts) {
        this.mProducts = mProducts;
    }

    public ArrayList<Integer> getDeleted_product() {
        return deleted_product;
    }

    public void setDeleted_product(ArrayList<Integer> deleted_product) {
        this.deleted_product = deleted_product;
    }
}

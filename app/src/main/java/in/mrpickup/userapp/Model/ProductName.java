package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/17/2018.
 */

public class ProductName {

    @SerializedName("units")
    private String mUnits;
    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("name")
    private String nName;

    @SerializedName("id")
    private String mId;


    public String getmUnits() {
        return mUnits;
    }

    public void setmUnits(String mUnits) {
        this.mUnits = mUnits;
    }

    public String getmQuantity() {
        return mQuantity;
    }

    public void setmQuantity(String mQuantity) {
        this.mQuantity = mQuantity;
    }

    public String getnName() {
        return nName;
    }

    public void setnName(String nName) {
        this.nName = nName;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }
}

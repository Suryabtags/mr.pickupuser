package in.mrpickup.userapp.Model;


public class PendingBookingItem {


    String deliveryType;
    private int mId, mStatus, mPrice;
    private double mCost;
    private String mName, mPhone, mPicOtp, mDelOtp, mProfilePic,
            mAddress, mDestAddr, mDate, empId, mProductName, mPname, mDname, mStoreName,
            mDescription, Distance, deliverycost, mProductWeight;
    private float mrate;
    private boolean mType;
    private int Quantity, Units, subcatId, catId;
    private String SubCatName, CatName, mOrderIdshow;

    public PendingBookingItem(int id, String name, String phone, String address, String destAddr, int status,
                              String delotp, String picotp, String date, int price, boolean type,
                              String profile, String empid, String Product, String pname, String dname, String storename, int mquantity, int units, String subcatname, int subcatid,
                              String catname, int catid, String dtype, String Desc, String mDistance,
                              String mdeliverycost, String productweight, String orderidshow) {
        mId = id;
        mName = name;
        mPhone = phone;
        mProfilePic = profile;
        mAddress = address;
        mDestAddr = destAddr;
        mStatus = status;
        mDelOtp = delotp;
        mPicOtp = picotp;
        mDate = date;
        mPrice = price;
        mType = type;
        empId = empid;
        mProductName = Product;
        mPname = pname;
        mDname = dname;
        mStoreName = storename;
        Quantity = mquantity;
        Units = units;
        SubCatName = subcatname;
        subcatId = subcatid;
        catId = catid;
        CatName = catname;
        deliveryType = dtype;
        mDescription = Desc;
        Distance = mDistance;
        deliverycost = mdeliverycost;
        mProductWeight = productweight;
        mOrderIdshow = orderidshow;
    }


    public PendingBookingItem(int id, int status, String mdelotp, String picotp,
                              String address, String destination, String date, int price, boolean type,
                              String eId, String mProduct, String pName, String dName, String sname, int mquantity, int units, String subcatname, int subcatid,
                              String catname, int catid, String dtype, String Desc, String mDistance,
                              String mdeliverycost, String productweight, String orderidshow) {
        mId = id;
        mStatus = status;
        mDelOtp = mdelotp;
        mPicOtp = picotp;
        mAddress = address;
        mDestAddr = destination;
        mDate = date;
        mPrice = price;
        mType = type;
        empId = eId;
        mProductName = mProduct;
        mPname = pName;
        mDname = dName;
        mStoreName = sname;
        Quantity = mquantity;
        Units = units;
        SubCatName = subcatname;
        subcatId = subcatid;
        catId = catid;
        CatName = catname;
        deliveryType = dtype;
        mDescription = Desc;
        Distance = mDistance;
        deliverycost = mdeliverycost;
        mProductWeight = productweight;
        mOrderIdshow = orderidshow;
    }

    public PendingBookingItem(int mquantity, int units, String subcatname, int subcatid,
                              String catname, int catid) {
        Quantity = mquantity;
        Units = units;
        SubCatName = subcatname;
        subcatId = subcatid;
        catId = catid;
        CatName = catname;

    }

    public String getmOrderIdshow() {
        return mOrderIdshow;
    }

    public void setmOrderIdshow(String mOrderIdshow) {
        this.mOrderIdshow = mOrderIdshow;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public int getUnits() {
        return Units;
    }

    public void setUnits(int units) {
        Units = units;
    }

    public int getSubcatId() {
        return subcatId;
    }

    public void setSubcatId(int subcatId) {
        this.subcatId = subcatId;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getSubCatName() {
        return SubCatName;
    }

    public void setSubCatName(String subCatName) {
        SubCatName = subCatName;
    }

    public String getCatName() {
        return CatName;
    }

    public void setCatName(String catName) {
        CatName = catName;
    }

    public String getmStoreName() {
        return mStoreName;
    }

    public void setmStoreName(String mStoreName) {
        this.mStoreName = mStoreName;
    }

    public String getmPname() {
        return mPname;
    }

    public void setmPname(String mPname) {
        this.mPname = mPname;
    }

    public String getmDname() {
        return mDname;
    }

    public void setmDname(String mDname) {
        this.mDname = mDname;
    }

    public String getmProductName() {
        return mProductName;
    }

    public void setmProductName(String mProductName) {
        this.mProductName = mProductName;
    }

    public int getPrice() {
        return mPrice;
    }

    public void setPrice(int price) {
        mPrice = price;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }


    public String getmDelOtp() {
        return mDelOtp;
    }

    public void setmDelOtp(String otp) {
        mDelOtp = otp;
    }

    public String getmPicOtp() {
        return mPicOtp;
    }

    public void setmPicOtp(String otp1) {
        mPicOtp = otp1;
    }

    public boolean getmType() {
        return mType;
    }

    public void setmType(boolean type) {
        mType = type;
    }


    public double getCost() {
        return mCost;
    }

    public void setCost(double cost) {
        mCost = cost;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getProfilePic() {
        return mProfilePic;
    }

    public void setProfilePic(String profilePic) {
        mProfilePic = profilePic;
    }


    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }


    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {

        mDate = date;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getmDestAddr() {
        return mDestAddr;
    }

    public void setmDestAddr(String mDestAddr) {
        this.mDestAddr = mDestAddr;
    }

    public int getmStatus() {
        return mStatus;
    }

    public void setmStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public float getmrate() {
        return mrate;
    }

    public void setmrate(float mrate) {
        this.mrate = mrate;
    }


    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }


    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getDeliverycost() {
        return deliverycost;
    }

    public void setDeliverycost(String deliverycost) {
        this.deliverycost = deliverycost;
    }

    public String getmProductWeight() {
        return mProductWeight;
    }

    public void setmProductWeight(String mProductWeight) {
        this.mProductWeight = mProductWeight;
    }


}

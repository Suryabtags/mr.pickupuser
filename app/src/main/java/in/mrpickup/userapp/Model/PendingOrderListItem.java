package in.mrpickup.userapp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import static in.mrpickup.userapp.helper.Utils.getCreatedDate;

/**
 * Created by admin on 1/4/2018.
 */

public class PendingOrderListItem {

    @SerializedName("id")
    public int mId;
    @SerializedName("category")
    public int mCategory;
    @SerializedName("product_weight")
    public int mProductWeight;
    @SerializedName("product")
    private String mProduct;
    @SerializedName("actual_weight")
    private String mWeight;
    @SerializedName("descriptions")
    private String mDesc;
    @SerializedName("pickup_name")
    private String mPickupName;
    @SerializedName("pickup_loc")
    private String mPickuploc;
    @SerializedName("pickup_lat")
    private String mPickupLat;
    @SerializedName("pickup_long")
    private String mPickUplong;
    @SerializedName("pickup_type")
    private boolean mPickuptype;
    @SerializedName("pickup_mob")
    private String mPickupMobile;
    @SerializedName("pickup_time")
    private String mPickuptime;
    @SerializedName("del_name")
    private String mDeliveryname;
    @SerializedName("del_loc")
    private String mDelLocation;
    @SerializedName("del_lat")
    private String mDellat;
    @SerializedName("del_long")
    private String mDellong;
    @SerializedName("del_mob")
    private String mDelmobile;
    @SerializedName("delivery_type")
    private String mDeliveryType;
    @SerializedName("payment_type")
    private String mPaymentype;
    @SerializedName("created_on")
    private String mCreatedOn;
    @SerializedName("pickup_image")
    private ArrayList<ImageListItem> mPickImage = new ArrayList<>();

    @SerializedName("delivery_image")
    private ArrayList<ImageListItem> mDelivreyImage = new ArrayList<>();

    @SerializedName("is_regular")
    private boolean is_regular;
    @SerializedName("orderassignment")
    private String mOrderAssignment;


    public String getmOrderAssignment() {
        return mOrderAssignment;
    }

    public void setmOrderAssignment(String mOrderAssignment) {
        this.mOrderAssignment = mOrderAssignment;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getmCategory() {
        return mCategory;
    }

    public void setmCategory(int mCategory) {
        this.mCategory = mCategory;
    }

    public int getmProductWeight() {
        return mProductWeight;
    }

    public void setmProductWeight(int mProductWeight) {
        this.mProductWeight = mProductWeight;
    }

    public String getmProduct() {
        return mProduct;
    }

    public void setmProduct(String mProduct) {
        this.mProduct = mProduct;
    }

    public String getmWeight() {
        return mWeight;
    }

    public void setmWeight(String mWeight) {
        this.mWeight = mWeight;
    }

    public String getmDesc() {
        return mDesc;
    }

    public void setmDesc(String mDesc) {
        this.mDesc = mDesc;
    }

    public String getmPickupName() {
        return mPickupName;
    }

    public void setmPickupName(String mPickupName) {
        this.mPickupName = mPickupName;
    }

    public String getmPickuploc() {
        return mPickuploc;
    }

    public void setmPickuploc(String mPickuploc) {
        this.mPickuploc = mPickuploc;
    }

    public String getmPickupLat() {
        return mPickupLat;
    }

    public void setmPickupLat(String mPickupLat) {
        this.mPickupLat = mPickupLat;
    }

    public String getmPickUplong() {
        return mPickUplong;
    }

    public void setmPickUplong(String mPickUplong) {
        this.mPickUplong = mPickUplong;
    }

    public boolean ismPickuptype() {
        return mPickuptype;
    }

    public void setmPickuptype(boolean mPickuptype) {
        this.mPickuptype = mPickuptype;
    }

    public String getmPickupMobile() {
        return mPickupMobile;
    }

    public void setmPickupMobile(String mPickupMobile) {
        this.mPickupMobile = mPickupMobile;
    }

    public String getmPickuptime() {
        return mPickuptime;
    }

    public void setmPickuptime(String mPickuptime) {
        this.mPickuptime = mPickuptime;
    }

    public String getmDeliveryname() {
        return mDeliveryname;
    }

    public void setmDeliveryname(String mDeliveryname) {
        this.mDeliveryname = mDeliveryname;
    }

    public String getmDelLocation() {
        return mDelLocation;
    }

    public void setmDelLocation(String mDelLocation) {
        this.mDelLocation = mDelLocation;
    }

    public String getmDellat() {
        return mDellat;
    }

    public void setmDellat(String mDellat) {
        this.mDellat = mDellat;
    }

    public String getmDellong() {
        return mDellong;
    }

    public void setmDellong(String mDellong) {
        this.mDellong = mDellong;
    }

    public String getmDelmobile() {
        return mDelmobile;
    }

    public void setmDelmobile(String mDelmobile) {
        this.mDelmobile = mDelmobile;
    }

    public String getmDeliveryType() {
        return mDeliveryType;
    }

    public void setmDeliveryType(String mDeliveryType) {
        this.mDeliveryType = mDeliveryType;
    }

    public String getmPaymentype() {
        return mPaymentype;
    }

    public void setmPaymentype(String mPaymentype) {
        this.mPaymentype = mPaymentype;
    }

    public String getmCreatedOn() {
        return getCreatedDate(mCreatedOn);
    }

    public void setmCreatedOn(String mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }

    public ArrayList<ImageListItem> getmPickImage() {
        return mPickImage;
    }

    public void setmPickImage(ArrayList<ImageListItem> mPickImage) {
        this.mPickImage = mPickImage;
    }

    public ArrayList<ImageListItem> getmDelivreyImage() {
        return mDelivreyImage;
    }

    public void setmDelivreyImage(ArrayList<ImageListItem> mDelivreyImage) {
        this.mDelivreyImage = mDelivreyImage;
    }

    public boolean isIs_regular() {
        return is_regular;
    }

    public void setIs_regular(boolean is_regular) {
        this.is_regular = is_regular;
    }
}

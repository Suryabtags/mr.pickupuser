package in.mrpickup.userapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import in.mrpickup.userapp.Adapter.ImageAdapter;
import in.mrpickup.userapp.Adapter.ProductListAdapter;
import in.mrpickup.userapp.Model.Productdata;
import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.callback.ProductCallback;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.userapp.app.Api.KEY_IMAGE;

public class BuyActivity extends AppCompatActivity implements View.OnClickListener, ProductCallback {
    private static final int REQUEST_IMAGE = 4;
    private static final int REQUEST_WRITE_STORAGE = 4;
    private static final int RESULT_PICK_CONTACT = 123;
    private static final int RESULT_CODE = 143;
    protected RecyclerView recyclerView;
    String productId = "";
    ArrayList<Productdata> myList;
    ProductListAdapter productListAdapter;
    double slat, slng, dlat, dlng;
    private Toolbar mToolbar;
    private Context mContext;
    private ImageView mImageViewMedia, mContact;
    private Button mOrderBtn;
    private TextInputEditText txtDate, txtTime, txtStore, mInputContact, mInputName, MInputStoreCode;
    private TextInputLayout layoutContact, layoutName;
    private EditText txtDesc;
    private ProgressDialog mProgressDialog;
    private LinearLayout mLayoutMediaContainer;
    private Spinner spinPaymentType, deliveryType;
    private List<File> mImageArrayList;
    private PreferenceManager mPreferenceManager;
    private String dateh, timeh, desc, store, Storecode, contact, username, mPaymentoption, mDeliverType;
    private ImageAdapter imageAdapter;
    private TextView addproduct;
    private RecyclerView mProductsListview;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);
        initObjects();
        initCall();
        setupToolbar();

    }

    private void initObjects() {
        mToolbar = findViewById(R.id.toolbar);
        mOrderBtn = findViewById(R.id.btn_order);
        recyclerView = findViewById(R.id.recylerview);
        mInputContact = findViewById(R.id.input_contact_number);
        mImageViewMedia = findViewById(R.id.img_media1);
        mContext = this;
        mInputName = findViewById(R.id.input_name);
        layoutName = findViewById(R.id.name);
        mContact = findViewById(R.id.contact);
        txtDate = findViewById(R.id.input_dateB);
        txtDate.setText(getCurrentDate());
        txtTime = findViewById(R.id.input_timeB);
        txtTime.setText(getCurrentTime());
        addproduct = findViewById(R.id.addproduct);
        mProductsListview = findViewById(R.id.products_listview);
        layoutContact = findViewById(R.id.contact_number);
        txtStore = findViewById(R.id.input_store);
        MInputStoreCode = findViewById(R.id.input_store_code);
        txtDesc = findViewById(R.id.input_description);
        mLayoutMediaContainer = findViewById(R.id.media_container1);
        mImageArrayList = new ArrayList<>();
        mProgressDialog = new ProgressDialog(mContext);
        mPreferenceManager = new PreferenceManager(this);

        spinPaymentType = findViewById(R.id.spinPaymentType);
        deliveryType = findViewById(R.id.deliveryType);
        slat = getIntent().getExtras().getDouble("SOURCE_LAT");
        slng = getIntent().getExtras().getDouble("SOURCE_LNG");
        dlat = getIntent().getExtras().getDouble("DESTINATION_LAT");
        dlng = getIntent().getExtras().getDouble("DESTINATION_LNG");


        mInputName.setText(mPreferenceManager.getName());
        mInputContact.setText(mPreferenceManager.getPhone());
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext,
                R.array.paymenttype, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPaymentType.setAdapter(adapter);

        spinPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                TextView selectedText = (TextView) arg0.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    selectedText.setTextSize(14);
                }
                if (position == 0) {
                    mPaymentoption = "CD";
                } else {
                    mPaymentoption = "ON";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(mContext,
                R.array.deliverytype, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deliveryType.setAdapter(adapter1);

        deliveryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                TextView selectedText = (TextView) arg0.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    selectedText.setTextSize(14);
                    selectedText.setPadding(12, 12, 12, 12);
                }
                if (position == 0) {
                    mDeliverType = "OR";
                } else {
                    mDeliverType = "UR";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        mInputName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2) {
                    layoutName.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initCall() {
        mOrderBtn.setOnClickListener(this);
        txtDate.setOnClickListener(this);
        txtTime.setOnClickListener(this);
        mContact.setOnClickListener(this);
        mImageViewMedia.setOnClickListener(this);
        addproduct.setOnClickListener(this);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Buy & Delivery");
        mToolbar.setNavigationIcon(R.drawable.ic_left_arrow_white);
        mToolbar.setTitleTextColor(ContextCompat.getColor(mContext, R.color.shade_white_cc));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("unused")
    public void addItemsOnSpinner2() {
        List<String> list = new ArrayList<>();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        } else if (requestCode == RESULT_CODE) {

            if (data != null) {
                mProducts(data.<Productdata>getParcelableArrayListExtra("ProductsLits"));
            }

        }
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source,
                                       int type) {
                mImageArrayList.addAll(imageFiles);
                showMedia();
            }
        });
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
            mInputContact.setText(phoneNo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void pickContact(int resultPickContact) {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, resultPickContact);

    }

    @Override
    public void onClick(View v) {
        if (v == mOrderBtn) {

            if (NetworkError.getInstance(mContext).isOnline()) {
                bookNow();
            } else {
                ToastBuilder.build(mContext, " No Internet Connection");
            }


        } else if (v == mImageViewMedia) {
            processPickImages();
        } else if (v == txtDate) {
            promptDatePickerDialog(txtDate);
        } else if (v == txtTime) {
            promptTimePickerDialog(txtTime);
        } else if (v == mContact) {
            pickContact(RESULT_PICK_CONTACT);
        } else if (v == addproduct) {

            if (NetworkError.getInstance(mContext).isOnline()) {
                Intent intent = new Intent(getBaseContext(), AddBuyProductActivity.class);
                intent.putExtra("SOURCE_LAT", slat);
                intent.putExtra("DESTINATION_LAT", dlat);
                intent.putExtra("SOURCE_LNG", slng);
                intent.putExtra("DESTINATION_LNG", dlng);
                startActivityForResult(intent, RESULT_CODE);
            } else {
                ToastBuilder.build(mContext, " No Internet Connection");
            }


        }
    }

    private void mProducts(ArrayList<Productdata> data) {
        myList = new ArrayList<>();
        myList = data;
        if (myList != null) {
            if (myList.size() > 0) {
                mProductsListview.setVisibility(View.VISIBLE);
                productListAdapter = new ProductListAdapter(mContext, myList, this);
                mProductsListview.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true));
                mProductsListview.setHasFixedSize(true);
                mProductsListview.setScrollBarSize(0);
                mProductsListview.setAdapter(productListAdapter);
                productListAdapter.notifyDataSetChanged();
            } else {

            }
        }
    }


    @Override
    public void onItemRemoveClick(int position) {
        myList.remove(position);
        productListAdapter.notifyItemRemoved(position);
        productListAdapter.notifyItemRangeChanged(position, myList.size());
        if (myList.size() == 0) {
            mProductsListview.setVisibility(View.GONE);
        } else {
            mProductsListview.setVisibility(View.VISIBLE);
        }
    }


    private void disableError() {
        layoutName.setErrorEnabled(false);
        layoutContact.setErrorEnabled(false);
        /*layoutDate.setErrorEnabled(false);
        layoutProduct.setErrorEnabled(false);
        layoutTime.setErrorEnabled(false);
*/
    }

    private void processRateCard() {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_rate_card, null);
        ImageView imageViewClose = mView.findViewById(R.id.img_close);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(mView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void promptDatePickerDialog(TextInputEditText editDate) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        openDatePickerDialog(editDate, year, month, dayOfMonth);
    }

    private void promptTimePickerDialog(TextInputEditText editTextDuration) {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY) + 1;
        int minute = calendar.get(Calendar.MINUTE);
        openTimePickerDialog(editTextDuration, hourOfDay, minute);


    }

    private void openDatePickerDialog(final EditText editDate, int year, int month,
                                      int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.DatePickerTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        editDate.setText(getDate(year, month + 1, dayOfMonth));
                    }
                }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void openTimePickerDialog(final EditText editDuration, int hourOfDay, int minute) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(mContext, R.style.DatePickerTheme,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        editDuration.setText(getTime(hourOfDay, minute));
                    }
                }, hourOfDay, minute, false);
        timePickerDialog.show();
    }

    private String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        dateh = getDate(year, month + 1, dayOfMonth);
        return getDate(year, month + 1, dayOfMonth);
    }

    private String getDate(int year, int month, int dayOfMonth) {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (dayOfMonth < 10 ? "0"
                + dayOfMonth : dayOfMonth);
    }

    private String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY) + 1;
        int minute = calendar.get(Calendar.MINUTE);
        timeh = getTime(hourOfDay, minute);
        return changeTo12hr(hourOfDay, minute);
    }

    private String getTime(int hourOfDay, int minute) {
        return (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute
                : minute);
    }

    public String changeTo12hr(int hourOfDay, int minute) {
        return (hourOfDay > 12 ? hourOfDay - 12 : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute) + (hourOfDay > 12 ? "pm" : "am");

    }

    private void checkImage() {
        int imageCount = mImageArrayList.size();
        if (imageCount == 0) {
            mImageViewMedia.setVisibility(View.VISIBLE);
            mLayoutMediaContainer.setVisibility(View.GONE);
        } else if (imageCount == 1) {
            mImageViewMedia.setVisibility(View.GONE);
            mLayoutMediaContainer.setVisibility(View.VISIBLE);
        }
    }

    private boolean hasWriteStoragePermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestWriteStoragePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
    }

    private void showMedia() {
        imageAdapter = new ImageAdapter(this, mImageArrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(imageAdapter);
    }

    private void processPickImages() {
        if (hasWriteStoragePermission()) {
            pickImages();
        } else {
            requestWriteStoragePermission();
        }
    }

    private void pickImages() {
        EasyImage.configuration(mContext)
                .setImagesFolderName(getString(R.string.app_name))
                .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(this, "Select Images", REQUEST_IMAGE);
    }

    @SuppressLint("InflateParams")
    private void displayImage(String path) {
        View mediaView = LayoutInflater.from(mContext).inflate(R.layout.dialog_media, null);
        ImageView imageViewGrievance = mediaView.findViewById(R.id.img_media);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setView(mediaView);
        alertDialog.show();

        Glide.with(this).load(path).into(imageViewGrievance);
    }

    private void promptBookingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to Buy?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog("Booking...");
                try {
                    getOrderRequest(store, desc, contact, username);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getOrderRequest(String store, String desc, String contact, String username) throws JSONException {

        String dateT = dateh + " " + timeh + ":00";

        ArrayList<Part> partArrayList = new ArrayList<>();

        JSONArray jsonArray = new JSONArray();

        for (int j = 0; j < myList.size(); j++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", myList.get(j).getSubdata());
            jsonObject.put("category_id", myList.get(j).getmId());
            jsonObject.put("quantity", myList.get(j).getmQuantity());
            jsonObject.put("units_id", myList.get(j).getmUnitId1());
            jsonArray.put(jsonObject);

        }

        partArrayList.add(new StringPart(Api.KEY_PICKUP_NAME, username));
        partArrayList.add(new StringPart(Api.KEY_STORENAME, store));
        partArrayList.add(new StringPart(Api.KEY_STORECODE, Storecode));
        partArrayList.add(new StringPart(Api.KEY_PICKLAT, String.valueOf(slat)));
        partArrayList.add(new StringPart(Api.KEY_PICKLONG, String.valueOf(slng)));
        partArrayList.add(new StringPart(Api.KEY_PRODUCT, jsonArray.toString()));
        partArrayList.add(new StringPart(Api.KEY_PICKUP_MOB, contact));
        partArrayList.add(new StringPart(Api.KEY_PRODUCT_WEIGHT, "1"));
        partArrayList.add(new StringPart(Api.KEY_CATEGORY, productId));
        partArrayList.add(new StringPart(Api.KEY_DESC, desc));
        partArrayList.add(new StringPart(Api.KEY_DELIVER_LAT, String.valueOf(dlat)));
        partArrayList.add(new StringPart(Api.KEY_DELIVER_LONG, String.valueOf(dlng)));
        partArrayList.add(new StringPart(Api.KEY_DELIVERY_NAME, username));
        partArrayList.add(new StringPart(Api.KEY_DELIVER_MOB, contact));
        partArrayList.add(new StringPart(Api.KEY_PICKUP_TYPE, "0"));
        partArrayList.add(new StringPart(Api.KEY_PAYMENTTYPE, mPaymentoption));
        partArrayList.add(new StringPart(Api.KEY_DELIVERYTYPE, mDeliverType));
        partArrayList.add(new StringPart(Api.KEY_PICKUP_TIME, dateT));
        for (int i = 0; i < mImageArrayList.size(); i++) {
            partArrayList.add(new FilePart(KEY_IMAGE, new File(mImageArrayList.get(i).getPath())));
        }

        Ion.with(mContext)
                .load(Api.CREATE_ORDER)
                .addHeader("Authorization", "Token " + mPreferenceManager.getToken())
                .addMultipartParts(partArrayList)
                .asJsonObject()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<JsonObject>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<JsonObject> result) {
                        hideProgressDialog();

                        Log.e("result", "" + result.getResult().toString());
                        if (e == null) {
                            int code = result.getHeaders().code();
                            Log.e("code", "" + code);
                            if (code == 200 || code == 201) {
                                ToastBuilder.build(mContext, "Booked Order");
                                Activity.launch(mContext, TripsActivity.class);
                                onBackPressed();
                            }
                        } else {
                            ToastBuilder.build(mContext, e.getMessage());

                            Log.e("errjir", "" + e.getMessage());
                        }
                    }
                });

    }

    private void bookNow() {
        username = mInputName.getText().toString().trim();
        Storecode = MInputStoreCode.getText().toString().trim();
        contact = mInputContact.getText().toString().trim();
        store = txtStore.getText().toString().trim();
        desc = txtDesc.getText().toString().trim();
        if (validateInput(store, desc, contact, username)) {
            if (myList != null) {
                promptBookingDialog();
            } else {
                ToastBuilder.build(mContext, "Please Add the Products ");
            }
        }
    }

    private Boolean validateInput(String store, String desc, String number, String username) {
        if (TextUtils.isEmpty(this.username)) {
            mInputName.requestFocus();
            layoutName.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Contact Name"));
            return false;
        } else if (TextUtils.isEmpty(number)) {
            mInputContact.requestFocus();
            layoutContact.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Contact Number"));
            return false;
        } else if (number.length() < 10) {
            mInputContact.requestFocus();
            layoutContact.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "Contact Number", 10, "digits"));
            return false;
        } else if (TextUtils.isEmpty(desc)) {
            txtDesc.requestFocus();
            txtDesc.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Description"));
            return false;
        }
        return true;
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}

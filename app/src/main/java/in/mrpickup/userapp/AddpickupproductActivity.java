package in.mrpickup.userapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.mrpickup.userapp.Adapter.ProductAdapter;
import in.mrpickup.userapp.Adapter.ProductListAdapter;
import in.mrpickup.userapp.Adapter.UnitsAdapter;
import in.mrpickup.userapp.Model.Productdata;
import in.mrpickup.userapp.Model.Products;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.callback.ProductCallback;
import in.mrpickup.userapp.helper.ErrorHandler;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.userapp.app.Constant.RESULT_CODE;

public class AddpickupproductActivity extends AppCompatActivity implements ProductCallback, View.OnClickListener {
    EditText add_products, quantity;
    ArrayList<Productdata> myList;
    ArrayList<HashMap<String, String>> ListProducts = new ArrayList<>();
    ArrayList<HashMap<String, String>> ListUnits = new ArrayList<>();
    TextView mBtn_Add, mSave;
    int productListsize;
    int mUnitsize;
    String mUnitId, unitName, product, mQuantity;
    String productId = "";
    String productname = "";
    ProductListAdapter productListAdapter;
    TextView mTextProductLabel;
    ImageView mImageClose;
    Productdata operatordata;
    double slat, slng, dlat, dlng;
    String DistanceKm;
    private Spinner spinner1, spinner2;
    private Context mContext;
    private ArrayList<HashMap<String, String>> mAllProducts = new ArrayList<>();
    private RecyclerView mProductsListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addproduct);

        initObjects();
        initCallbacks();


    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initCallbacks() {
        mImageClose.setOnClickListener(this);
        mSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mImageClose) {
            onBackPressed();
        } else if (v == mSave) {
            mSavebutton();

            if (myList.size() > 0) {
                Intent intent = new Intent(AddpickupproductActivity.this, CatergoryActivity.class);
                intent.putParcelableArrayListExtra("ProductsLits", myList);
                intent.putExtra("SOURCE_LAT", slat);
                intent.putExtra("DESTINATION_LAT", dlat);
                intent.putExtra("SOURCE_LNG", slng);
                intent.putExtra("DESTINATION_LNG", dlng);
                intent.putExtra("DistanceKm", DistanceKm);
                //     intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                setResult(RESULT_CODE, intent);
                finish();
            } else {
                ToastBuilder.build(mContext, "Please Add the Products");
            }

        }

    }

    private void initObjects() {
        mContext = this;
        spinner1 = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        mBtn_Add = findViewById(R.id.Apply_btn);
        mTextProductLabel = findViewById(R.id.txt_productlist);
        add_products = findViewById(R.id.add_products);
        quantity = findViewById(R.id.quantity);
        mSave = findViewById(R.id.img_save);
        mImageClose = findViewById(R.id.img_close);
        mProductsListview = findViewById(R.id.products_listview);
        myList = new ArrayList<>();
        slat = getIntent().getExtras().getDouble("SOURCE_LAT");
        slng = getIntent().getExtras().getDouble("SOURCE_LNG");
        dlat = getIntent().getExtras().getDouble("DESTINATION_LAT");
        dlng = getIntent().getExtras().getDouble("DESTINATION_LNG");
        dlng = getIntent().getExtras().getDouble("DESTINATION_LNG");
        DistanceKm = getIntent().getExtras().getString("DistanceKm");
        getProducts();
        getUnits();
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                TextView selectedText = (TextView) adapterView.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                }
                productId = ListProducts.get(i).get("productid");
                productname = ListProducts.get(i).get("productname");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                mUnitId = ListUnits.get(i).get("unitsId");
                unitName = ListUnits.get(i).get("unitsName");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mBtn_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mbuttonAdd();
            }
        });
    }

    private void getProducts() {
        ApiInterface apiInterface = Api.getClient().create(ApiInterface.class);
        Call<ArrayList<Products>> call = apiInterface.getProductList();
        call.enqueue(new retrofit2.Callback<ArrayList<Products>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<Products>> call, @NonNull Response<ArrayList<Products>> response) {
                List<Products> products = response.body();
                if (response.isSuccessful() && products != null) {
                    productListsize = products.size();
                    Log.e("products", "" + response.toString());
                    ListProducts.clear();
                    for (int i = 0; i < productListsize; i++) {
                        HashMap<String, String> categorymap = new HashMap<>();
                        categorymap.put("productid", String.valueOf(products.get(i).getmId()));
                        categorymap.put("productname", products.get(i).getmName());
                        ListProducts.add(categorymap);
                    }
                    spinner1.setAdapter(new ProductAdapter(mContext, ListProducts));

                } else {

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Products>> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void getUnits() {
        ApiInterface apiInterface = Api.getClient().create(ApiInterface.class);
        Call<ArrayList<Products>> call = apiInterface.getUnits();
        call.enqueue(new retrofit2.Callback<ArrayList<Products>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<Products>> call, @NonNull Response<ArrayList<Products>> response) {
                List<Products> products = response.body();
                if (response.isSuccessful() && products != null) {
                    mUnitsize = products.size();
                    ListUnits.clear();
                    for (int i = 0; i < mUnitsize; i++) {
                        HashMap<String, String> categorymap = new HashMap<>();
                        categorymap.put("unitsId", String.valueOf(products.get(i).getmId()));
                        categorymap.put("unitsName", products.get(i).getmName());
                        ListUnits.add(categorymap);
                    }
                    spinner2.setAdapter(new UnitsAdapter(mContext, ListUnits));

                } else {

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Products>> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void mbuttonAdd() {
        product = add_products.getText().toString();
        mQuantity = quantity.getText().toString();

        if (product.isEmpty()) {
            add_products.setError("Please enter the product");
        } else if (mQuantity.isEmpty()) {
            quantity.setError("Please enter Quantity");
        } else {
            HashMap<String, String> productmap = new HashMap<>();
            productmap.put("selectedproductname", productname);
            productmap.put("addproductname", product);
            productmap.put("selectedProductid", productId);
            productmap.put("productQuantity", mQuantity);
            productmap.put("mUnitName", unitName);
            productmap.put("units_id", mUnitId);
            mAllProducts.add(productmap);
            add_products.getText().clear();
            quantity.getText().clear();
            for (int i = 0; i < mAllProducts.size(); i++) {
                operatordata = new Productdata(mAllProducts.get(i).get("selectedproductname"),
                        mAllProducts.get(i).get("addproductname"), mAllProducts.get(i).get("productQuantity"),
                        mAllProducts.get(i).get("mUnitName"), mAllProducts.get(i).get("selectedProductid"), mAllProducts.get(i).get("units_id"));
                Log.e("mylistzize", "" + mAllProducts.get(i).get("units_id"));

            }
            myList.add(operatordata);
            mProductsListview.setVisibility(View.VISIBLE);
            mTextProductLabel.setVisibility(View.VISIBLE);
            productListAdapter = new ProductListAdapter(mContext, myList, this);
            mProductsListview.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true));
            mProductsListview.setHasFixedSize(true);
            mProductsListview.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
            mProductsListview.setScrollBarSize(0);
            mProductsListview.setAdapter(productListAdapter);
            productListAdapter.notifyDataSetChanged();
        }

    }


    @Override
    public void onItemRemoveClick(int position) {

        myList.remove(position);
        mAllProducts.remove(position);
        productListAdapter.notifyItemRemoved(position);
        productListAdapter.notifyItemRangeChanged(position, myList.size());

        if (mAllProducts.size() == 0) {
            mProductsListview.setVisibility(View.GONE);
            mTextProductLabel.setVisibility(View.GONE);
        } else {
            mProductsListview.setVisibility(View.VISIBLE);
            mTextProductLabel.setVisibility(View.VISIBLE);
        }
    }

    private void mSavebutton() {
        product = add_products.getText().toString();
        mQuantity = quantity.getText().toString();

        if (product.isEmpty()) {
            add_products.setError("Please enter  product");
        } else if (mQuantity.isEmpty()) {
            quantity.setError("Please enter Quantity");
        } else {
            HashMap<String, String> productmap = new HashMap<>();
            productmap.put("selectedproductname", productname);
            productmap.put("addproductname", product);
            productmap.put("selectedProductid", productId);
            productmap.put("productQuantity", mQuantity);
            productmap.put("mUnitName", unitName);
            productmap.put("units_id", mUnitId);
            mAllProducts.add(productmap);
            add_products.getText().clear();
            quantity.getText().clear();
            for (int i = 0; i < mAllProducts.size(); i++) {
                operatordata = new Productdata(mAllProducts.get(i).get("selectedproductname"),
                        mAllProducts.get(i).get("addproductname"), mAllProducts.get(i).get("productQuantity"),
                        mAllProducts.get(i).get("mUnitName"), mAllProducts.get(i).get("selectedProductid"), mAllProducts.get(i).get("units_id"));
                Log.e("mylistzize", "" + mAllProducts.get(i).get("units_id"));

            }
            myList.add(operatordata);
        }

    }

}

package in.mrpickup.userapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ResetPassActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int REQUEST_READ_SMS = 4;
    private Context mContext;
    private Toolbar mToolbar;
    private EditText mEditTextPhone;
    private Button mButtonResetPass;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initObjects();
        initCallbacks();

        setupToolbar();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_SMS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    processForgotPass();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonResetPass) {

            if (NetworkError.getInstance(this).isOnline()) {
                processForgotPass();
            } else {
                ToastBuilder.build(this, " No Internet Connection");
            }

        }
    }

    private void initObjects() {
        mToolbar = findViewById(R.id.toolbar);
        mEditTextPhone = findViewById(R.id.input_phone);
        mButtonResetPass = findViewById(R.id.btn_reset_pass);

        mContext = this;
        mPreferenceManager = new PreferenceManager(this);
        mProgressDialog = new ProgressDialog(this);
    }

    private void initCallbacks() {
        mButtonResetPass.setOnClickListener(this);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Reset Password");
        mToolbar.setTitleTextColor(ContextCompat.getColor(mContext, R.color.shade_white_cc));
        mToolbar.setNavigationIcon(R.drawable.ic_left_arrow_white);
    }

    private void processForgotPass() {
        String phone = mEditTextPhone.getText().toString().trim();
        if (validateInput(phone)) {
            if (hasReadSmsPermission()) {
                showProgressDialog("Verifying phone..");
                mPreferenceManager.setPhone(phone);
                forgotPass(getForgotPassRequestJson(phone));
            } else {
                requestReadSmsPermission();
            }
        }
    }

    private JSONObject getForgotPassRequestJson(String phone) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Api.KEY_MOBILENUMBER, phone);
            jsonObject.put(Api.KEY_ROLE, "5");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private boolean validateInput(String phone) {
        if (TextUtils.isEmpty(phone)) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Phone Number"));
            return false;
        } else if (phone.length() < 10) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "Phone Number", 10, "digits"));
            return false;
        }
        return true;
    }

    private void forgotPass(JSONObject forgotPassRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                Api.FORGOT_PASS, forgotPassRequestJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handleForgotPassResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
                Log.e("eeerere", "" + error.toString());
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "forgot_pass");
    }

    private void handleForgotPassResponse(JSONObject response) {
        try {
            String mobile = response.getString(Api.KEY_MOBILENUMBER);
            String role = response.getString("role");
            Log.e("jdhfdkfd", "" + mobile);
            ToastBuilder.build(mContext, "Otp Send Successfully");
            Activity.launch(mContext, SetPasswordActivity.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestReadSmsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS},
                REQUEST_READ_SMS);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}



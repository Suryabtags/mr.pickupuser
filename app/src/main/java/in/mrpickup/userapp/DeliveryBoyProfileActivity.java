package in.mrpickup.userapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.userapp.Model.DeliveryBoyProfileView;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.Constant;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.helper.ErrorHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryBoyProfileActivity extends AppCompatActivity implements View.OnClickListener {

    TextView usr_name, usr_phone, usr_mail, usr_address, usr_ratings, usr_dob;
    private CircleImageView mImageViewProfile;
    private ImageView mImageBack;
    private PreferenceManager mPreference;
    private Context mContext;
    private String mUrl, mEmpId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_boy_profile);
        processBundle();
        initObjects();
        initCallbacks();

        if (NetworkError.getInstance(mContext).isOnline()) {
            setUrl();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }


    }

    private void setUrl() {

        mUrl = Api.URL + "employee/" + mEmpId + "/retrieve_employee/";

        getProfilemethod();
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        mEmpId = bundle != null ? bundle.getString(Constant.KEY_EMPID) : null;

        Log.e("fdkfdlf", "" + mEmpId);
    }

    private void initCallbacks() {
        mImageBack.setOnClickListener(this);
    }


    private void initObjects() {
        mContext = this;

        mImageViewProfile = findViewById(R.id.img_profile);
        mImageBack = findViewById(R.id.img_back);
        usr_name = findViewById(R.id.usr_name);
        usr_ratings = findViewById(R.id.ratings);
        usr_phone = findViewById(R.id.usr_phone);
        usr_mail = findViewById(R.id.usr_mail);
        usr_dob = findViewById(R.id.usr_dob);
        usr_address = findViewById(R.id.usr_address);
        mPreference = new PreferenceManager(this);

    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        }
    }

    public void getProfilemethod() {
        ApiInterface apiService = Api.getClient().create(ApiInterface.class);
        Call<DeliveryBoyProfileView> call = apiService.getDeliveryboyProfile(mUrl, "Token " + mPreference.getToken());
        call.enqueue(new Callback<DeliveryBoyProfileView>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<DeliveryBoyProfileView> call, @NonNull Response<DeliveryBoyProfileView> response) {
                DeliveryBoyProfileView profileView = response.body();
                if (response.isSuccessful() && profileView != null) {
                    usr_name.setText(profileView.getEmployee().getmFirstName());
                    usr_phone.setText(profileView.getEmployee().getmUsername());
                    usr_mail.setText(profileView.getEmployee().getmEmail());
                    usr_ratings.setText(profileView.getEmployee().getUserprofile().getmRatings());
                    usr_dob.setText(profileView.getEmployee().getUserprofile().getDob());
                    if (profileView.getEmployee().getUseraddress() != null) {
                        if (profileView.getEmployee().getUseraddress().getStreet() != null &&
                                profileView.getEmployee().getUseraddress().getCity() != null &&
                                profileView.getEmployee().getUseraddress().getCountry() != null &&
                                profileView.getEmployee().getUseraddress().getState() != null &&
                                profileView.getEmployee().getUseraddress().getmZipcode() != null) {
                            usr_address.setVisibility(View.VISIBLE);
                            usr_address.setText(profileView.getEmployee().getUseraddress().getStreet() + " ," +
                                    profileView.getEmployee().getUseraddress().getCity() + " ," +
                                    profileView.getEmployee().getUseraddress().getState() + " ," +
                                    profileView.getEmployee().getUseraddress().getCountry() + " ," +
                                    profileView.getEmployee().getUseraddress().getmZipcode());
                        }


                    }

                    String imageloader = profileView.getEmployee().getUserprofile().getProfile_pic();
                    if (imageloader != null) {
                        Glide.with(mContext).load(imageloader).into(mImageViewProfile);

                    } else {
                        Glide.with(mContext).load(R.drawable.man).into(mImageViewProfile);
                    }

                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }


            @Override
            public void onFailure(@NonNull Call<DeliveryBoyProfileView> call, @NonNull Throwable t) {
                Log.e("failuretokenprofile", "" + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

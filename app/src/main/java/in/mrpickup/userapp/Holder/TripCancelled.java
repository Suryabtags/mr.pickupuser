package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.mrpickup.userapp.R;

/**
 * Created by Admin on 7/6/2017.
 */

public class TripCancelled extends RecyclerView.ViewHolder {

    private TextView mTextViewTripId, mTextViewAddress, mTextViewDrop, mTextViewExtras, mTriptype,
            mTextViewDate;
    private ImageView imageView;

    public TripCancelled(View itemView) {
        super(itemView);
        mTextViewTripId = itemView.findViewById(R.id.txt_trip_id);
        mTextViewAddress = itemView.findViewById(R.id.txt_address);
        mTextViewDrop = itemView.findViewById(R.id.txt_drop);
        mTextViewDate = itemView.findViewById(R.id.txt_date);
        imageView = itemView.findViewById(R.id.img_cancel);
        mTriptype = itemView.findViewById(R.id.txt_trip_type);
    }

    public TextView getmTriptype() {
        return mTriptype;
    }

    public void setmTriptype(TextView mTriptype) {
        this.mTriptype = mTriptype;
    }

    public TextView getTextViewTripId() {
        return mTextViewTripId;
    }

    public TextView getTextViewAddress() {
        return mTextViewAddress;
    }

    public TextView getTextViewDrop() {
        return mTextViewDrop;
    }

    public TextView getTextViewDate() {
        return mTextViewDate;
    }

    public ImageView getImageView() {
        return imageView;
    }
}

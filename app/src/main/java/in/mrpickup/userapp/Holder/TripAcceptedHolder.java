package in.mrpickup.userapp.Holder;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.userapp.R;

public class TripAcceptedHolder extends RecyclerView.ViewHolder {

    private TextView mTextViewTripId, mTextViewAddress, mTextViewComments, mTextViewDrop,
            mTextViewDate, mTextViewPrice, mTextViewName, mTextViewPhone, mTextViewOtp, mTextViewStatus, mTriptype;
    private CircleImageView mImageViewProfile;

    public TripAcceptedHolder(View itemView) {
        super(itemView);
        mTextViewTripId = itemView.findViewById(R.id.txt_trip_id);
        mTextViewAddress = itemView.findViewById(R.id.txt_address);
        mTextViewDrop = itemView.findViewById(R.id.txt_drop);
        mTextViewDate = itemView.findViewById(R.id.txt_date);
        mTextViewName = itemView.findViewById(R.id.txt_name);
        mTextViewPhone = itemView.findViewById(R.id.txt_phone);
        mTextViewOtp = itemView.findViewById(R.id.txt_otp);
        mImageViewProfile = itemView.findViewById(R.id.img_profile);
        mTriptype = itemView.findViewById(R.id.txt_trip_type);
    }

    public TextView getmTriptype() {
        return mTriptype;
    }

    public void setmTriptype(TextView mTriptype) {
        this.mTriptype = mTriptype;
    }

    public TextView getTextViewTripId() {
        return mTextViewTripId;
    }

    public TextView getTextViewAddress() {
        return mTextViewAddress;
    }


    public TextView getTextViewDrop() {
        return mTextViewDrop;
    }

    public TextView getTextViewDate() {
        return mTextViewDate;
    }


    public TextView getTextViewName() {
        return mTextViewName;
    }

    public TextView getTextViewPhone() {
        return mTextViewPhone;
    }

    public TextView getTextViewOtp() {
        return mTextViewOtp;
    }

    public CircleImageView getImageViewProfile() {
        return mImageViewProfile;
    }
}


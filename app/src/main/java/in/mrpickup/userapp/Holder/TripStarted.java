package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.userapp.R;

/**
 * Created by Admin on 7/6/2017.
 */

public class TripStarted extends RecyclerView.ViewHolder {

    private TextView mTextViewTripId, mTextViewAddress, mTextViewDrop, mTextViewExtras,
            mTextViewDate, mTextViewPrice, mTextViewName, mTriptype, mTripWaitingTimeStarted;
    private CircleImageView mImageViewProfile;

    public TripStarted(View itemView) {
        super(itemView);
        mTextViewTripId = itemView.findViewById(R.id.txt_trip_id);
        mTextViewAddress = itemView.findViewById(R.id.txt_address);
        mTextViewDrop = itemView.findViewById(R.id.txt_drop);
        mTextViewDate = itemView.findViewById(R.id.txt_date);
        mTextViewPrice = itemView.findViewById(R.id.txt_price);
        mTextViewName = itemView.findViewById(R.id.txt_name);
        mImageViewProfile = itemView.findViewById(R.id.img_profile);
        mTriptype = itemView.findViewById(R.id.txt_trip_type);
        mTripWaitingTimeStarted = itemView.findViewById(R.id.waitingtime);
    }

    public TextView getmTripWaitingTimeStarted() {
        return mTripWaitingTimeStarted;
    }

    public void setmTripWaitingTimeStarted(TextView mTripWaitingTimeStarted) {
        this.mTripWaitingTimeStarted = mTripWaitingTimeStarted;
    }

    public TextView getmTriptype() {
        return mTriptype;
    }

    public void setmTriptype(TextView mTriptype) {
        this.mTriptype = mTriptype;
    }

    public TextView getTextViewTripId() {
        return mTextViewTripId;
    }

    public TextView getTextViewAddress() {
        return mTextViewAddress;
    }

    public TextView getTextViewDrop() {
        return mTextViewDrop;
    }

    public TextView getTextViewExtras() {
        return mTextViewExtras;
    }

    public TextView getTextViewDate() {
        return mTextViewDate;
    }

    public TextView getTextViewPrice() {
        return mTextViewPrice;
    }

    public TextView getTextViewName() {
        return mTextViewName;
    }


    public CircleImageView getImageViewProfile() {
        return mImageViewProfile;
    }
}

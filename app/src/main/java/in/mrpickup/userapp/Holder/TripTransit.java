package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.userapp.R;

/**
 * Created by Admin on 7/7/2017.
 */

public class TripTransit extends RecyclerView.ViewHolder {

    private TextView mTextViewTripId, mTextViewAddress, mTextViewDrop, mTextViewExtras,
            mTextViewDate, mTextViewPrice, mTextViewName, mTriptype, mTripWaitingProcess;
    private CircleImageView mImageViewProfile;

    public TripTransit(View itemView) {
        super(itemView);
        mTextViewTripId = itemView.findViewById(in.mrpickup.userapp.R.id.txt_trip_id);
        mTextViewAddress = itemView.findViewById(in.mrpickup.userapp.R.id.txt_address);
        mTextViewDrop = itemView.findViewById(in.mrpickup.userapp.R.id.txt_drop);
        mTextViewDate = itemView.findViewById(in.mrpickup.userapp.R.id.txt_date);
        mTextViewPrice = itemView.findViewById(in.mrpickup.userapp.R.id.txt_price);
        mTextViewName = itemView.findViewById(in.mrpickup.userapp.R.id.txt_name);
        mImageViewProfile = itemView.findViewById(in.mrpickup.userapp.R.id.img_profile);
        mTriptype = itemView.findViewById(R.id.txt_trip_type);
        mTripWaitingProcess = itemView.findViewById(R.id.waitingtime);
    }

    public TextView getmTripWaitingProcess() {
        return mTripWaitingProcess;
    }

    public void setmTripWaitingProcess(TextView mTripWaitingProcess) {
        this.mTripWaitingProcess = mTripWaitingProcess;
    }

    public TextView getmTriptype() {
        return mTriptype;
    }

    public void setmTriptype(TextView mTriptype) {
        this.mTriptype = mTriptype;
    }
    public TextView getTextViewTripId() {
        return mTextViewTripId;
    }

    public TextView getTextViewAddress() {
        return mTextViewAddress;
    }

    public TextView getTextViewDrop() {
        return mTextViewDrop;
    }

    public TextView getTextViewExtras() {
        return mTextViewExtras;
    }

    public TextView getTextViewDate() {
        return mTextViewDate;
    }

    public TextView getTextViewPrice() {
        return mTextViewPrice;
    }

    public TextView getTextViewName() {
        return mTextViewName;
    }


    public CircleImageView getImageViewProfile() {
        return mImageViewProfile;
    }
}


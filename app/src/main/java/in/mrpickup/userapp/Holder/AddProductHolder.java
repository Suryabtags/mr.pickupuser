package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.mrpickup.userapp.R;
import in.mrpickup.userapp.callback.ProductCallback;

/**
 * Created by Bobby on 19/05/17
 */

public class AddProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mCategory, mSubCategory, mQuantity, mUnits;
    public ImageView img_remove;

    ProductCallback productCallback;

    public AddProductHolder(View itemView, ProductCallback mProductCallback) {
        super(itemView);
        mCategory = itemView.findViewById(R.id.category);
        mSubCategory = itemView.findViewById(R.id.subcategory);
        img_remove = itemView.findViewById(R.id.img_remove);
        mQuantity = itemView.findViewById(R.id.quantity);
        mUnits = itemView.findViewById(R.id.units);
        productCallback = mProductCallback;
        img_remove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == img_remove) {
            productCallback.onItemRemoveClick(getLayoutPosition());
        }
    }


}

package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.userapp.R;

/**
 * Created by Admin on 7/6/2017.
 */

public class TripOrderPlaced extends RecyclerView.ViewHolder {

    LinearLayout mAcceptLayout;
    CircleImageView mImageProfileStatus;
    private TextView mTextViewTripId, mTextViewAddress, mTextViewDrop, mTextViewExtras,
            mTextViewDate, mTextViewPrice, mTextViewTripStatus, mTextViewOtp, mAccept, mReject, morderType, mTriptype;

    public TripOrderPlaced(View itemView) {
        super(itemView);
        mTextViewTripId = itemView.findViewById(R.id.txt_trip_id);
        mTextViewAddress = itemView.findViewById(R.id.txt_address);
        mTextViewDrop = itemView.findViewById(R.id.txt_drop);
        mTextViewDate = itemView.findViewById(R.id.txt_date);
        mTextViewPrice = itemView.findViewById(R.id.txt_price);
        mTextViewTripStatus = itemView.findViewById(R.id.txt_tripstatus);
        mTextViewOtp = itemView.findViewById(R.id.txt_otp);
        mAcceptLayout = itemView.findViewById(R.id.accept_layout);
        mAccept = itemView.findViewById(R.id.txt_accept);
        mReject = itemView.findViewById(R.id.txt_reject);
        mImageProfileStatus = itemView.findViewById(R.id.img_profile);
        morderType = itemView.findViewById(R.id.orderType);
        mTriptype = itemView.findViewById(R.id.txt_trip_type);


    }

    public TextView getmTriptype() {
        return mTriptype;
    }

    public void setmTriptype(TextView mTriptype) {
        this.mTriptype = mTriptype;
    }

    public TextView getMorderType() {
        return morderType;
    }

    public void setMorderType(TextView morderType) {
        this.morderType = morderType;
    }

    public TextView getTextViewTripId() {
        return mTextViewTripId;
    }

    public TextView getTextViewAddress() {
        return mTextViewAddress;
    }

    public TextView getmTextViewDrop() {
        return mTextViewDrop;
    }

    public TextView getTextViewExtras() {
        return mTextViewExtras;
    }

    public TextView getTextViewDate() {
        return mTextViewDate;
    }

    public TextView getTextViewPrice() {
        return mTextViewPrice;
    }

    public TextView getmTextViewTripStatus() {
        return mTextViewTripStatus;
    }

    public TextView getmTextViewOtp() {
        return mTextViewOtp;
    }

    public LinearLayout getmAcceptLayout() {
        return mAcceptLayout;
    }

    public TextView getmAccept() {
        return mAccept;
    }

    public TextView getmReject() {
        return mReject;
    }

    public CircleImageView getmImageProfileStatus() {
        return mImageProfileStatus;
    }
}


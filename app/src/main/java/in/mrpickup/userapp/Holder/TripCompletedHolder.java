package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.userapp.R;

/**
 * Created by Admin on 7/5/2017.
 */

public class TripCompletedHolder extends RecyclerView.ViewHolder {

    private TextView mTextViewTripId, mTextViewAddress, mTextViewDrop,
            mTextViewDate, mTextViewPrice, mTextViewName, mTextViewRate, mTriptype;
    private CircleImageView mImageViewProfile;

    public TripCompletedHolder(View itemView) {
        super(itemView);
        mTextViewTripId = itemView.findViewById(R.id.txt_trip_id);
        mTextViewAddress = itemView.findViewById(R.id.txt_address);
        mTextViewDrop = itemView.findViewById(R.id.txt_drop);
        mTextViewDate = itemView.findViewById(R.id.txt_date);
        mTextViewPrice = itemView.findViewById(R.id.txt_price);
        mTextViewRate = itemView.findViewById(R.id.txt_rate);
        mTriptype = itemView.findViewById(R.id.txt_trip_type);
    }

    public TextView getmTriptype() {
        return mTriptype;
    }

    public void setmTriptype(TextView mTriptype) {
        this.mTriptype = mTriptype;
    }

    public TextView getTextViewTripId() {
        return mTextViewTripId;
    }

    public TextView getTextViewAddress() {
        return mTextViewAddress;
    }

    public TextView getTextViewComments() {
        return mTextViewDrop;
    }


    public TextView getTextViewDate() {
        return mTextViewDate;
    }

    public TextView getTextViewPrice() {
        return mTextViewPrice;
    }


    public TextView getTextViewRate() {
        return mTextViewRate;
    }

}
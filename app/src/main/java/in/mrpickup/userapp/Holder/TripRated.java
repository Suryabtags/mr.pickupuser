package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import in.mrpickup.userapp.R;

/**
 * Created by Admin on 7/8/2017.
 */

public class TripRated extends RecyclerView.ViewHolder {

    private TextView mTextViewTripId, mTextViewAddress, mTextViewDrop, mTextViewExtras,
            mTextViewDate, mTextViewPrice, mTriptype;
    private RatingBar mRatingBar;
    private ImageView mImageViewCompletedstamp;

    public TripRated(View itemView) {
        super(itemView);
        mTextViewTripId = itemView.findViewById(R.id.txt_trip_id);
        mTextViewAddress = itemView.findViewById(R.id.txt_address);
        mTextViewDrop = itemView.findViewById(R.id.txt_drop);
        mTextViewDate = itemView.findViewById(R.id.txt_date);
        mTextViewPrice = itemView.findViewById(R.id.txt_price);
        mRatingBar = itemView.findViewById(R.id.rating);
        mImageViewCompletedstamp = itemView.findViewById(R.id.img_cancel);
        mTriptype = itemView.findViewById(R.id.txt_trip_type);

    }

    public TextView getmTriptype() {
        return mTriptype;
    }

    public void setmTriptype(TextView mTriptype) {
        this.mTriptype = mTriptype;
    }

    public TextView getTextViewTripId() {
        return mTextViewTripId;
    }

    public TextView getTextViewAddress() {
        return mTextViewAddress;
    }

    public TextView getTextViewDrop() {
        return mTextViewDrop;
    }

    public TextView getTextViewDate() {
        return mTextViewDate;
    }

    public TextView getTextViewPrice() {
        return mTextViewPrice;
    }


    public RatingBar getRatingBar() {
        return mRatingBar;
    }

    public ImageView getmImageViewCompletedstamp() {
        return mImageViewCompletedstamp;
    }

    public void setmImageViewCompletedstamp(ImageView mImageViewCompletedstamp) {
        this.mImageViewCompletedstamp = mImageViewCompletedstamp;
    }
}


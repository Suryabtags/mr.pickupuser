package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.mrpickup.userapp.R;
import in.mrpickup.userapp.callback.ProductCallback;

/**
 * Created by Bobby on 19/05/17
 */

public class RateCardHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_price, txt_extrakmprice, txt_extrakgprice;

    ProductCallback productCallback;

    public RateCardHolder(View itemView, ProductCallback mProductCallback) {
        super(itemView);
        txt_price = itemView.findViewById(R.id.txt_price);
        txt_extrakmprice = itemView.findViewById(R.id.txt_extrakmprice);
        txt_extrakgprice = itemView.findViewById(R.id.txt_extrakgprice);
        productCallback = mProductCallback;
    }

    @Override
    public void onClick(View v) {

    }


}

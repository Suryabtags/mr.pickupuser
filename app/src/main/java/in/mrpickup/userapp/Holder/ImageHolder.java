package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import in.mrpickup.userapp.R;

/**
 * Created by Bobby on 19/05/17
 */

public class ImageHolder extends RecyclerView.ViewHolder {

    public ImageView mImageViewProfile;
    public ImageView img_remove;

    public ImageHolder(View itemView) {
        super(itemView);
        mImageViewProfile = itemView.findViewById(R.id.img_media);
        img_remove = itemView.findViewById(R.id.img_remove);
    }

}

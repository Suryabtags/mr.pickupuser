package in.mrpickup.userapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.mrpickup.userapp.R;
import in.mrpickup.userapp.callback.ProductCallback;

/**
 * Created by Bobby on 19/05/17
 */

public class AddEditProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mCategory, mSubCategory, mQuantity;
    public ImageView mImageEdit;
    ProductCallback productCallback;

    public AddEditProductHolder(View itemView, ProductCallback mProductCallback) {
        super(itemView);
        mCategory = itemView.findViewById(R.id.category);
        mSubCategory = itemView.findViewById(R.id.subcategory);
        mQuantity = itemView.findViewById(R.id.quantity);
        mImageEdit = itemView.findViewById(R.id.img_edit);
        productCallback = mProductCallback;
    }

    @Override
    public void onClick(View v) {


    }


}

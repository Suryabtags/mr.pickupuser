package in.mrpickup.userapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.mrpickup.userapp.Adapter.ActiveProductListAdapter;
import in.mrpickup.userapp.Model.ActiveBookingItem;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.VolleyErrorHandler;
import in.mrpickup.userapp.callback.ProductCallback;

import static in.mrpickup.userapp.app.Api.KEY_FIRST_NAME;
import static in.mrpickup.userapp.helper.Utils.getCreatedDate;

public class ActiveOrderDetailsActivity extends AppCompatActivity implements View.OnClickListener, ProductCallback {


    public TextView orderId, pickAddr, dropAddr, date, name, category, delType,
            mStore, mOrderStatus, mMobile, mDelName, mDesc, mdist, mdcost, mTextViewProductWeight;
    public Context mContext;
    public PreferenceManager mPreference;
    ActiveProductListAdapter productEditListAdapter;
    ArrayList<ActiveBookingItem> mOrders = new ArrayList<>();
    int quantity, units, subId, CatId;
    String subName, morderIDshow;
    int Oid;
    ImageView img_back;
    ArrayList<ActiveBookingItem> mOrderArray = new ArrayList<>();
    String empId, empName, profile;
    private LinearLayout mStoreLayouts, mobile_layout, product_layout, descriptionLayout;
    private RecyclerView mProductsListview;
    private TextView mWaitingTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        processBundle();
        initObjects();
        initCallbacks();
    }

    private void initObjects() {

        mContext = this;
        mProductsListview = findViewById(R.id.products_listview);
        orderId = findViewById(R.id.order_id);
        img_back = findViewById(R.id.img_back);
        pickAddr = findViewById(R.id.pickupaddress);
        dropAddr = findViewById(R.id.dropaddress);
        date = findViewById(R.id.created_date);
        mTextViewProductWeight = findViewById(R.id.product_weight);
        name = findViewById(R.id.name);
        mWaitingTime = findViewById(R.id.waitingtime);
        category = findViewById(R.id.category);
        delType = findViewById(R.id.type);
        mStore = findViewById(R.id.store);
        mdist = findViewById(R.id.order_distance);
        mdcost = findViewById(R.id.order_deliverycost);
        mOrderStatus = findViewById(R.id.order_status);
        mStoreLayouts = findViewById(R.id.storelayout);
        mobile_layout = findViewById(R.id.mobile_layout);
        mMobile = findViewById(R.id.mobilenumber);
        product_layout = findViewById(R.id.product_layout);
        descriptionLayout = findViewById(R.id.descriptionLayout);
        mDesc = findViewById(R.id.txt_descr);
        mDelName = findViewById(R.id.del_name);
        mPreference = new PreferenceManager(mContext);
        orderId.setText(String.valueOf(morderIDshow));
        getMethod();

    }


    private void initCallbacks() {
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            onBackPressed();
        }

    }

    private void processBundle() {
        if (getIntent().getExtras() != null) {

            Oid = getIntent().getExtras().getInt("ORDER_ID");
            morderIDshow = getIntent().getExtras().getString("orderidshow");
        }

    }


    @Override
    public void onItemRemoveClick(int position) {


    }

    @Override
    protected void onResume() {
        getMethod();
        super.onResume();
    }

    private void getMethod() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "http://mrpickup.zordec.com/orders/" + Oid + "/";
        JsonObjectRequest jsonObjectRequest = new
                JsonObjectRequest(Request.Method.GET,
                        url,
                        null,
                        new com.android.volley.Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                handleOrdersList(response);
                                Log.e("response", "" + response.toString());
                            }
                        },
                        new com.android.volley.Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                VolleyErrorHandler.handle(mContext, error);
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", "Token " + mPreference.getToken());
                        return headers;
                    }

                };

        requestQueue.add(jsonObjectRequest);
    }

    private void handleOrdersList(JSONObject response) {

        String pickMob, Storename, Description, mDistance, mdeliveryCost, CatName = "";
        int statys;
        boolean pictype;
        try {
            JSONObject jsonObject = new JSONObject(response.toString());

            pickAddr.setText(jsonObject.getString("pickup_loc"));
            name.setText(jsonObject.getString("pickup_name"));
            dropAddr.setText(jsonObject.getString("del_loc"));
            mDelName.setText(jsonObject.getString("del_name"));
            date.setText(getCreatedDate(jsonObject.getString("created_on")));
            pickMob = jsonObject.getString("pickup_mob");
            Storename = jsonObject.getString("store_name");
            pictype = jsonObject.getBoolean("pickup_type");
            JSONObject orderDetails = jsonObject.getJSONObject("orderdetails");
            JSONObject statusObj = orderDetails.getJSONObject("status");
            statys = statusObj.getInt("id");
            String deliveryType = jsonObject.getString("delivery_type");
            Description = jsonObject.getString("descriptions");
            mDistance = orderDetails.getString("approx_dist");
            mdeliveryCost = orderDetails.getString("delivery_cost");
            JSONArray userObj = jsonObject.getJSONArray(Api.KEY_ORDER_ASSIGN);

            for (int j = 0; j < userObj.length(); j++) {
                JSONObject userObj22 = userObj.getJSONObject(j);
                JSONObject KeyNameObj = userObj22.getJSONObject(Api.KEY_EMP_ASSIGN);
                empId = KeyNameObj.getString(Api.KEY_ID);
                empName = KeyNameObj.getString(KEY_FIRST_NAME);
                profile = KeyNameObj.getString(Api.KEY_PROFILE_PIC);
            }
            boolean waiting = orderDetails.getBoolean("is_waiting");
            if (waiting) {
                mWaitingTime.setVisibility(View.VISIBLE);
            } else {
                mWaitingTime.setVisibility(View.GONE);

            }
            mOrderArray.clear();
            JSONArray productObj = jsonObject.getJSONArray("categories");
            for (int k = 0; k < productObj.length(); k++) {
                JSONObject ProductnameObj = productObj.getJSONObject(k);
                JSONObject jsonObject1 = ProductnameObj.getJSONObject("category");
                CatName = jsonObject1.getString("name");
                CatId = jsonObject1.getInt("id");
                for (int m = 0; m < ProductnameObj.length(); m++) {
                    JSONArray jsonArray = ProductnameObj.getJSONArray("products");
                    for (int y = 0; y < jsonArray.length(); y++) {
                        JSONObject ProductnameObj1 = jsonArray.getJSONObject(y);
                        quantity = ProductnameObj1.getInt("units");
                        units = ProductnameObj1.getInt("quantity");
                        subId = ProductnameObj1.getInt("id");
                        subName = ProductnameObj1.getString("name");
                    }
                }
                mOrderArray.add(new ActiveBookingItem(quantity, units, subName, subId, CatName, CatId));
                mProductsListview.setVisibility(View.VISIBLE);
                productEditListAdapter = new ActiveProductListAdapter(mContext, mOrderArray, this);
                mProductsListview.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true));
                mProductsListview.setHasFixedSize(true);
                mProductsListview.setScrollBarSize(0);
                mProductsListview.setAdapter(productEditListAdapter);
                productEditListAdapter.notifyDataSetChanged();

            }
            initSetText(mDistance, pickMob, statys, pictype, Storename, Description,
                    mdeliveryCost, CatName, deliveryType);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void initSetText(String mDistance,
                            String pickMob, int status, boolean pickType, String Storename, String Description,
                            String deliverycost, String category12, String dtype) {

        if (dtype.equals("UR")) {
            mTextViewProductWeight.setText("Urgent");
        } else {
            mTextViewProductWeight.setText("Ordinary");
        }
        if (status == 1) {
            mOrderStatus.setText("Order Placed");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (status == 2) {
            mOrderStatus.setText("Employee Assigned");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (status == 4) {
            mOrderStatus.setText("Completed");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (status == 5) {
            mOrderStatus.setText("Trip End");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (status == 6) {
            mOrderStatus.setText(" User Cancelled");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        } else if (status == 7) {
            mOrderStatus.setText("Trip Started");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (status == 8) {
            mOrderStatus.setText("Employee Rejected");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        } else if (status == 9) {
            mOrderStatus.setText("Pending");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
        float dist = Float.valueOf(mDistance);
        mdist.setText(String.format("%s km", String.format("%.1f", dist)));
        mdcost.setText(String.format("₹%s", deliverycost));

        if (pickMob != null) {
            mMobile.setText(pickMob);
            mobile_layout.setVisibility(View.VISIBLE);
        } else {
            mobile_layout.setVisibility(View.GONE);
        }
        if (category12 != null) {
            category.setText(category12);
            product_layout.setVisibility(View.VISIBLE);
        } else {
            product_layout.setVisibility(View.GONE);
        }


        if (pickType) {
            delType.setText("PickUp & Delivery");
            mStoreLayouts.setVisibility(View.GONE);

        } else {
            delType.setText("Buy & Delivery");

            if (Storename != null && !Storename.isEmpty()) {
                mStoreLayouts.setVisibility(View.VISIBLE);
                mStore.setText(Storename);
            }

        }
        if (!Description.isEmpty() && !Description.equals("null")) {
            mDesc.setText(Description);
            descriptionLayout.setVisibility(View.VISIBLE);
        } else {
            descriptionLayout.setVisibility(View.GONE);
        }
    }
}

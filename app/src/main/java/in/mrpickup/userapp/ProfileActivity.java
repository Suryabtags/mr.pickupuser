package in.mrpickup.userapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.userapp.Model.ProfileView;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static in.mrpickup.userapp.app.Activity.launch;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {


    TextView usr_name, usr_phone, usr_mail, usr_address;
    private CircleImageView mImageViewProfile;
    private ImageView profile_edit, img_back;
    private PreferenceManager mPreference;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initObjects();
        initCallbacks();


        if (NetworkError.getInstance(mContext).isOnline()) {
            getProfilemethod();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }


    }

    private void initCallbacks() {

        img_back.setOnClickListener(this);
        profile_edit.setOnClickListener(this);
    }


    private void initObjects() {
        mContext = this;

        mImageViewProfile = findViewById(R.id.img_profile);
        img_back = findViewById(R.id.img_back);
        profile_edit = findViewById(R.id.profile_edit);
        usr_name = findViewById(R.id.usr_name);
        usr_phone = findViewById(R.id.usr_phone);
        usr_mail = findViewById(R.id.usr_mail);
        usr_address = findViewById(R.id.usr_address);
        mPreference = new PreferenceManager(this);

    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            onBackPressed();
        } else if (v == profile_edit) {
            launch(mContext, EditProfileActivity.class);

        }
    }

    public void getProfilemethod() {
        ApiInterface apiService = Api.getClient().create(ApiInterface.class);
        Call<ProfileView> call = apiService.getProfile("Token " + mPreference.getToken());
        call.enqueue(new Callback<ProfileView>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ProfileView> call, Response<ProfileView> response) {
                ProfileView profileView = response.body();
                if (response.isSuccessful() && profileView != null) {
                    usr_name.setText(profileView.getFirst_name());
                    usr_phone.setText(profileView.getUsername());
                    usr_mail.setText(profileView.getEmail());

                    if (profileView.getUseraddress() != null) {
                        if (profileView.getUseraddress().getStreet() != null &&
                                profileView.getUseraddress().getCity() != null &&
                                profileView.getUseraddress().getCountry() != null &&
                                profileView.getUseraddress().getState() != null &&
                                profileView.getUseraddress().getZipcode() != null &&
                                profileView.getUseraddress().getArea() != null) {
                            usr_address.setVisibility(View.VISIBLE);
                            usr_address.setText(profileView.getUseraddress().getStreet() + ", " +
                                    profileView.getUseraddress().getCity() + ", " +
                                    profileView.getUseraddress().getArea() + ", " +
                                    profileView.getUseraddress().getState() + ", " +
                                    profileView.getUseraddress().getCountry() + ", " +
                                    profileView.getUseraddress().getZipcode());
                        }


                    }

                    String imageloader = profileView.getUserprofile().getProfile_pic();
                    if (imageloader != null) {
                        Glide.with(mContext).load(imageloader)
                                .apply(new RequestOptions().placeholder(R.drawable.man).error(R.drawable.man))
                                .into(mImageViewProfile);
                    }

                }
            }

            @Override
            public void onFailure(Call<ProfileView> call, Throwable t) {
                Log.e("failuretokenprofile", "" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        getProfilemethod();
        super.onResume();
    }
}



package in.mrpickup.userapp;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.widget.TextView;

import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.PreferenceManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Splash extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    private Context mContext;
    private TextView mTextViewFooter;
    private PreferenceManager mPreferenceManager;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().getDecorView().setBackgroundColor(Color.WHITE);
        initObjects();
        setFooterTextSpan();
        checkUserSession();
    }

    private void initObjects() {
        mTextViewFooter = findViewById(R.id.txt_footer);
        mPreferenceManager = new PreferenceManager(this);
        mContext = this;
    }

    private void setFooterTextSpan() {
        ImageSpan imageSpan = new ImageSpan(mContext, R.drawable.ic_heart,
                DynamicDrawableSpan.ALIGN_BASELINE);
        SpannableString string = new SpannableString("Copy rights by Mr.Pickup");
        string.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.colorAccent)),
                12, 24, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextViewFooter.setText(string);
    }

    private void checkUserSession() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mPreferenceManager.getToken() == null) {
                    Activity.launch(mContext, ChooseActivity.class);
                } else {
                    Activity.launchClearStack(mContext, PickUpActivity.class);
                }
            }
        }, 1000);
    }
}


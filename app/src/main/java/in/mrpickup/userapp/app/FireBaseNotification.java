package in.mrpickup.userapp.app;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import in.mrpickup.userapp.OrderDetailsActivity;
import in.mrpickup.userapp.R;
import in.mrpickup.userapp.TripsActivity;

import static in.mrpickup.userapp.app.Api.KEY_DESCRIPTION;
import static in.mrpickup.userapp.app.Api.KEY_ORDERIDNOTIFY;
import static in.mrpickup.userapp.app.Api.KEY_TICKER;
import static in.mrpickup.userapp.app.Api.KEY_TITLE;


public class FireBaseNotification extends FirebaseMessagingService {
    private static final String NOTIFY_ID = "Mr-Pickup";
    private static final String NOTIFY_CHANNEL = "Mr.Pickup";

    int orderid;
    String status;
    Intent intent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        String ticker = null;
        String title = null;
        String description = null;

        PreferenceManager preferenceManager = new PreferenceManager(this);
        try {
            ticker = jsonObject.getString(KEY_TICKER);
            title = jsonObject.getString(KEY_TITLE);
            description = jsonObject.getString(KEY_DESCRIPTION);
            orderid = jsonObject.getInt(KEY_ORDERIDNOTIFY);
            status = jsonObject.getString("status");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e("preferenceManage", "" + preferenceManager.getToken());
        if (preferenceManager.getToken() != null) {
            displayNotification(ticker, title, description, orderid, status);
        }
    }

    private void displayNotification(String ticker, String title, String description, int orderId, String status) {
        if (status.equals("1") || status.equals("9")) {
            intent = new Intent(this, OrderDetailsActivity.class);
            intent.putExtra("ORDER_ID", orderId);
        } else if (status.equals("2") || status.equals("3") || status.equals("5") || status.equals("7")) {
            intent = new Intent(this, OrderDetailsActivity.class);
            intent.putExtra("ORDER_ID", orderId);
        } else {
            intent = new Intent(this, TripsActivity.class);
        }
        PendingIntent pendingIntentContent = PendingIntent.getActivity(this, orderId,
                intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.finallogo);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFY_ID);
        builder.setTicker(ticker)
                .setContentTitle(title)
                .setContentText(description)
                .setContentIntent(pendingIntentContent)
                .setSmallIcon(R.drawable.finallogo)
                .setLargeIcon(bitmapLargeIcon)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(description))
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL);


        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFY_ID, NOTIFY_CHANNEL,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify(orderId, builder.build());
        }

    }
}

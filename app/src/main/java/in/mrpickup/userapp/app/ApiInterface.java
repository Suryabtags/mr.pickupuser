package in.mrpickup.userapp.app;


import java.util.ArrayList;

import in.mrpickup.userapp.Model.ChangeStatus;
import in.mrpickup.userapp.Model.DeliveryBoyProfileView;
import in.mrpickup.userapp.Model.EditProduct;
import in.mrpickup.userapp.Model.FcmToken;
import in.mrpickup.userapp.Model.OffersList;
import in.mrpickup.userapp.Model.OrderDetailItem;
import in.mrpickup.userapp.Model.PendingOrderList;
import in.mrpickup.userapp.Model.Products;
import in.mrpickup.userapp.Model.ProductsList;
import in.mrpickup.userapp.Model.ProfileView;
import in.mrpickup.userapp.Model.Rates;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Url;


/**
 * Created by admin on 10/26/2017.
 */

public interface ApiInterface {


    @GET("customers/")
    Call<ProfileView> getProfile(@Header("Authorization") String token);

    @GET("rates/")
    Call<ArrayList<Rates>> getRates(@Header("Authorization") String token);

    @GET("rates/product/")
    Call<ArrayList<Products>> getProductList();

    @GET("orders/unit/")
    Call<ArrayList<Products>> getUnits();

    @Multipart
    @PUT("customers/update_profile/")
    Call<ProfileView> getEditProfile(@Header("Authorization") String token,
                                     @Part("first_name") RequestBody firstname,
                                     @Part("email") RequestBody email,
                                     @Part("username") RequestBody username,
                                     @Part("useraddress") RequestBody jsonObject,
                                     @Part MultipartBody.Part profile_pic);

    @Multipart
    @PUT("customers/update_profile/")
    Call<ProfileView> getEditProfile1(@Header("Authorization") String token,
                                      @Part("first_name") RequestBody firstname,
                                      @Part("email") RequestBody email,
                                      @Part("username") RequestBody username,
                                      @Part("useraddress") RequestBody jsonObject);

    @GET("orders/my_order/?status=1")
    Call<PendingOrderList> OrderList(@Header("Authorization") String token);


    @PUT
    Call<Void> getAcceptOption(@Url String url, @Header("Authorization") String token, @Body ChangeStatus status);


    @DELETE
    Call<Void> deleteAddress(@Url String url, @Header("Authorization") String token);

    @GET
    Call<ArrayList<OffersList>> getListService(@Url String url, @Header("Authorization") String token);


    @POST
    Call<DeliveryBoyProfileView> getDeliveryboyProfile(@Url String url, @Header("Authorization") String token);

    @PUT("customers/update_device_token/")
    Call<Void> updateFcm(@Header("Authorization") String token, @Body FcmToken fcmToken);


    @GET("orders/my_order/?status=1")
    Call<ArrayList<ProductsList>> getProducts(@Header("Authorization") String token);

    @PUT
    Call<EditProduct> UpdateProducts(@Header("Authorization") String token, @Url String mUrl, @Body EditProduct editProduct);


    @GET
    Call<OrderDetailItem> getAcceptOption(@Url String url, @Header("Authorization") String token);



}

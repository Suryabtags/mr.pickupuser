package in.mrpickup.userapp.app;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class Api {
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_PICK_NAME = "pickup_name";
    public static final String KEY_DEL_NAME = "del_name";
    public static final String KEY_STORE_NAME = "store_name";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_DETAIL = "detail";
    public static final String KEY_OTP = "otp";
    public static final String KEY_CLIENT = "client";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_USERID = "id";
    public static final String KEY_ROLE = "role";
    public static final String KEY_PICKUP_NAME = "pickup_name";
    public static final String KEY_PICKLONG = "pickup_long";
    public static final String KEY_PICKLAT = "pickup_lat";
    public static final String KEY_PICKUP_MOB = "pickup_mob";
    public static final String KEY_PRODUCT = "product_name";
    public static final String KEY_PRODUCT_WEIGHT = "product_weight";
    public static final String KEY_PROFILE_PIC = "profile_pic";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_DESC = "descriptions";
    public static final String KEY_DELIVERY_NAME = "del_name";
    public static final String KEY_DELIVER_LONG = "del_long";
    public static final String KEY_DELIVER_LAT = "del_lat";
    public static final String KEY_DELIVER_MOB = "del_mob";
    public static final String KEY_PICKUP_TYPE = "pickup_type";
    public static final String KEY_PICKUP_TIME = "pickup_time";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_PICKUP_LOC = "pickup_loc";
    public static final String KEY_DELIVER_LOC = "del_loc";
    public static final String KEY_ORDER_DETAILS = "orderdetails";
    public static final String KEY_PICKUP_OTP = "pickup_otp";
    public static final String KEY_DELIVER_OTP = "delivery_otp";
    public static final String KEY_STATUS = "status";
    public static final String KEY_ORDER_ASSIGN = "order_assignment";
    public static final String KEY_EMP_ASSIGN = "assign_employee";
    public static final String KEY_USERPROFILE = "userprofile";
    public static final String KEY_RATING = "rating";
    public static final String KEY_NON_FIELD_ERRORS = "non_field_errors";
    public static final String KEY_TICKER = "ticker";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESCRIPTION = "body";
    public static final String KEY_FCM_TOKEN = "device_token";
    public static final String KEY_COLOR = "color";
    public static final String KEY_EMI_MODE = "emi_mode";
    public static final String KEY_THEME = "theme";
    public static final String KEY_PREFILL = "prefill";
    public static final String KEY_NAME = "name";
    public static final String KEY_RAZORPAY_ORDER_ID = "razorpay_order_id";
    public static final String KEY_RAZORPAY_PAYMENT_ID = "razorpay_payment_id";
    public static final String KEY_RAZORPAY_SIGNATURE = "razorpay_signature";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_ORDER_ID = "order_id";
    public static final String KEY_CURRENCY = "currency";
    public static final String KEY_BOOKING_ID = "booking_id";
    public static final String KEY_LIST_ACTIVE_ORDERS = "status";
    public static final String KEY_STATUS_ID = "id";
    public static final String KEY_DELIVERY_COST = "delivery_cost";
    public static final String KEY_PRODUCT_COST = "product_cost";
    public static final String KEY_PICK_TYPE = "pickup_type";
    public static final String KEY_MOBILENUMBER = "mobile_number";
    public static final String KEY_STORENAME = "store_name";
    public static final String KEY_STORECODE = "store_code";
    public static final String KEY_ORDERIDNOTIFY = "order_id";
    public static final String KEY_ORDERIDSHOW = "order_id";


    public static final String URL = "http://mrpickup.zordec.com/";
    //public static final String URL = "http://192.168.1.120:8000/";


    public static final String REGISTER_URL = URL + "oauth/registration/";
    public static final String ACTIVATE_URL = URL + "oauth/activate/account/";
    public static final String FORGOT_PASS = URL + "oauth/forgot-password/";
    public static final String LOGIN_URL = URL + "oauth/login/";
    public static final String LOGOUT_URL = URL + "oauth/logout/";
    public static final String RESET_PASS = URL + "oauth/reset-password/";
    public static final String CREATE_ORDER = URL + "orders/";
    public static final String RATING = URL + "customers/";
    public static final String PAYMENT_GATEWAY = URL + "orders/payment/";
    public static final String LIST_ORDER = URL + "orders/my_order/?status=2";
    public static final String PENDING_ORDER = URL + "orders/my_order/?status=1";
    public static final String RETRIVE_ORDER = URL + "orders/";
    public static final String COMPLETED_ORDER = URL + "orders/my_order/?status=3";
    public static final String UPDATE_TOKEN_URL = URL + "customers/update_device_token/";
    public static final String PAYMENT_SUCCESS = URL + "orders/payment/";
    public static final String KEY_COMMENTS = "comments";
    public static String KEY_CATEGORY2 = "category";
    public static String KEY_PAYMENTTYPE = "payment_type";
    public static String KEY_DELIVERYTYPE = "delivery_type";
    public static String KEY_EDITPRODUCT = "products";
    /**
     * Api Keys
     */
    public static String KEY_ID = "id";


    private static Retrofit sRetrofit = null;

    public static Retrofit getClient() {
        if (sRetrofit == null) {
            sRetrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return sRetrofit;
    }


//old Url And Api list
//
//
//
//    public static final String URL = "http://mr-pickup.billioncart.in/";
//    public static final String REGISTER_URL = URL + "oauth/register/";
//    public static final String ACTIVATE_URL = URL + "oauth/activate/account/";
//    public static final String FORGOT_PASS = URL + "oauth/forgot-password/";
//    public static final String LOGIN_URL = URL + "oauth/login/";
//    public static final String CANCEL_BOOKING = URL + "admin/update/book/status/";
//    public static final String LOGOUT_URL = URL + "oauth/logout/";
//    public static final String RESET_PASS = URL + "/oauth/reset-password/";
//    public static final String CREATE_ORDER = URL + "user/create/order/";
//    public static final String RATING = URL + "user/submit/rating/";
//    public static final String PAYMENT_GATEWAY = URL + "/user/payment-gateway/";
//    public static final String LIST_ORDER = URL + "user/list/order/?status=0";
//    public static final String COMPLETED_ORDER = URL + "user/list/order/?status=1";
//    public static final String UPDATE_TOKEN_URL = URL + "user/update/user/token/";
//    public static final String PAYMENT_SUCCESS = URL + "user/payment-success/";

}
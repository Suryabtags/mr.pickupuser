package in.mrpickup.userapp.app;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Admin on 7/8/2017.
 */

public class FireBaseServices extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("fcm-token", token);
        storeToken(token);
    }

    private void storeToken(String token) {
        PreferenceManager myPreference = new PreferenceManager(this);
        myPreference.setFcmToken(token);
        myPreference.setTokenUploaded(false);
    }
}

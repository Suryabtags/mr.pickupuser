package in.mrpickup.userapp.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by Bobby on 18-02-2017
 */

public class PreferenceManager {

    private static final String PREF_USER = "user";
    private static final String PREF_LOCATION = "location";
    private static final String PREF_EXTRAS = "extras";
    private static final String USER_ID = "user_id";
    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String TOKEN = "token";
    private static final String PHONE = "phone";
    private static final String LAT = "lat";
    private static final String LNG = "lng";
    private static final String ABOUT_US = "about_us";
    private static final String BOOKING_OPTIONS = "booking_options";
    private static final String BOOKING = "booking";
    private static final String ORDER_ID = "order_id";

    private static final String FCM_TOKEN = "device_token";
    private static final String TOKEN_UPLOADED = "token_uploaded";
    private SharedPreferences mPreferencesUser, mPreferencesLocation, mPreferencesExtras;

    public PreferenceManager(Context context) {
        mPreferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        mPreferencesLocation = context.getSharedPreferences(PREF_LOCATION, Context.MODE_PRIVATE);
        mPreferencesExtras = context.getSharedPreferences(PREF_EXTRAS, Context.MODE_PRIVATE);
    }

    public int getUserId() {
        return mPreferencesUser.getInt(encode(USER_ID), -1);
    }

    public void setUserId(int userId) {
        mPreferencesUser.edit().putInt(encode(USER_ID), userId).apply();
    }

    public String getName() {
        return mPreferencesUser.getString(encode(NAME), null);
    }

    public void setName(String name) {
        mPreferencesUser.edit().putString(encode(NAME), name).apply();
    }

    public String getEmail() {
        return mPreferencesUser.getString(encode(EMAIL), null);
    }

    public void setEmail(String email) {
        mPreferencesUser.edit().putString(encode(EMAIL), email).apply();
    }

    public String getToken() {
        return mPreferencesUser.getString(encode(TOKEN), null);
    }

    public void setToken(String token) {
        mPreferencesUser.edit().putString(encode(TOKEN), token).apply();
    }

    public String getPhone() {
        return mPreferencesUser.getString(encode(PHONE), null);
    }

    public void setPhone(String phone) {
        mPreferencesUser.edit().putString(encode(PHONE), phone).apply();
    }

    public String getLat() {
        return mPreferencesLocation.getString(encode(LAT), String.valueOf(Constant.DEFAULT_LAT));
    }

    public void setLat(String lat) {
        mPreferencesLocation.edit().putString(encode(LAT), lat).apply();
    }

    public String getLng() {
        return mPreferencesLocation.getString(encode(LNG), String.valueOf(Constant.DEFAULT_LNG));
    }

    public void setLng(String lng) {
        mPreferencesLocation.edit().putString(encode(LNG), lng).apply();
    }

    public String getAboutUs() {
        return mPreferencesExtras.getString(encode(ABOUT_US), null);
    }

    public void setAboutUs(String aboutUs) {
        mPreferencesExtras.edit().putString(encode(ABOUT_US), aboutUs).apply();
    }

    public String getBookingOptions() {
        return mPreferencesExtras.getString(encode(BOOKING_OPTIONS), null);
    }

    public void setBookingOptions(String bookingOptions) {
        mPreferencesExtras.edit().putString(encode(BOOKING_OPTIONS), bookingOptions).apply();
    }

    public String getBooking() {
        return mPreferencesExtras.getString(encode(BOOKING), null);
    }

    public void setBooking(String booking) {
        mPreferencesExtras.edit().putString(encode(BOOKING), booking).apply();
    }

    public boolean isTokenUploaded() {
        return mPreferencesExtras.getBoolean(encode(TOKEN_UPLOADED), false);
    }

    public void setTokenUploaded(boolean tokenUploaded) {
        mPreferencesExtras.edit().putBoolean(encode(TOKEN_UPLOADED), tokenUploaded).apply();
    }

    public String getFcmToken() {
        return mPreferencesExtras.getString(encode(FCM_TOKEN), null);
    }

    void setFcmToken(String fcmToken) {
        mPreferencesExtras.edit().putString(encode(FCM_TOKEN), fcmToken).apply();
    }

    public void clearUser() {
        setTokenUploaded(false);
        mPreferencesUser.edit().clear().apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }

    public int getOrderId() {
        return mPreferencesUser.getInt(encode(ORDER_ID), -1);
    }

    public void setOrderId(int id) {
        mPreferencesUser.edit().putInt(encode(ORDER_ID), id).apply();
    }

}
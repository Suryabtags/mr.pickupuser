package in.mrpickup.userapp.app;

public class Constant {
    /**
     * Call us
     */
    public static final String EXTRA_PDUS = "pdus";
    public static final String OTP_SENDER_ID = "PICKUP";
    public static final String EXTRA_FORMAT = "format";
    public static final String SUPPORT_PHONE = "+91 8344114411";
    public static final String EXTRA_BOOKING = "booking";
    public static final String TRIP_ARRAY = "item";
    public static final int ROLE = 5;
    /**
     * Location Settings
     */
    public static final double DEFAULT_LAT = 13.0306;
    public static final double DEFAULT_LNG = 80.2380;
    public static final int ORDER_PLACED = 1;
    public static final int EMPLOYEE_ASSIGNED = 2;
    public static final int PICKUP_BUY = 3;
    public static final int DELIVERED = 4;
    public static final int PAYMENT = 5;
    public static final int CANCEL = 6;
    public static final int STARTED = 7;
    public static final int RATED = 10;
    public static final int ORDER_PENDING = 9;

    public static final int PAID = 11;
    public static final String KEY_EMPID = "empid";
    public static final int RESULT_CODE = 143;
    public static int UPDATE_INTERVAL = 30000;
    public static int FATEST_INTERVAL = 10000;
    public static int DISPLACEMENT = 10;
}

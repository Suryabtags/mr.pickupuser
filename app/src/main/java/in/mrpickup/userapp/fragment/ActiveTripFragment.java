package in.mrpickup.userapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.razorpay.Checkout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.mrpickup.userapp.ActiveOrderDetailsActivity;
import in.mrpickup.userapp.Adapter.ActiveBookingAdapter;
import in.mrpickup.userapp.DeliveryBoyProfileActivity;
import in.mrpickup.userapp.Model.ActiveBookingItem;
import in.mrpickup.userapp.R;
import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;

import static in.mrpickup.userapp.app.Activity.launchWithBundle;
import static in.mrpickup.userapp.app.Api.KEY_AMOUNT;
import static in.mrpickup.userapp.app.Api.KEY_COLOR;
import static in.mrpickup.userapp.app.Api.KEY_CURRENCY;
import static in.mrpickup.userapp.app.Api.KEY_DESCRIPTION;
import static in.mrpickup.userapp.app.Api.KEY_EMI_MODE;
import static in.mrpickup.userapp.app.Api.KEY_FIRST_NAME;
import static in.mrpickup.userapp.app.Api.KEY_NAME;
import static in.mrpickup.userapp.app.Api.KEY_PREFILL;
import static in.mrpickup.userapp.app.Api.KEY_STATUS_ID;
import static in.mrpickup.userapp.app.Api.KEY_THEME;
import static in.mrpickup.userapp.app.Api.KEY_USERNAME;
import static in.mrpickup.userapp.app.Api.LIST_ORDER;
import static in.mrpickup.userapp.helper.Utils.getCreatedDate;

public class ActiveTripFragment extends Fragment implements
        ActiveBookingAdapter.ActiveBookingListener {

    private static final String KEY_EMPID = "empid";
    String empId, phone, empName, profile, productname;
    int quantity, units, subId, CatId;
    String subName, CatName;
    String PImage, dImage;
    private Context mContext;
    private View mRootView;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private ArrayList<ActiveBookingItem> mActiveBookingItemList;
    private ActiveBookingAdapter mAdapter;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;

    public ActiveTripFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_active, container, false);
        initObjects();
        setupRecyclerView();
        setupRefresh();
        refreshList();
        return mRootView;
    }

    @Override
    public void onCallPorterClick(String phone) {
        Activity.launchDialPad(mContext, phone);
    }

    @Override
    public void onResume() {
        refreshList();
        super.onResume();
    }

    @Override
    public void onPayNowClick(int id) {
        showProgressDialog("Processing..");
        paymentGateway(id);
    }

    @Override
    public void onTripClick(int pos) {
        Intent intent = new Intent(mContext, ActiveOrderDetailsActivity.class);
        intent.putExtra("ORDER_ID", mActiveBookingItemList.get(pos).getId());
        intent.putExtra("orderidshow", mActiveBookingItemList.get(pos).getmOrderIDShow());
        startActivity(intent);
    }

    @Override
    public void onDetailProfileView(String id) {

        Bundle bundle = new Bundle();
        bundle.putString(KEY_EMPID, id);
        launchWithBundle(mContext, DeliveryBoyProfileActivity.class, bundle);

    }

    private void initObjects() {
        mRefreshLayout = mRootView.findViewById(R.id.refresh);
        mRecyclerView = mRootView.findViewById(R.id.list_trip_active);
        mContext = getActivity();
        mActiveBookingItemList = new ArrayList<>();
        mAdapter = new ActiveBookingAdapter(mContext, mActiveBookingItemList, this);
        mPreferenceManager = new PreferenceManager(mContext);
        mProgressDialog = new ProgressDialog(mContext);

    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);

    }

    private void setupRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        });
    }

    private void refreshList() {
        mRefreshLayout.setRefreshing(true);
        getActiveTrips();
    }

    private void getActiveTrips() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                LIST_ORDER, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                mRefreshLayout.setRefreshing(false);
                if (isAdded()) {
                    handleOrdersList(response);
                    if (mActiveBookingItemList.isEmpty()) {
                        ToastBuilder.build(mContext, "No Orders Yet");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mRefreshLayout.setRefreshing(false);
                VolleyErrorHandler.handle(mContext, error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreferenceManager.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, "Orders_List");
    }

    private void handleOrdersList(JSONArray response) {
        mActiveBookingItemList.clear();

        for (int i = 0; i < response.length(); i++) {
            try {

                JSONObject jsonObject = response.getJSONObject(i);
                int orderId = jsonObject.getInt(Api.KEY_ID);
                String pickLoc = jsonObject.getString(Api.KEY_PICKUP_LOC);
                String delLoc = jsonObject.getString(Api.KEY_DELIVER_LOC);
                JSONObject orderDetails = jsonObject.getJSONObject(Api.KEY_ORDER_DETAILS);
                String pickOtp = orderDetails.getString(Api.KEY_PICKUP_OTP);
                String delOtp = orderDetails.getString(Api.KEY_DELIVER_OTP);
                String StoreName = jsonObject.getString(Api.KEY_STORE_NAME);
                int price = orderDetails.getInt(Api.KEY_DELIVERY_COST);
                double product_cost = orderDetails.getDouble(Api.KEY_PRODUCT_COST);
                boolean isWaiting = orderDetails.getBoolean("is_waiting");
                String orderIdShow = jsonObject.getString(Api.KEY_ORDERIDSHOW);

                JSONObject statusObj = jsonObject.getJSONObject(Api.KEY_STATUS);
                int status = statusObj.getInt(KEY_STATUS_ID);
                boolean PickType = jsonObject.getBoolean(Api.KEY_PICK_TYPE);
                String date = getCreatedDate(jsonObject.getString("created_on"));
                String pickupName = jsonObject.getString(Api.KEY_PICK_NAME);
                String DelName = jsonObject.getString(Api.KEY_DEL_NAME);
                String Distance = orderDetails.getString("approx_dist");
                String deliverycost = orderDetails.getString("delivery_cost");
                String mProductWeight = jsonObject.getString("product_weight");

                JSONArray userObj = jsonObject.getJSONArray(Api.KEY_ORDER_ASSIGN);
                for (int j = 0; j < userObj.length(); j++) {
                    JSONObject userObj22 = userObj.getJSONObject(j);
                    JSONObject KeyNameObj = userObj22.getJSONObject(Api.KEY_EMP_ASSIGN);
                    empId = KeyNameObj.getString(Api.KEY_ID);
                    empName = KeyNameObj.getString(KEY_FIRST_NAME);
                    phone = KeyNameObj.getString(KEY_USERNAME);
                    profile = KeyNameObj.getString(Api.KEY_PROFILE_PIC);
                }

                JSONArray productObj = jsonObject.getJSONArray("categories");
                for (int k = 0; k < productObj.length(); k++) {
                    JSONObject ProductnameObj = productObj.getJSONObject(k);
                    JSONArray jsonArray = ProductnameObj.getJSONArray("products");
                    JSONObject jsonObject1 = ProductnameObj.getJSONObject("category");
                    CatName = jsonObject1.getString("name");
                    CatId = jsonObject1.getInt("id");
                    for (int y = 0; y < jsonArray.length(); y++) {
                        JSONObject ProductnameObj1 = jsonArray.getJSONObject(y);
                        quantity = ProductnameObj1.getInt("units");
                        units = ProductnameObj1.getInt("quantity");
                        subId = ProductnameObj1.getInt("id");
                        subName = ProductnameObj1.getString("name");
                    }

                }

                Log.e("status", "" + status);
                JSONArray pickup_image = jsonObject.getJSONArray("pickup_image");
                for (int g = 0; g < pickup_image.length(); g++) {
                    JSONObject pickupImage = pickup_image.getJSONObject(g);
                    PImage = pickupImage.getString("image");
                }
                JSONArray delivery_image = jsonObject.getJSONArray("delivery_image");
                for (int h = 0; h < delivery_image.length(); h++) {
                    JSONObject pickupImage = delivery_image.getJSONObject(h);
                    dImage = pickupImage.getString("image");
                }
                String deliveryType = jsonObject.getString("delivery_type");
                String Description = jsonObject.getString("descriptions");
                if (!jsonObject.isNull(Api.KEY_ORDER_ASSIGN)) {
                    mActiveBookingItemList.add(new ActiveBookingItem(orderId, empName, phone, pickLoc, delLoc,
                            status, delOtp, pickOtp, date, price,
                            PickType, profile, empId, productname, pickupName, DelName, StoreName,
                            quantity, units, subName, subId, CatName, CatId, deliveryType, product_cost,
                            Description, Distance, deliverycost, mProductWeight, isWaiting, PImage, dImage, orderIdShow));
                } else
                    mActiveBookingItemList.add(new ActiveBookingItem(orderId, status, delOtp, pickOtp, pickLoc, delLoc,
                            date, price, PickType, empId, productname, pickupName, DelName, StoreName, quantity, units,
                            subName, subId, CatName, CatId, deliveryType, product_cost, Description, Distance,
                            deliverycost, mProductWeight, isWaiting, PImage, dImage, orderIdShow));


                mAdapter.notifyDataSetChanged();

            } catch (JSONException e) {


                e.printStackTrace();
            }
        }
    }


    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void paymentGateway(int id) {

        Log.e("yryryr", "" + Api.PAYMENT_GATEWAY + id + "/?is_online=True");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Api.PAYMENT_GATEWAY + id + "/?is_online=True",
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handlePaymentGateway(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreferenceManager.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "payment_gateway");
    }

    private void handlePaymentGateway(JSONObject response) {
        try {
            int amount = response.getInt(KEY_AMOUNT);
            String orderId = response.getString(Api.KEY_ORDER_ID);
            processPayment(amount, orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processPayment(int amount, String orderId) {
        Checkout checkout = new Checkout();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_NAME, getString(R.string.app_name));
            jsonObject.put(KEY_DESCRIPTION, "Trip Booking");
            jsonObject.put(KEY_CURRENCY, "INR");
            jsonObject.put(KEY_AMOUNT, amount);

            JSONObject themeObject = new JSONObject();
            themeObject.put(KEY_COLOR, "#FE0000");
            themeObject.put(KEY_EMI_MODE, true);
            jsonObject.put(KEY_THEME, themeObject);

            JSONObject prefillObject = new JSONObject();
            prefillObject.put(KEY_FIRST_NAME, mPreferenceManager.getName());
            prefillObject.put(KEY_USERNAME, mPreferenceManager.getPhone());
          /*  prefillObject.put(KEY_EMAIL, mPreferenceManager.getEmail());*/
            jsonObject.put(KEY_PREFILL, prefillObject);

            jsonObject.put(Api.KEY_ORDER_ID, orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        checkout.open(getActivity(), jsonObject);
    }
}


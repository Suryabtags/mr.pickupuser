package in.mrpickup.userapp.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.mrpickup.userapp.Adapter.PendingAdapter;
import in.mrpickup.userapp.Model.PendingBookingItem;
import in.mrpickup.userapp.Model.ProductsList;
import in.mrpickup.userapp.OrderDetailsActivity;
import in.mrpickup.userapp.R;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;
import in.mrpickup.userapp.helper.ErrorHandler;
import retrofit2.Call;
import retrofit2.Callback;

import static in.mrpickup.userapp.app.Api.KEY_FIRST_NAME;
import static in.mrpickup.userapp.app.Api.KEY_STATUS_ID;
import static in.mrpickup.userapp.app.Api.KEY_USERNAME;
import static in.mrpickup.userapp.app.Api.PENDING_ORDER;
import static in.mrpickup.userapp.helper.Utils.getCreatedDate;

public class PendingFragment extends Fragment implements
        PendingAdapter.ActiveBookingListener {

    String mUrl11;
    String empId, phone, empName, profile, productname, categoryName;
    String productItems;
    int quantity, units, subId, CatId;
    String subName, CatName;
    private Context mContext;
    private View mRootView;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private ArrayList<PendingBookingItem> mActiveBookingItemList;
    private PendingAdapter mAdapter;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;

    public PendingFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_active, container, false);
        initObjects();
        setupRecyclerView();
        setupRefresh();
        getProducts();

        refreshList();
        return mRootView;

    }

    @Override
    public void onResume() {
        refreshList();
        super.onResume();
    }

    @Override
    public void onTripLongClick(int pos) {
        promptCancelBooking(pos);
    }

    @Override
    public void onTripClick(int pos) {

        Intent intent = new Intent(mContext, OrderDetailsActivity.class);
        intent.putExtra("ORDER_ID", mActiveBookingItemList.get(pos).getId());
        intent.putExtra("orderidshow", mActiveBookingItemList.get(pos).getmOrderIdshow());
        startActivity(intent);

    }

    private void setUrl111(int pos) {
        int id = mActiveBookingItemList.get(pos).getId();
        mUrl11 = Api.URL + "orders/" + id + "/cancel_my_order/";
        deleteAddresss();
        removeItem(pos);
    }

    private void deleteAddresss() {

        ApiInterface authService = Api.getClient().create(ApiInterface.class);
        Call<Void> call = authService.deleteAddress(mUrl11, "Token " + mPreferenceManager.getToken());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull retrofit2.Response<Void> response) {
                if (response.isSuccessful()) {
                    hideProgressDialog();
                    ToastBuilder.build(mContext, "Booking cancelled");
                    mAdapter.notifyDataSetChanged();
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());

                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void removeItem(int position) {
        mActiveBookingItemList.remove(position);
        mAdapter.notifyItemRemoved(position);
        mAdapter.notifyItemRangeChanged(position, mActiveBookingItemList.size());
    }

    private void initObjects() {
        mRefreshLayout = mRootView.findViewById(R.id.refresh);
        mRecyclerView = mRootView.findViewById(R.id.list_trip_active);
        mContext = getActivity();
        mActiveBookingItemList = new ArrayList<>();
        mAdapter = new PendingAdapter(mContext, mActiveBookingItemList, this);
        mPreferenceManager = new PreferenceManager(mContext);
        mProgressDialog = new ProgressDialog(mContext);

    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);

    }

    private void setupRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        });
    }

    private void refreshList() {
        mRefreshLayout.setRefreshing(true);
        getActiveTrips();
    }

    private void getActiveTrips() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                PENDING_ORDER, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                mRefreshLayout.setRefreshing(false);

                if (isAdded()) {
                    handleOrdersList(response);
                    if (mActiveBookingItemList.isEmpty()) {
                        ToastBuilder.build(mContext, "No Orders Yet");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mRefreshLayout.setRefreshing(false);
                VolleyErrorHandler.handle(mContext, error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreferenceManager.getToken());

                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, "Orders_List");
    }

    private void handleOrdersList(JSONArray response) {
        mActiveBookingItemList.clear();
        for (int i = 0; i < response.length(); i++) {
            try {

                JSONObject jsonObject = response.getJSONObject(i);
                int orderId = jsonObject.getInt(Api.KEY_ID);
                String orderIdShow = jsonObject.getString(Api.KEY_ORDERIDSHOW);
                String pickLoc = jsonObject.getString(Api.KEY_PICKUP_LOC);
                String pickupName = jsonObject.getString(Api.KEY_PICK_NAME);
                String DelName = jsonObject.getString(Api.KEY_DEL_NAME);
                String mProductWeight = jsonObject.getString("product_weight");
                String StoreName = jsonObject.getString(Api.KEY_STORE_NAME);
                String delLoc = jsonObject.getString(Api.KEY_DELIVER_LOC);
                boolean PickType = jsonObject.getBoolean(Api.KEY_PICK_TYPE);
                JSONObject orderDetails = jsonObject.getJSONObject(Api.KEY_ORDER_DETAILS);
                String pickOtp = orderDetails.getString(Api.KEY_PICKUP_OTP);
                String delOtp = orderDetails.getString(Api.KEY_DELIVER_OTP);
                int price = orderDetails.getInt(Api.KEY_PRODUCT_COST);
                String Distance = orderDetails.getString("approx_dist");
                String deliverycost = orderDetails.getString("delivery_cost");
                JSONObject statusObj = jsonObject.getJSONObject(Api.KEY_STATUS);
                int status = statusObj.getInt(KEY_STATUS_ID);
                String date = getCreatedDate(jsonObject.getString("created_on"));

                JSONArray userObj = jsonObject.getJSONArray(Api.KEY_ORDER_ASSIGN);
                for (int j = 0; j < userObj.length(); j++) {
                    JSONObject userObj22 = userObj.getJSONObject(j);
                    JSONObject KeyNameObj = userObj22.getJSONObject(Api.KEY_EMP_ASSIGN);
                    empId = KeyNameObj.getString(Api.KEY_ID);
                    empName = KeyNameObj.getString(KEY_FIRST_NAME);
                    phone = KeyNameObj.getString(KEY_USERNAME);
                    profile = KeyNameObj.getString(Api.KEY_PROFILE_PIC);
                }

                JSONArray productObj = jsonObject.getJSONArray("categories");
                for (int k = 0; k < productObj.length(); k++) {
                    JSONObject ProductnameObj = productObj.getJSONObject(k);

                    for (int m = 0; m < ProductnameObj.length(); m++) {
                        JSONArray jsonArray = ProductnameObj.getJSONArray("products");
                        for (int y = 0; y < jsonArray.length(); y++) {
                            JSONObject ProductnameObj1 = jsonArray.getJSONObject(y);
                            quantity = ProductnameObj1.getInt("units");
                            units = ProductnameObj1.getInt("quantity");
                            subId = ProductnameObj1.getInt("id");
                            subName = ProductnameObj1.getString("name");
                        }
                    }

                    JSONObject jsonObject1 = ProductnameObj.getJSONObject("category");
                    CatName = jsonObject1.getString("name");
                    CatId = jsonObject1.getInt("id");
                }


                String deliveryType = jsonObject.getString("delivery_type");
                String Description = jsonObject.getString("descriptions");
                if (!jsonObject.isNull(Api.KEY_ORDER_ASSIGN)) {
                    mActiveBookingItemList.add(new PendingBookingItem(orderId, empName, phone, pickLoc, delLoc,
                            status, delOtp, pickOtp, date, price, PickType, profile, empId, productname,
                            pickupName, DelName, StoreName, quantity, units, subName, subId, CatName,
                            CatId, deliveryType, Description, Distance, deliverycost, mProductWeight, orderIdShow));
                } else
                    mActiveBookingItemList.add(new PendingBookingItem(orderId, status, delOtp, pickOtp, pickLoc, delLoc,
                            date, price, PickType, empId, productname, pickupName, DelName, StoreName, quantity, units,
                            subName, subId, CatName, CatId, deliveryType, Description, Distance, deliverycost, mProductWeight, orderIdShow));
                mAdapter.notifyDataSetChanged();

            } catch (JSONException e) {

                e.printStackTrace();
            }
        }
    }
    private void promptCancelBooking(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Do you want to cancel the booking?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showProgressDialog("Cancelling..");
                setUrl111(pos);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void getProducts() {
        ApiInterface apiService = Api.getClient().create(ApiInterface.class);
        Call<ArrayList<ProductsList>> call = apiService.getProducts("Token " + mPreferenceManager.getToken());
        call.enqueue(new Callback<ArrayList<ProductsList>>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ArrayList<ProductsList>> call, retrofit2.Response<ArrayList<ProductsList>> response) {
                ArrayList<ProductsList> productsLists = response.body();
                if (response.isSuccessful() && productsLists != null) {
                    for (int i = 0; i < productsLists.size(); i++) {
                        for (int j = 0; j < productsLists.get(i).getCategories().size(); j++) {

                        }

                    }

                }

            }

            @Override
            public void onFailure(Call<ArrayList<ProductsList>> call, Throwable t) {
                Log.e("failuretokenprofile", "" + t.getMessage());
            }
        });
    }


}


package in.mrpickup.userapp.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.razorpay.Checkout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.mrpickup.userapp.Adapter.CompletedBookingAdapter;
import in.mrpickup.userapp.CompletedOrderDetailsActivity;
import in.mrpickup.userapp.Model.CompletedBookingItem;
import in.mrpickup.userapp.R;
import in.mrpickup.userapp.RatingActivity;
import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;

import static in.mrpickup.userapp.app.Api.KEY_AMOUNT;
import static in.mrpickup.userapp.app.Api.KEY_COLOR;
import static in.mrpickup.userapp.app.Api.KEY_CURRENCY;
import static in.mrpickup.userapp.app.Api.KEY_DESCRIPTION;
import static in.mrpickup.userapp.app.Api.KEY_EMI_MODE;
import static in.mrpickup.userapp.app.Api.KEY_FIRST_NAME;
import static in.mrpickup.userapp.app.Api.KEY_NAME;
import static in.mrpickup.userapp.app.Api.KEY_PREFILL;
import static in.mrpickup.userapp.app.Api.KEY_STATUS_ID;
import static in.mrpickup.userapp.app.Api.KEY_THEME;
import static in.mrpickup.userapp.app.Api.KEY_USERNAME;
import static in.mrpickup.userapp.app.Constant.EXTRA_BOOKING;
import static in.mrpickup.userapp.app.Constant.RATED;
import static in.mrpickup.userapp.app.Constant.TRIP_ARRAY;
import static in.mrpickup.userapp.helper.Utils.getCreatedDate;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedTripFragment extends Fragment implements
        CompletedBookingAdapter.CompletedBookingListener {
    String empId, phone, empName, profile;
    int quantity, units, subId, CatId;
    String subName, CatName;
    String PImage, dImage;
    private Context mContext;
    private View mRootView;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private ArrayList<CompletedBookingItem> mCompletedBookingItem;
    private CompletedBookingAdapter mAdapter;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_completed, container, false);
        initObjects();
        setupRecyclerView();
        setupRefresh();

        refreshList();
        return mRootView;
    }

    @Override
    public void onRateNowClicked(CompletedBookingItem completedBookingItem) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_BOOKING, completedBookingItem);
        Activity.launchWithBundle(mContext, RatingActivity.class, bundle);
    }

    @Override
    public void onPayNowClick(int id) {
        showProgressDialog("Processing..");
        paymentGateway(id);
    }

    @Override
    public void onDetailProfileView(String id) {

    }

    @Override
    public void onResume() {
        refreshList();
        super.onResume();
    }
    @Override
    public void onTripClick(int pos) {
        Intent intent = new Intent(mContext, CompletedOrderDetailsActivity.class);
        intent.putExtra("NAME", mCompletedBookingItem.get(pos).getName());
        intent.putExtra("ORDER_ID", mCompletedBookingItem.get(pos).getId());
        intent.putExtra("PICK_MOB", mCompletedBookingItem.get(pos).getPhone());
        intent.putExtra("STATUS", mCompletedBookingItem.get(pos).getmStatus());

        intent.putExtra("Date", mCompletedBookingItem.get(pos).getDate());
        intent.putExtra("pickaddrs", mCompletedBookingItem.get(pos).getAddress());
        intent.putExtra("dropaddres", mCompletedBookingItem.get(pos).getmDestAddr());
        intent.putExtra("PaymentType", mCompletedBookingItem.get(pos).getPhone());
        intent.putExtra("PICK_TYPE", mCompletedBookingItem.get(pos).isPicktype());
        intent.putExtra("Pick_NAME", mCompletedBookingItem.get(pos).getmPname());
        intent.putExtra("Del_NAME", mCompletedBookingItem.get(pos).getmDname());
        intent.putExtra("StoreName", mCompletedBookingItem.get(pos).getmStoreName());
        intent.putExtra("deliveryCost", mCompletedBookingItem.get(pos).getPrice());
        intent.putExtra("productCost", mCompletedBookingItem.get(pos).getmProductCost());
        intent.putExtra("waitingCost", mCompletedBookingItem.get(pos).getWaitingCost());

        intent.putExtra("Description", mCompletedBookingItem.get(pos).getmDescription());
        intent.putExtra("Approximatedist", mCompletedBookingItem.get(pos).getDistance());
        intent.putExtra("deliveryCost", mCompletedBookingItem.get(pos).getDeliverycost());
        intent.putExtra("productWeight", mCompletedBookingItem.get(pos).getmProductWeight());

        intent.putParcelableArrayListExtra(TRIP_ARRAY, mCompletedBookingItem);
        startActivity(intent);
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);

    }

    private void initObjects() {
        mRefreshLayout = mRootView.findViewById(R.id.refresh);
        mRecyclerView = mRootView.findViewById(R.id.list_trip_completed);

        mContext = getActivity();
        mCompletedBookingItem = new ArrayList<>();
        mAdapter = new CompletedBookingAdapter(mContext, mCompletedBookingItem, this);
        mPreferenceManager = new PreferenceManager(mContext);
    }

    private void setupRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        });
    }

    private void refreshList() {
        mRefreshLayout.setRefreshing(true);
        getCompletedTrips();
    }

    private void getCompletedTrips() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                Api.COMPLETED_ORDER, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                mRefreshLayout.setRefreshing(false);
                if (isAdded()) {
                    handleOrdersList(response);
                    if (mCompletedBookingItem.isEmpty()) {
                        ToastBuilder.build(mContext, "No Completed Orders");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mRefreshLayout.setRefreshing(false);
                VolleyErrorHandler.handle(mContext, error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreferenceManager.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, "Orders_List");
    }


    private void handleOrdersList(JSONArray response) {

        mCompletedBookingItem.clear();
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonObject = response.getJSONObject(i);
                int orderId = jsonObject.getInt(Api.KEY_ID);
                String pickLoc = jsonObject.getString(Api.KEY_PICKUP_LOC);
                String delLoc = jsonObject.getString(Api.KEY_DELIVER_LOC);
                JSONObject orderDetails = jsonObject.getJSONObject(Api.KEY_ORDER_DETAILS);
                String pickOtp = orderDetails.getString(Api.KEY_PICKUP_OTP);
                String StoreName = jsonObject.getString(Api.KEY_STORE_NAME);
                String delOtp = orderDetails.getString(Api.KEY_DELIVER_OTP);
                JSONObject statusObj = orderDetails.getJSONObject(Api.KEY_STATUS);
                boolean PickType = jsonObject.getBoolean(Api.KEY_PICK_TYPE);
                String orderIdShow = jsonObject.getString(Api.KEY_ORDERIDSHOW);
                int status = statusObj.getInt(KEY_STATUS_ID);
                String pickupName = jsonObject.getString(Api.KEY_PICK_NAME);
                String DelName = jsonObject.getString(Api.KEY_DEL_NAME);
                String price = orderDetails.getString(Api.KEY_DELIVERY_COST);
                double product_cost = orderDetails.getDouble(Api.KEY_PRODUCT_COST);
                String payment_type = jsonObject.getString("payment_type");
                Log.e("price", "" + price);
                JSONArray userObj = jsonObject.getJSONArray(Api.KEY_ORDER_ASSIGN);
                for (int j = 0; j < userObj.length(); j++) {
                    JSONObject userObj22 = userObj.getJSONObject(j);
                    JSONObject KeyNameObj = userObj22.getJSONObject(Api.KEY_EMP_ASSIGN);
                    empId = KeyNameObj.getString(Api.KEY_ID);
                    empName = KeyNameObj.getString(KEY_FIRST_NAME);
                    phone = KeyNameObj.getString(KEY_USERNAME);
                    profile = KeyNameObj.getString(Api.KEY_PROFILE_PIC);
                }
                String Distance = orderDetails.getString("approx_dist");
                String deliverycost = orderDetails.getString("delivery_cost");
                double waiting_cost = orderDetails.getDouble("waiting_cost");
                String mProductWeight = jsonObject.getString("product_weight");
                String deliveryType = jsonObject.getString("delivery_type");
                String Description = jsonObject.getString("descriptions");
                boolean is_paid = jsonObject.getBoolean("is_paid");
                String date = getCreatedDate(jsonObject.getString("created_on"));

                JSONArray pickup_image = jsonObject.getJSONArray("pickup_image");
                for (int g = 0; g < pickup_image.length(); g++) {
                    JSONObject pickupImage = pickup_image.getJSONObject(g);
                    PImage = pickupImage.getString("image");
                }
                JSONArray delivery_image = jsonObject.getJSONArray("delivery_image");
                for (int h = 0; h < delivery_image.length(); h++) {
                    JSONObject pickupImage = delivery_image.getJSONObject(h);
                    dImage = pickupImage.getString("image");


                }

                JSONArray productObj = jsonObject.getJSONArray("categories");
                for (int k = 0; k < productObj.length(); k++) {
                    JSONObject ProductnameObj = productObj.getJSONObject(k);
                    JSONArray jsonArray = ProductnameObj.getJSONArray("products");
                    JSONObject jsonObject1 = ProductnameObj.getJSONObject("category");
                    CatName = jsonObject1.getString("name");
                    CatId = jsonObject1.getInt("id");
                    for (int y = 0; y < jsonArray.length(); y++) {
                        JSONObject ProductnameObj1 = jsonArray.getJSONObject(y);
                        quantity = ProductnameObj1.getInt("units");
                        units = ProductnameObj1.getInt("quantity");
                        subId = ProductnameObj1.getInt("id");
                        subName = ProductnameObj1.getString("name");
                    }

                }

                float rating = 0;
                if (!jsonObject.isNull(Api.KEY_RATING)) {
                    rating = (float) jsonObject.getDouble(Api.KEY_RATING);
                    if (rating > 0) {
                        status = RATED;
                    }
                }

                mCompletedBookingItem.add(new CompletedBookingItem(orderId, rating, empName, phone
                        , pickLoc, delLoc, status, price, date, profile, empId,
                        pickupName, DelName, StoreName, payment_type, product_cost, PickType, is_paid,
                        deliveryType, Description, Distance, deliverycost, mProductWeight,
                        quantity, units, subName, subId, CatName, CatId, PImage, dImage, waiting_cost));
                mAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void paymentGateway(int id) {

        Log.e("yryryr", "" + Api.PAYMENT_GATEWAY + id + "/?is_online=True");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Api.PAYMENT_GATEWAY + id + "/?is_online=True",
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handlePaymentGateway(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreferenceManager.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "payment_gateway");
    }


    private void handlePaymentGateway(JSONObject response) {
        try {
            int amount = response.getInt(KEY_AMOUNT);
            String orderId = response.getString(Api.KEY_ORDER_ID);
            processPayment(amount, orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processPayment(int amount, String orderId) {
        Checkout checkout = new Checkout();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_NAME, getString(R.string.app_name));
            jsonObject.put(KEY_DESCRIPTION, "Trip Booking");
            jsonObject.put(KEY_CURRENCY, "INR");
            jsonObject.put(KEY_AMOUNT, amount);

            JSONObject themeObject = new JSONObject();
            themeObject.put(KEY_COLOR, "#FE0000");
            themeObject.put(KEY_EMI_MODE, true);
            jsonObject.put(KEY_THEME, themeObject);

            JSONObject prefillObject = new JSONObject();
            prefillObject.put(KEY_FIRST_NAME, mPreferenceManager.getName());
            prefillObject.put(KEY_USERNAME, mPreferenceManager.getPhone());
          /*  prefillObject.put(KEY_EMAIL, mPreferenceManager.getEmail());*/
            jsonObject.put(KEY_PREFILL, prefillObject);

            jsonObject.put(Api.KEY_ORDER_ID, orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        checkout.open(getActivity(), jsonObject);
    }

}

package in.mrpickup.userapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import in.mrpickup.userapp.app.Activity;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChooseActivity extends AppCompatActivity
        implements View.OnClickListener {
    private Context mContext;
    private Toolbar mToolbar;
    private TextView mTextVIew;
    private TextView mTextView1;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
        initObjects();
        initCallbacks();
        setupToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mTextVIew) {
            Activity.launch(this, SignIn.class);
        } else if (v == mTextView1) {
            Activity.launch(this, SignUp.class);
        }

    }

    private void initObjects() {
        mToolbar = findViewById(R.id.toolbar);
        mContext = this;
        mTextVIew = findViewById(R.id.txt_signin);
        mTextView1 = findViewById(R.id.txt_signup);
    }

    private void initCallbacks() {
        mTextVIew.setOnClickListener(this);
        mTextView1.setOnClickListener(this);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(null);
    }
}

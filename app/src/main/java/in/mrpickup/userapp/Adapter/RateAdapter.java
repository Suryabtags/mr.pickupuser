package in.mrpickup.userapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import in.mrpickup.userapp.R;

public class RateAdapter extends BaseAdapter {

    Context c;
    ArrayList<HashMap<String, String>> Data;

    public RateAdapter(Context context, ArrayList<HashMap<String, String>> objects) {
        super();
        this.c = context;
        this.Data = objects;
    }

    @Override
    public int getCount() {
        return Data.size();
    }

    @Override
    public Object getItem(int position) {
        return Data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup
            parent) {

        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        View row = inflater.inflate(R.layout.item_rates, parent, false);
        TextView label = row.findViewById(R.id.categorytext);
        label.setText(Data.get(position).get("catname"));

        return row;
    }
}


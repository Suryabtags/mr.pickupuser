package in.mrpickup.userapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Locale;

import in.mrpickup.userapp.Holder.TripOrderPlaced;
import in.mrpickup.userapp.Model.ChangeStatus;
import in.mrpickup.userapp.Model.PendingBookingItem;
import in.mrpickup.userapp.R;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.helper.ErrorHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static in.mrpickup.userapp.app.Constant.ORDER_PENDING;
import static in.mrpickup.userapp.app.Constant.ORDER_PLACED;

public class PendingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    String otp;
    private Context mContext;
    private List<PendingBookingItem> mActiveBookingItemList;
    private ActiveBookingListener mListener;
    private PreferenceManager mPreference;

    public PendingAdapter(Context context, List<PendingBookingItem> activeBookingItemList,
                          ActiveBookingListener listener) {
        mContext = context;
        mActiveBookingItemList = activeBookingItemList;
        mListener = listener;

        mPreference = new PreferenceManager(mContext);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ORDER_PENDING:
                return new TripOrderPlaced(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_pending, parent, false));
            case ORDER_PLACED:
                return new TripOrderPlaced(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_pending, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ORDER_PENDING:
                bindTripOrdered((TripOrderPlaced) holder, position);
                break;
            case ORDER_PLACED:
                bindTripOrdered((TripOrderPlaced) holder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mActiveBookingItemList.get(position).getmStatus();
    }

    @Override
    public int getItemCount() {
        return mActiveBookingItemList.size();
    }

    @SuppressLint("SetTextI18n")
    private void bindTripOrdered(final TripOrderPlaced holder, int position) {
        final PendingBookingItem activeBookingItem = mActiveBookingItemList.get(position);


        if (activeBookingItem.getmType()) {

            if (activeBookingItem.getmPicOtp() != null && !activeBookingItem.getmPicOtp().isEmpty()) {
                otp = String.format(Locale.getDefault(), mContext.getString(R.string.format_otp),
                        activeBookingItem.getmPicOtp());
            }

        } else {
            if (activeBookingItem.getmDelOtp() != null && !activeBookingItem.getmDelOtp().isEmpty()) {
                otp = String.format(Locale.getDefault(), mContext.getString(R.string.format_otp),
                        activeBookingItem.getmDelOtp());
            }
        }

        String address = activeBookingItem.getAddress();
        String destination = activeBookingItem.getmDestAddr();
        String date = activeBookingItem.getDate();
        int price = activeBookingItem.getPrice();
        int productcost = price;
        holder.getTextViewTripId().setText("Trip Id: " + activeBookingItem.getmOrderIdshow());
        holder.getTextViewAddress().setText(address);
        holder.getmTextViewDrop().setText(destination);
        holder.getTextViewDate().setText(date);

        if (activeBookingItem.getmType()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }
        if (otp != null && activeBookingItem.getmType()) {
            holder.getmTextViewOtp().setText(otp);
            holder.getTextViewPrice().setVisibility(View.GONE);
            holder.getmTextViewOtp().setVisibility(View.VISIBLE);
        } else if (!activeBookingItem.getmType() && otp != null && productcost > 0) {
            holder.getmTextViewOtp().setText(otp);
            holder.getTextViewPrice().setText("\u20B9 " + price);
            holder.getTextViewPrice().setVisibility(View.VISIBLE);
            holder.getmTextViewOtp().setVisibility(View.VISIBLE);
        } else if (!activeBookingItem.getmType() && otp != null) {
            holder.getmTextViewOtp().setText(otp);
            holder.getmTextViewOtp().setVisibility(View.VISIBLE);
        } else {
            holder.getTextViewPrice().setVisibility(View.GONE);
        }

        if (activeBookingItem.getmStatus() == 9) {
            holder.getmTextViewTripStatus().setText("Pending...");
        } else {
            holder.getmTextViewTripStatus().setText("Order Placed");
            holder.getmTextViewTripStatus().setTextColor(ContextCompat.getColor(mContext, R.color.green));
        }

        if (!activeBookingItem.getmType() && activeBookingItem.getmStatus() == 9 && productcost > 0) {
            holder.getmAcceptLayout().setVisibility(View.VISIBLE);
            holder.getmTextViewTripStatus().setVisibility(View.GONE);

            holder.getmAccept().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Are you sure you want to Accept the Order?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mAcceptOption(activeBookingItem.getId(), new ChangeStatus("1"));

                            holder.getmAcceptLayout().setVisibility(View.GONE);
                            holder.getmTextViewTripStatus().setVisibility(View.VISIBLE);
                            holder.getmTextViewTripStatus().setText("Order placed");
                            holder.getmTextViewTripStatus().setTextColor(ContextCompat.getColor(mContext, R.color.green));
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();


                }
            });

            holder.getmReject().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Are you sure you want to Reject the Order?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mAcceptOption(activeBookingItem.getId(), new ChangeStatus("6"));

                            holder.getmAcceptLayout().setVisibility(View.GONE);
                            holder.getmTextViewTripStatus().setVisibility(View.VISIBLE);
                            holder.getmTextViewTripStatus().setText("Rejected");
                            holder.getmTextViewTripStatus().setTextColor(ContextCompat.getColor(mContext, R.color.red));
                            holder.getmImageProfileStatus().setImageResource(R.drawable.ic_rejected);
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();


                }
            });


        } else {
            holder.getmAcceptLayout().setVisibility(View.GONE);
            holder.getmTextViewTripStatus().setVisibility(View.VISIBLE);
        }


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onTripLongClick(holder.getLayoutPosition());
                return false;
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onTripLongClick(holder.getAdapterPosition());
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getLayoutPosition());
            }

        });
    }

    private void mAcceptOption(int id, ChangeStatus changeStatus) {
        String mUrl = Api.URL + "orders/" + id + "/change_status/";
        ApiInterface authService = Api.getClient().create(ApiInterface.class);
        Call<Void> call = authService.getAcceptOption(mUrl, "Token " + mPreference.getToken(), changeStatus);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {

                if (response.isSuccessful()) {
                    Log.e("response", "" + response.toString());

                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }


            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());

            }
        });
    }

    public interface ActiveBookingListener {

        void onTripLongClick(int id);

        void onTripClick(int id);
    }
}


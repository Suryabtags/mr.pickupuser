package in.mrpickup.userapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.mrpickup.userapp.Holder.AddEditProductHolder;
import in.mrpickup.userapp.Model.CompletedBookingItem;
import in.mrpickup.userapp.R;
import in.mrpickup.userapp.callback.ProductCallback;


public class ProductCompleteListAdapter extends RecyclerView.Adapter<AddEditProductHolder> {

    String catname, subcatname, mQuantity, id, productId;
    private Context mContext;
    private List<CompletedBookingItem> mProductList;
    private ProductCallback mCallback;

    public ProductCompleteListAdapter(Context context, List<CompletedBookingItem> chatList, ProductCallback callback) {
        mContext = context;
        mCallback = callback;
        mProductList = chatList;
    }

    @Override
    public AddEditProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddEditProductHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editproducts, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(final AddEditProductHolder holder, @SuppressLint("RecyclerView") final int position) {
        CompletedBookingItem productdata = mProductList.get(position);

        holder.mCategory.setText(productdata.getCatName());
        holder.mSubCategory.setText(productdata.getSubCatName());
        holder.mQuantity.setText(String.valueOf(productdata.getQuantity()));
        Log.e("hdfgdjk", "" + productdata.getUnits());

        catname = productdata.getCatName();
        subcatname = productdata.getSubCatName();
//        mQuantity = String.valueOf(productdata.getQuantity());
//        id = String.valueOf(productdata.getId());
//        productId = String.valueOf(productdata.getSubcatId());
//        holder.mImageEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i= new Intent(mContext, AddEditProductActivity.class);
//                i.putExtra("mCategory",catname);
//                i.putExtra("mSubCategory",subcatname);
//                i.putExtra("mQuantity",mQuantity);
//                i.putExtra("mId",id);
//                i.putExtra("productId",productId);
//                mContext.startActivity(i);
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}

package in.mrpickup.userapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.mrpickup.userapp.Holder.AddProductHolder;
import in.mrpickup.userapp.Model.Productdata;
import in.mrpickup.userapp.R;
import in.mrpickup.userapp.callback.ProductCallback;


public class ProductListAdapter extends RecyclerView.Adapter<AddProductHolder> {

    private Context mContext;
    private List<Productdata> mProductList;
    private ProductCallback mCallback;

    public ProductListAdapter(Context context, List<Productdata> chatList, ProductCallback callback) {
        mContext = context;
        mCallback = callback;
        mProductList = chatList;
    }

    @Override
    public AddProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddProductHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_addproducts, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(final AddProductHolder holder, @SuppressLint("RecyclerView") final int position) {
        Productdata productdata = mProductList.get(position);

        holder.mCategory.setText(productdata.getdata());
        holder.mSubCategory.setText(productdata.getSubdata());
        holder.mQuantity.setText(productdata.getmQuantity());
        holder.mUnits.setText(productdata.getmUnitId());

    }


    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}

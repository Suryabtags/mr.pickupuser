package in.mrpickup.userapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import in.mrpickup.userapp.Holder.TripAcceptedHolder;
import in.mrpickup.userapp.Holder.TripDelivered;
import in.mrpickup.userapp.Holder.TripStarted;
import in.mrpickup.userapp.Holder.TripTransit;
import in.mrpickup.userapp.Model.ActiveBookingItem;
import in.mrpickup.userapp.R;

import static in.mrpickup.userapp.app.Constant.EMPLOYEE_ASSIGNED;
import static in.mrpickup.userapp.app.Constant.PAYMENT;
import static in.mrpickup.userapp.app.Constant.PICKUP_BUY;
import static in.mrpickup.userapp.app.Constant.STARTED;

public class ActiveBookingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    private List<ActiveBookingItem> mActiveBookingItemList;
    private ActiveBookingListener mListener;

    public ActiveBookingAdapter(Context context, List<ActiveBookingItem> activeBookingItemList,
                                ActiveBookingListener listener) {
        mContext = context;
        mActiveBookingItemList = activeBookingItemList;
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case EMPLOYEE_ASSIGNED:
                return new TripAcceptedHolder(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_accepted, parent, false));
            case STARTED:
                return new TripStarted(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_trip_started, parent, false));
            case PAYMENT:
                return new TripDelivered(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_delivered, parent, false));
            case PICKUP_BUY:
                return new TripTransit(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_transit, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case EMPLOYEE_ASSIGNED:
                bindTripAcceptedHolder((TripAcceptedHolder) holder, position);
                break;
            case STARTED:
                bindTripPickupBuy((TripStarted) holder, position);
                break;
            case PAYMENT:
                bindTripPayHolder((TripDelivered) holder, position);
                break;
            case PICKUP_BUY:
                bindTripTransit((TripTransit) holder, position);
        }
    }

    @SuppressLint("SetTextI18n")
    private void bindTripPayHolder(final TripDelivered holder, int position) {
        final ActiveBookingItem completedBookingItem = mActiveBookingItemList.get(position);
        String tripId = String.format(Locale.getDefault(),
                mContext.getString(R.string.format_trip_id), completedBookingItem.getId());
        String address = completedBookingItem.getAddress();
        String date = completedBookingItem.getDate();
        String name = completedBookingItem.getName();
        String destAddr = completedBookingItem.getmDestAddr();
        String profilePic = completedBookingItem.getProfilePic();
        double cost = completedBookingItem.getPrice();
        double productcost = completedBookingItem.getmProductCost();
        if (completedBookingItem.getmType()) {
            holder.getTextViewPrice().setText(String.format("₹ %s", cost));
        } else {
            double totalcost = cost + productcost;
            String totalcost1 = "\u20B9 " + String.valueOf(totalcost);
            holder.getTextViewPrice().setText(totalcost1);

        }
        holder.getTextViewTripId().setText("Trip Id: " + completedBookingItem.getmOrderIDShow());
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDate().setText(date);
        holder.getTextViewName().setText("Driver");
        holder.getTextViewPrice().setText(String.format("₹ %s", String.valueOf(cost)));

        holder.getTextViewDrop().setText(destAddr);
        Glide.with(mContext).load(profilePic).into(holder.getImageViewProfile());

        if (completedBookingItem.getmType()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }
        holder.getTextViewPayNow().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPayNowClick(completedBookingItem.getId());
            }
        });

        holder.getImageViewProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDetailProfileView(completedBookingItem.getEmpId());
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getLayoutPosition());
            }

        });

    }


    @Override
    public int getItemViewType(int position) {
        return mActiveBookingItemList.get(position).getmStatus();
    }

    @Override
    public int getItemCount() {
        return mActiveBookingItemList.size();
    }


    private void bindTripAcceptedHolder(final TripAcceptedHolder holder, int position) {
        final ActiveBookingItem activeBookingItem = mActiveBookingItemList.get(position);

        String address = activeBookingItem.getAddress();
        String date = activeBookingItem.getDate();
        String name = activeBookingItem.getName();
        String otp;
        if (activeBookingItem.getmPicOtp() != null && activeBookingItem.getmType()) {
            otp = String.format(Locale.getDefault(), mContext.getString(R.string.format_otp), activeBookingItem.getmPicOtp());
        } else {
            otp = String.format(Locale.getDefault(), mContext.getString(R.string.format_otp), activeBookingItem.getmDelOtp());

        }

        if (activeBookingItem.getmType()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }

        String destAddr = activeBookingItem.getmDestAddr();
        int status = activeBookingItem.getmStatus();
        String phone = activeBookingItem.getPhone();
        String profilePic = activeBookingItem.getProfilePic();


        holder.getTextViewTripId().setText("Trip Id: " + activeBookingItem.getmOrderIDShow());
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDate().setText(date);
        holder.getTextViewName().setText("Driver");
        if (otp != null) {
            holder.getTextViewOtp().setText(otp);
        }

        holder.getTextViewDrop().setText(destAddr);

        String empid = activeBookingItem.getEmpId();
        Glide.with(mContext).load(profilePic).into(holder.getImageViewProfile());

        holder.getTextViewPhone().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCallPorterClick(activeBookingItem.getPhone());
            }
        });


        holder.getTextViewPhone().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCallPorterClick(activeBookingItem.getPhone());
            }
        });
        holder.getImageViewProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDetailProfileView(activeBookingItem.getEmpId());
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getLayoutPosition());
            }

        });
    }

    private void bindTripPickupBuy(final TripStarted holder, int position) {
        final ActiveBookingItem activeBookingItem = mActiveBookingItemList.get(position);
        String otp = String.format(Locale.getDefault(), mContext.getString(R.string.format_otp),
                activeBookingItem.getmDelOtp());

        String address = activeBookingItem.getAddress();
        String destination = activeBookingItem.getmDestAddr();
        String date = activeBookingItem.getDate();
        String name = activeBookingItem.getName();

        holder.getTextViewTripId().setText("Trip Id: " + activeBookingItem.getmOrderIDShow());
        holder.getTextViewPrice().setText(otp);
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDrop().setText(destination);
        holder.getTextViewDate().setText(date);
        holder.getTextViewName().setText("Driver");
        String profilePic = activeBookingItem.getProfilePic();
        Glide.with(mContext).load(profilePic).into(holder.getImageViewProfile());
        if (activeBookingItem.getmType()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }
        holder.getImageViewProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDetailProfileView(activeBookingItem.getEmpId());
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getLayoutPosition());
            }

        });
        if (activeBookingItem.isWaiting()) {
            holder.getmTripWaitingTimeStarted().setVisibility(View.VISIBLE);
        } else {
            holder.getmTripWaitingTimeStarted().setVisibility(View.GONE);

        }
    }


    private void bindTripTransit(final TripTransit holder, int position) {
        final ActiveBookingItem activeBookingItem = mActiveBookingItemList.get(position);

        String otp = String.format(Locale.getDefault(), mContext.getString(R.string.format_otp),
                activeBookingItem.getmPicOtp());
        String address = activeBookingItem.getAddress();
        String destination = activeBookingItem.getmDestAddr();
        String date = activeBookingItem.getDate();
        String name = activeBookingItem.getName();

        holder.getTextViewTripId().setText("Trip Id: " + activeBookingItem.getmOrderIDShow());
        holder.getTextViewPrice().setText(otp);
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDrop().setText(destination);
        holder.getTextViewDate().setText(date);
        holder.getTextViewName().setText("Driver");
        if (activeBookingItem.getmType()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }
        String profilePic = activeBookingItem.getProfilePic();
        Glide.with(mContext).load(profilePic).into(holder.getImageViewProfile());

        holder.getImageViewProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDetailProfileView(activeBookingItem.getEmpId());
            }
        });

        holder.getTextViewAddress().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getAdapterPosition());
            }

        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getLayoutPosition());
            }

        });

        if (activeBookingItem.isWaiting()) {
            holder.getmTripWaitingProcess().setVisibility(View.VISIBLE);
        } else {
            holder.getmTripWaitingProcess().setVisibility(View.GONE);

        }
    }


    public interface ActiveBookingListener {
        void onCallPorterClick(String phone);

        void onPayNowClick(int id);

        void onTripClick(int id);

        void onDetailProfileView(String id);
    }
}


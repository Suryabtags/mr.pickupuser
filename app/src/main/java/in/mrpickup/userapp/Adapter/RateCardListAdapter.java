package in.mrpickup.userapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.mrpickup.userapp.Holder.RateCardHolder;
import in.mrpickup.userapp.Model.Rates;
import in.mrpickup.userapp.R;
import in.mrpickup.userapp.callback.ProductCallback;


public class RateCardListAdapter extends RecyclerView.Adapter<RateCardHolder> {

    private Context mContext;
    private List<Rates> mRatesList;
    private ProductCallback mCallback;

    public RateCardListAdapter(Context context, List<Rates> chatList, ProductCallback callback) {
        mContext = context;
        mCallback = callback;
        mRatesList = chatList;
    }

    @NonNull
    @Override
    public RateCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RateCardHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_rate_card, parent,
                        false), mCallback);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull final RateCardHolder holder, @SuppressLint("RecyclerView") final int position) {
        Rates rates = mRatesList.get(position);


        holder.txt_price.setText("Upto " + String.valueOf(rates.getmToWeight() + " km") + " With "
                + String.valueOf(rates.getmToWeight() + " kg ") + String.format("₹ %s", String.valueOf(rates.getmDistanceCost())));
        holder.txt_extrakmprice.setText(String.format("₹ %.0f", rates.getmExtraKm()));
        holder.txt_extrakgprice.setText(String.format("₹ %.0f", rates.getmExtraKg()));

    }

    @Override
    public int getItemCount() {
        return mRatesList.size();
    }
}

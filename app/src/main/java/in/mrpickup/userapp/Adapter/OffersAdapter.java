package in.mrpickup.userapp.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.mrpickup.userapp.Model.OffersList;
import in.mrpickup.userapp.Model.OffersListHolder;
import in.mrpickup.userapp.R;


/**
 * Created by Bobby on 14/07/17
 */

public class OffersAdapter extends RecyclerView.Adapter<OffersListHolder> {

    private List<OffersList> mServiceCategoryList;

    public OffersAdapter(List<OffersList> serviceCategoryList) {
        mServiceCategoryList = serviceCategoryList;
    }

    @NonNull
    @Override
    public OffersListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OffersListHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offers,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OffersListHolder holder, int position) {
        OffersList serviceCategory = mServiceCategoryList.get(position);
        holder.mTextviewOffersName.setText(serviceCategory.getmOfferName());


        String reason = "Purchase Upto Rs " + serviceCategory.getmFromPrice() + " to " + serviceCategory.getmToPrice();
        String price = " Get  " + String.valueOf(serviceCategory.getOffer_km()) + " km Free Delivery ";
        holder.mTextviewOfferReason.setText(reason);
        holder.mTextViewOfferPrice.setText(price);
        if (serviceCategory.getmOffersAvailable().equals("1")) {
            holder.mTextViewOfferType.setText("for " + "Pick & Delivery");
        } else if (serviceCategory.getmOffersAvailable().equals("2")) {
            holder.mTextViewOfferType.setText("for " + "Buy & Delivery");
        } else {
            holder.mTextViewOfferType.setText("for " + "Both Delivery");
        }
    }

    @Override
    public int getItemCount() {
        return mServiceCategoryList.size();
    }
}

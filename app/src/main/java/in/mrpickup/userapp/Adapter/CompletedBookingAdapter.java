package in.mrpickup.userapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import in.mrpickup.userapp.Holder.TripCancelled;
import in.mrpickup.userapp.Holder.TripCompletedHolder;
import in.mrpickup.userapp.Holder.TripDelivered;
import in.mrpickup.userapp.Holder.TripRated;
import in.mrpickup.userapp.Model.CompletedBookingItem;
import in.mrpickup.userapp.R;

import static in.mrpickup.userapp.app.Constant.CANCEL;
import static in.mrpickup.userapp.app.Constant.DELIVERED;
import static in.mrpickup.userapp.app.Constant.PAID;
import static in.mrpickup.userapp.app.Constant.PAYMENT;
import static in.mrpickup.userapp.app.Constant.RATED;

/**
 * Created by Admin on 7/6/2017.
 */

public class CompletedBookingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<CompletedBookingItem> mCompletedBookingItemList;
    private CompletedBookingListener mListener;

    public CompletedBookingAdapter(Context context,
                                   List<CompletedBookingItem> completedBookingItemList,
                                   CompletedBookingListener listener) {
        mContext = context;
        mCompletedBookingItemList = completedBookingItemList;
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case DELIVERED:
                return new TripCompletedHolder(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_trip_completed, parent, false));
            case PAID:
                return new TripCompletedHolder(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_trip_completed, parent, false));
            case CANCEL:
                return new TripCancelled(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_trip_cancelled, parent, false));
            case PAYMENT:
                return new TripDelivered(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_delivered, parent, false));
            case RATED:
                return new TripRated(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip_rated, parent, false));
            default:
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mCompletedBookingItemList.get(position).getmStatus();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case DELIVERED:
                bindTripCompletedHolder((TripCompletedHolder) holder, position);
                break;
            case PAID:
                bindTripCompletedHolder((TripCompletedHolder) holder, position);
                break;
            case CANCEL:
                bindTripCancelled((TripCancelled) holder, position);
                break;
            case PAYMENT:
                bindTripPayHolder((TripDelivered) holder, position);
                break;
            case RATED:
                bindTripRated((TripRated) holder, position);
                break;

        }
    }


    @Override
    public int getItemCount() {
        return mCompletedBookingItemList.size();
    }

    private void bindTripPayHolder(final TripDelivered holder, int position) {
        final CompletedBookingItem completedBookingItem = mCompletedBookingItemList.get(position);
        String tripId = String.format(Locale.getDefault(),
                mContext.getString(R.string.format_trip_id), completedBookingItem.getId());
        String address = completedBookingItem.getAddress();
        String date = completedBookingItem.getDate();
        String name = completedBookingItem.getName();
        String cost = completedBookingItem.getPrice();
        double productcost = completedBookingItem.getmProductCost();


        if (completedBookingItem.isPicktype()) {
            holder.getTextViewPrice().setText("\u20b9 " + cost);
        } else {
            double totalcost = Double.parseDouble(cost) + productcost;
            String totalcost1 = "\u20B9 " + String.valueOf(totalcost);
            holder.getTextViewPrice().setText(totalcost1);

        }
        String destAddr = completedBookingItem.getmDestAddr();
        String profilePic = completedBookingItem.getProfilePic();

        holder.getTextViewTripId().setText(tripId);
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDate().setText(date);
        holder.getTextViewName().setText(name);

        holder.getTextViewDrop().setText(destAddr);
        Glide.with(mContext).load(profilePic).into(holder.getImageViewProfile());

        if (completedBookingItem.isPicktype()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }
        holder.getTextViewPayNow().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPayNowClick(completedBookingItem.getId());
            }
        });

        holder.getImageViewProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDetailProfileView(completedBookingItem.getEmpId());
            }
        });
    }

    private void bindTripCompletedHolder(final TripCompletedHolder holder, int position) {
        final CompletedBookingItem completedBookingItem = mCompletedBookingItemList.get(position);
        String tripId = String.format(Locale.getDefault(),
                mContext.getString(R.string.format_trip_id), completedBookingItem.getId());
        String address = completedBookingItem.getAddress();
        String date = completedBookingItem.getDate();
        String name = completedBookingItem.getName();
        String cost = completedBookingItem.getPrice();
        String destAddr = completedBookingItem.getmDestAddr();
        String profilePic = completedBookingItem.getProfilePic();
        holder.getTextViewTripId().setText(tripId);
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDate().setText(date);
        //   holder.getTextViewName().setText(name);
        holder.getTextViewComments().setText(destAddr);
        //  holder.getImageViewProfile().setImageResource(R.drawable.ic_completed);
        holder.getTextViewRate().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onRateNowClicked(completedBookingItem);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getAdapterPosition());
            }

        });
        if (completedBookingItem.isPicktype()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }

        double productcost = completedBookingItem.getmProductCost();
        double waitingcost = completedBookingItem.getWaitingCost();
        if (completedBookingItem.isPicktype()) {
            if (waitingcost > 0) {
                double total = Double.parseDouble(cost) + waitingcost;
                holder.getTextViewPrice().setText(String.format("₹ %s", total));
            } else {
                holder.getTextViewPrice().setText(String.format("₹ %s", cost));
            }
        } else {

            if (waitingcost > 0) {
                double totalcost = Double.parseDouble(cost) + productcost + waitingcost;
                String totalcost1 = "\u20B9 " + String.valueOf(totalcost);
                holder.getTextViewPrice().setText(totalcost1);
            } else {
                double totalcost = Double.parseDouble(cost) + productcost;
                String totalcost1 = "\u20B9 " + String.valueOf(totalcost);
                holder.getTextViewPrice().setText(totalcost1);
            }


        }
    }

    private void bindTripPaidHolder(final TripCompletedHolder holder, int position) {
        final CompletedBookingItem completedBookingItem = mCompletedBookingItemList.get(position);
        String tripId = String.format(Locale.getDefault(),
                mContext.getString(R.string.format_trip_id), completedBookingItem.getId());
        String address = completedBookingItem.getAddress();
        String date = completedBookingItem.getDate();
        String name = completedBookingItem.getName();
        String cost = completedBookingItem.getPrice();
        String destAddr = completedBookingItem.getmDestAddr();
        String profilePic = completedBookingItem.getProfilePic();
        holder.getTextViewTripId().setText(tripId);
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDate().setText(date);
        //   holder.getTextViewName().setText(name);
        holder.getTextViewComments().setText(destAddr);
        //  holder.getImageViewProfile().setImageResource(R.drawable.ic_completed);
        holder.getTextViewRate().setText("PAID");


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getAdapterPosition());
            }

        });
        if (completedBookingItem.isPicktype()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }

        double productcost = completedBookingItem.getmProductCost();
        double waitingcost = completedBookingItem.getWaitingCost();

        if (completedBookingItem.isPicktype()) {
            if (waitingcost > 0) {
                double total = Double.parseDouble(cost) + waitingcost;
                holder.getTextViewPrice().setText(String.format("₹ %s", total));
            } else {
                holder.getTextViewPrice().setText(String.format("₹ %s", cost));
            }
        } else {

            if (waitingcost > 0) {
                double totalcost = Double.parseDouble(cost) + productcost + waitingcost;
                String totalcost1 = "\u20B9 " + String.valueOf(totalcost);
                holder.getTextViewPrice().setText(totalcost1);
            } else {
                double totalcost = Double.parseDouble(cost) + productcost;
                String totalcost1 = "\u20B9 " + String.valueOf(totalcost);
                holder.getTextViewPrice().setText(totalcost1);
            }


        }
    }

    private void bindTripCancelled(final TripCancelled holder, int position) {
        final CompletedBookingItem completedBookingItem = mCompletedBookingItemList.get(position);
        String tripId = String.format(Locale.getDefault(),
                mContext.getString(R.string.format_trip_id), completedBookingItem.getId());
        String address = completedBookingItem.getAddress();
        String destAddr = completedBookingItem.getmDestAddr();
        String date = completedBookingItem.getDate();
        holder.getTextViewTripId().setText(tripId);
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDrop().setText(destAddr);
        holder.getTextViewDate().setText(date);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getAdapterPosition());
            }

        });
        if (completedBookingItem.isPicktype()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }
    }

    private void bindTripRated(final TripRated holder, int position) {
        final CompletedBookingItem completedBookingItem = mCompletedBookingItemList.get(position);
        String tripId = String.format(Locale.getDefault(),
                mContext.getString(R.string.format_trip_id), completedBookingItem.getId());

        Log.e("cost", "" + completedBookingItem.getPrice());
        String address = completedBookingItem.getAddress();
        String destAddr = completedBookingItem.getmDestAddr();
        String date = completedBookingItem.getDate();
        String name = completedBookingItem.getName();
        String profilePic = completedBookingItem.getProfilePic();
        double rating = completedBookingItem.getmrate();

        holder.getTextViewTripId().setText(tripId);
        String cost = completedBookingItem.getPrice();

        double productcost = completedBookingItem.getmProductCost();
        double waitingcost = completedBookingItem.getWaitingCost();

        if (completedBookingItem.isPicktype()) {
            if (waitingcost > 0) {
                double total = Double.parseDouble(cost) + waitingcost;
                holder.getTextViewPrice().setText(String.format("₹ %s", total));
            } else {
                holder.getTextViewPrice().setText(String.format("₹ %s", cost));
            }
        } else {

            if (waitingcost > 0) {
                double totalcost = Double.parseDouble(cost) + productcost + waitingcost;
                String totalcost1 = "\u20B9 " + String.valueOf(totalcost);
                holder.getTextViewPrice().setText(totalcost1);
            } else {
                double totalcost = Double.parseDouble(cost) + productcost;
                String totalcost1 = "\u20B9 " + String.valueOf(totalcost);
                holder.getTextViewPrice().setText(totalcost1);
            }


        }
        holder.getTextViewAddress().setText(address);
        holder.getTextViewDrop().setText(destAddr);
        holder.getTextViewDate().setText(date);
        holder.getRatingBar().setRating((float) rating);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTripClick(holder.getAdapterPosition());
            }

        });
        if (completedBookingItem.isPicktype()) {
            holder.getmTriptype().setText("Pick Up & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accentLight));
        } else {
            holder.getmTriptype().setText("Buy & Delivery");
            holder.getmTriptype().setTextColor(ContextCompat.getColor(mContext, R.color.accent_green));
        }
    }

    public interface CompletedBookingListener {
        void onRateNowClicked(CompletedBookingItem completedBookingItem);

        void onPayNowClick(int id);


        void onDetailProfileView(String id);

        void onTripClick(int id);

    }
}




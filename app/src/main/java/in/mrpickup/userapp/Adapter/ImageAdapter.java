package in.mrpickup.userapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.util.List;

import in.mrpickup.userapp.Holder.ImageHolder;
import in.mrpickup.userapp.R;


public class ImageAdapter extends RecyclerView.Adapter<ImageHolder> {

    private Context mContext;
    private List<File> mChatList;

    public ImageAdapter(Context context, List<File> chatList) {
        mContext = context;
        mChatList = chatList;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_media, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(final ImageHolder holder, @SuppressLint("RecyclerView") final int position) {

        Glide.with(mContext).
                load(mChatList.get(position))
                .apply(new RequestOptions().override(300, 300))
                .into(holder.mImageViewProfile);

        holder.img_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   mChatList.remove(position);
                removeAt(position);
                notifyDataSetChanged();

            }
        });

    }

    private void removeAt(int position) {
        mChatList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mChatList.size());
    }

    @Override
    public int getItemCount() {
        return mChatList.size();
    }
}

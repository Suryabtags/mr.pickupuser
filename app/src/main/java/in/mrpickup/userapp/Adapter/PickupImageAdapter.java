package in.mrpickup.userapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import in.mrpickup.userapp.Holder.ImageHolder;
import in.mrpickup.userapp.Model.ActiveBookingItem;
import in.mrpickup.userapp.R;


public class PickupImageAdapter extends RecyclerView.Adapter<ImageHolder> {

    private Context mContext;
    private List<ActiveBookingItem> mChatList;

    public PickupImageAdapter(Context context, List<ActiveBookingItem> chatList) {
        mContext = context;
        mChatList = chatList;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_media_new, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageHolder holder, @SuppressLint("RecyclerView") final int position) {

        Glide.with(mContext).
                load(mChatList.get(position).getPickImage())
                .apply(new RequestOptions().override(200, 200))
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_loader).error(R.drawable.ic_image_loader))
                .into(holder.mImageViewProfile);
        holder.img_remove.setVisibility(View.GONE);

    }

    private void removeAt(int position) {
        mChatList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mChatList.size());
    }

    @Override
    public int getItemCount() {
        return mChatList.size();
    }
}

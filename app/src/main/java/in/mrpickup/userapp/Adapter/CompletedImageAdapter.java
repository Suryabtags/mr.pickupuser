package in.mrpickup.userapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import in.mrpickup.userapp.Holder.ImageHolder;
import in.mrpickup.userapp.Model.CompletedBookingItem;
import in.mrpickup.userapp.R;


public class CompletedImageAdapter extends RecyclerView.Adapter<ImageHolder> {

    AlertDialog alertDialog2;
    private Context mContext;
    private List<CompletedBookingItem> mChatList;
    public CompletedImageAdapter(Context context, List<CompletedBookingItem> chatList) {
        mContext = context;
        mChatList = chatList;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_media_new, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageHolder holder, @SuppressLint("RecyclerView") final int position) {

        Glide.with(mContext).
                load(mChatList.get(position).getPickImage())
                .apply(new RequestOptions().override(200, 200))
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_loader).error(R.drawable.ic_image_loader))
                .into(holder.mImageViewProfile);
        holder.img_remove.setVisibility(View.GONE);

        holder.mImageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage(mChatList.get(position).getPickImage());
            }
        });

    }


    @Override
    public int getItemCount() {
        return mChatList.size();
    }


    private void openImage(String imageitem) {
        View otpView = LayoutInflater.from(mContext).inflate(R.layout.item_imageview, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(otpView);
        alertDialog2 = builder.create();

        final ImageView img_remove = otpView.findViewById(R.id.img_remove);
        final ImageView img_media = otpView.findViewById(R.id.img_media);
        Glide.with(mContext).
                load(imageitem)
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_loader).error(R.drawable.ic_image_loader))
                .into(img_media);

        img_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog2.dismiss();
            }
        });


        alertDialog2.show();
    }

}

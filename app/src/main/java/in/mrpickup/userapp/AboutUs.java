package in.mrpickup.userapp;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.mrpickup.userapp.Adapter.MyPagerAdapter;
import in.mrpickup.userapp.fragment.AboutUsFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutUs extends AppCompatActivity
        implements TabLayout.OnTabSelectedListener {


    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private List<Fragment> mFragmentList;
    private MyPagerAdapter mPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        initObjects();
        setupToolbar();
        initCallbacks();
        populateTabs();
        changeTabsFont();

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initObjects() {
        mToolbar = findViewById(R.id.toolbar);
        mTabLayout = findViewById(R.id.tab_layout);
        mViewPager = findViewById(R.id.pager_about_us);
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragmentList);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("About Us");
        mToolbar.setNavigationIcon(R.drawable.ic_left_arrow);
    }

    private void initCallbacks() {
        mTabLayout.addOnTabSelectedListener(this);

    }

    private void populateTabs() {
        String data = "Looking for trustworthy reliable domestic and international courier service in Chennai. We’re on a \n" +
                "mission to make each delivery a delightful experience for individuals and businesses.Mr.Pickup is revolutionizing the world of delivery in Chennai with cutting-edge technologies. Sending \n" +
                "and receiving packages has never been this simple, seamless, or easy.Who are we: We’re an innovative technology driven delivery service company based in India." +
                "transforming the traditional way of picking and delivering packages. \n" + "What do we do: We solve the unpredictability of package delivery in Chennai. " +
                "We use the GPS in your phone for location based pickup and delivery with our patented app. " +
                "All you have to do is just Snap, Swipe and Ship\n" + "Where do we ship to: Chennai\n" + "How does it work: Have a look at our page www.mrpickup." +
                "in to learn how we can help your business and our pricing details\n" + "What makes us special: Our pickup and delivery courier app makes your life easier; with a few taps you can get" +
                " us to run your errands for you so you’ll get more time in your day to do the things you enjoy doing.";
        String data1 = "Our Privacy Policy is developed in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our policies in detail:\n" +
                "Before or at the time of collecting personal information, we will identify the purposes for which information is being collected. We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law. We will only retain personal information as long as necessary for the fulfillment of those purposes. We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned. Personal data should be relevant to the purposes for which it is to be used, and to the extent necessary for those purposes, should be accurate, complete, and up-to-date.We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.";
        String data2 = "Copyrights\n" +
                "All content and functionality on the Site, including text, graphics, logos, icons, and images and the selection and arrangement thereof, is the exclusive property of Adityavani.\n" +
                "Trademarks\n" +
                "The trademarks, service marks, designs, and logos displayed on the Site are the registered and unregistered Trademarks of Adityavani. You agree that you will not refer to or attribute any information to Adityavani in any public medium (e.g., press release, websites) for advertising or promotion purposes, or for the purpose of informing or influencing any third party.\n";
        String data3 = "Disclaimers\n" +
                "All content and functionality on the site is provided “as is,” without warranty of any kind, either express or implied, including, without limitation, implied warranties of merchantability and fitness for a particular purpose.";
        mTabLayout.addTab(mTabLayout.newTab().setText("About"));
        mFragmentList.add(AboutUsFragment.newInstance(data));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Privacy Policy"));
//        mFragmentList.add(AboutUsFragment.newInstance(data1));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Terms & Conditions"));
//        mFragmentList.add(AboutUsFragment.newInstance(data2));
        mTabLayout.addTab(mTabLayout.newTab().setText("Disclaimer"));
        mFragmentList.add(AboutUsFragment.newInstance(data3));
        mPagerAdapter.notifyDataSetChanged();

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(
                            Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf"),
                            Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(
                            getResources().getDimension(R.dimen.small_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

}

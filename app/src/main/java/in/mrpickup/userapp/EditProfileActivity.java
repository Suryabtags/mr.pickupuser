package in.mrpickup.userapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mrpickup.userapp.Model.ProfileView;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.helper.ErrorHandler;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {


    private static final int REQUEST_STORAGE_PERMISSION = 4;
    private static final int REQUEST_READ_PERMISSION = 13;
    private static final int REQUEST_FEED_IMAGE = 4;
    ImageView Profile_image_edit_icon, img_back;
    EditText edit_name, edit_Street, edit_City, edit_state, edit_Pincode, edit_Area;
    TextView usr_phone, usr_mail;
    Button btn_submit_in;
    Context mContext;
    File file;
    private CircleImageView mImageViewProfile;
    private String mImagePath;
    private ProgressDialog mProgressDialog;
    private PreferenceManager mPreference;
    private String mUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initObjects();
        initCallbacks();
        processIntent();

        if (NetworkError.getInstance(mContext).isOnline()) {
            getProfilemethod();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }

    }

    private void initCallbacks() {
        Profile_image_edit_icon.setOnClickListener(this);
        btn_submit_in.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }

    private void initObjects() {

        Profile_image_edit_icon = findViewById(R.id.Profile_image_edit_icon);
        img_back = findViewById(R.id.img_back);
        mImageViewProfile = findViewById(R.id.img_profileEdit);
        edit_name = findViewById(R.id.edit_name);
        usr_phone = findViewById(R.id.usr_phone);
        usr_mail = findViewById(R.id.usr_mail);
        edit_Street = findViewById(R.id.edit_Street);
        edit_City = findViewById(R.id.edit_City);
        edit_state = findViewById(R.id.edit_state);
        edit_Pincode = findViewById(R.id.edit_Pincode);
        edit_Area = findViewById(R.id.edit_Area);
        btn_submit_in = findViewById(R.id.btn_submit_in);
        mContext = this;
        mPreference = new PreferenceManager(this);
        mProgressDialog = new ProgressDialog(mContext);
        edit_name.setText(mPreference.getName());
        usr_phone.setText(mPreference.getPhone());
        usr_mail.setText(mPreference.getEmail());

    }

    @Override
    public void onClick(View v) {

        if (v == Profile_image_edit_icon) {
            processPickImage();
        } else if (v == btn_submit_in) {
            String name = edit_name.getText().toString().trim();
            String email = usr_mail.getText().toString().trim();
            String phone = usr_phone.getText().toString().trim();
            String street = edit_Street.getText().toString();
            String area = edit_Area.getText().toString();
            String city = edit_City.getText().toString();
            String state = edit_state.getText().toString();
            String pincode = edit_Pincode.getText().toString();

            if (validateInput(name, email, phone, street, area, city, state, pincode)) {
                showProgressDialog("Updating....");

                if (NetworkError.getInstance(mContext).isOnline()) {
                    geteditprofile(name, email, phone, street, area, city, state, pincode);
                } else {
                    ToastBuilder.build(mContext, " No Internet Connection");
                }

            }

        } else if (v == img_back) {
            onBackPressed();
        }
    }


    private boolean validateInput(String name, String email, String phone, String street, String area, String city, String state, String pincode) {
        if (TextUtils.isEmpty(name)) {
            edit_name.requestFocus();
            edit_name.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Name"));
            return false;
        } else if (TextUtils.isEmpty(email)) {
            usr_mail.requestFocus();
            usr_mail.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Email"));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            usr_mail.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_invalid), "Email"));
            usr_mail.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(phone)) {
            usr_phone.requestFocus();
            usr_phone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Phone"));
            return false;
        } else if (phone.length() < 10) {
            usr_phone.requestFocus();
            usr_phone.setError("Please enter a Valid Mobile Number");
            return false;
        } else if (TextUtils.isEmpty(street)) {
            edit_Street.requestFocus();
            edit_Street.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Street"));
            return false;
        } else if (TextUtils.isEmpty(area)) {
            edit_Area.requestFocus();
            edit_Area.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Area"));
            return false;
        } else if (TextUtils.isEmpty(city)) {
            edit_City.requestFocus();
            edit_City.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "City"));
            return false;
        } else if (TextUtils.isEmpty(state)) {
            edit_state.requestFocus();
            edit_state.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "State"));
            return false;
        } else if (TextUtils.isEmpty(pincode)) {
            edit_Pincode.requestFocus();
            edit_Pincode.setError(String.format(Locale.getDefault(), getString(R.string.error_empty),
                    "Pincode"));
            return false;
        } else if (pincode.length() < 6) {
            edit_Pincode.requestFocus();
            edit_Pincode.setError("Please Enter Valid Pincode");
            return false;
        }
        return true;
    }

    private void processPickImage() {
        if (hasStoragePermission()) {
            pickImage();
        } else {
            requestStoragePermission(REQUEST_STORAGE_PERMISSION);
        }
    }

    private void pickImage() {
        EasyImage.configuration(mContext).setImagesFolderName(getString(R.string.app_name));
        EasyImage.openChooserWithGallery(this, "Select Image", REQUEST_FEED_IMAGE);
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestStoragePermission(int permission) {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, permission);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles,
                                       EasyImage.ImageSource source, int type) {
                mImagePath = imageFiles.get(0).getPath();
                Log.e("imagepathdataprofile", "" + mImagePath);
                file = new File(mImagePath);
                displayImage();

            }
        });
    }

    private void displayImage() {
        Glide.with(mContext).load(mImagePath).into(mImageViewProfile);
    }

    private void processIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            String action = intent.getAction();
            if (action != null && action.equals(Intent.ACTION_SEND)) {
                Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                mImagePath = imageUri.getPath();
                if (hasStoragePermission()) {
                    displayImage();
                } else {
                    requestStoragePermission(REQUEST_READ_PERMISSION);
                }
            }
        }
    }


    public void getProfilemethod() {
        ApiInterface apiService = Api.getClient().create(ApiInterface.class);
        Call<ProfileView> call = apiService.getProfile("Token " + mPreference.getToken());
        call.enqueue(new Callback<ProfileView>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ProfileView> call, Response<ProfileView> response) {
                ProfileView profileView = response.body();
                if (response.isSuccessful() && profileView != null) {
                    edit_name.setText(profileView.getFirst_name());
                    usr_phone.setText(profileView.getUsername());
                    usr_mail.setText(profileView.getEmail());
                    if (profileView.getUseraddress() != null) {
                        if (profileView.getUseraddress().getStreet() != null &&
                                profileView.getUseraddress().getCity() != null &&
                                profileView.getUseraddress().getCountry() != null &&
                                profileView.getUseraddress().getState() != null &&
                                profileView.getUseraddress().getZipcode() != null &&
                                profileView.getUseraddress().getArea() != null) {
                            edit_Street.setText(profileView.getUseraddress().getStreet());
                            edit_City.setText(profileView.getUseraddress().getCity());
                            edit_Area.setText(profileView.getUseraddress().getArea());
                            edit_state.setText(profileView.getUseraddress().getState());
                            edit_Pincode.setText(profileView.getUseraddress().getZipcode());

                        }
                    }
                    String imageloader = profileView.getUserprofile().getProfile_pic();
                    if (imageloader != null) {
                        Glide.with(mContext).load(imageloader)
                                .apply(new RequestOptions().placeholder(R.drawable.man).error(R.drawable.man))
                                .into(mImageViewProfile);

                    }
                }

            }

            @Override
            public void onFailure(Call<ProfileView> call, Throwable t) {
                Log.e("failuretokenprofile", "" + t.getMessage());
            }
        });
    }


    public void geteditprofile(final String first_Name, final String email,
                               final String mobile, final String street, final String area, final String city,
                               final String state, final String zipcode) {

        JSONObject username = new JSONObject();
        try {
            username.put("area", area);
            username.put("street", street);
            username.put("city", city);
            username.put("state", state);
            username.put("zipcode", zipcode);
            username.put("country", "India");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mImagePath != null) {

            Log.e("methodresponse", "" + mPreference.getToken());
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("profile_pic", file.getName(), requestBody);
            RequestBody first_Name1 = RequestBody.create(MediaType.parse("form-data"), first_Name);
            RequestBody email1 = RequestBody.create(MediaType.parse("form-data"), email);
            RequestBody mobile1 = RequestBody.create(MediaType.parse("form-data"), mobile);
            RequestBody useraddress = RequestBody.create(MediaType.parse("multipart/form-data"), username.toString());
            ApiInterface newsFeedService = Api.getClient().create(ApiInterface.class);
            Call<ProfileView> call = newsFeedService.getEditProfile("Token " + mPreference.getToken(),
                    first_Name1, email1, mobile1, useraddress, imageFileBody);
            call.enqueue(new retrofit2.Callback<ProfileView>() {

                @Override
                public void onResponse(@NonNull Call<ProfileView> call, @NonNull Response<ProfileView> response) {
                    ProfileView profileView = response.body();
                    if (response.isSuccessful() && profileView != null) {
                        hideProgressDialog();
                        Toast.makeText(mContext, "Update Sucessfully", Toast.LENGTH_SHORT).show();
                        edit_name.setText(profileView.getFirst_name());
                        usr_phone.setText(profileView.getUsername());
                        usr_mail.setText(profileView.getEmail());
                        edit_Street.setText(profileView.getUseraddress().getStreet());
                        edit_City.setText(profileView.getUseraddress().getCity());
                        edit_Area.setText(profileView.getUseraddress().getArea());
                        edit_state.setText(profileView.getUseraddress().getState());
                        edit_Pincode.setText(profileView.getUseraddress().getZipcode());

                        onBackPressed();

                    } else {
                        ErrorHandler.processError(mContext, response.code(), response.errorBody());
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ProfileView> call, @NonNull Throwable t) {
                    hideProgressDialog();
                    ToastBuilder.build(mContext, t.getMessage());
                }
            });

        } else {
            RequestBody first_Name1 = RequestBody.create(MediaType.parse("form-data"), first_Name);
            RequestBody email1 = RequestBody.create(MediaType.parse("form-data"), email);
            RequestBody mobile1 = RequestBody.create(MediaType.parse("form-data"), mobile);
            RequestBody useraddress = RequestBody.create(MediaType.parse("multipart/form-data"), username.toString());
            ApiInterface newsFeedService = Api.getClient().create(ApiInterface.class);
            Call<ProfileView> call = newsFeedService.getEditProfile1("Token " + mPreference.getToken(),
                    first_Name1, email1, mobile1, useraddress);
            call.enqueue(new retrofit2.Callback<ProfileView>() {

                @Override
                public void onResponse(Call<ProfileView> call, Response<ProfileView> response) {
                    ProfileView profileView = response.body();
                    if (response.isSuccessful() && profileView != null) {
                        hideProgressDialog();
                        Toast.makeText(mContext, " UpdateSucessfully", Toast.LENGTH_SHORT).show();
                        edit_name.setText(profileView.getFirst_name());
                        usr_phone.setText(profileView.getUsername());
                        usr_mail.setText(profileView.getEmail());
                        edit_Street.setText(profileView.getUseraddress().getStreet());
                        edit_Area.setText(profileView.getUseraddress().getArea());
                        edit_Pincode.setText(profileView.getUseraddress().getZipcode());
                        edit_City.setText(profileView.getUseraddress().getCity());
                        edit_state.setText(profileView.getUseraddress().getState());
                        onBackPressed();
                    } else {
                        hideProgressDialog();
                        ErrorHandler.processError(mContext, response.code(), response.errorBody());
                    }

                }

                @Override
                public void onFailure(Call<ProfileView> call, Throwable t) {
                    hideProgressDialog();
                    Log.e("editprofilefailure111", "" + t.getMessage().toString());
                }
            });

        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}

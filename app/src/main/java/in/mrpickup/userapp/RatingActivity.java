package in.mrpickup.userapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.mrpickup.userapp.Model.CompletedBookingItem;
import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.Constant;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;
import in.mrpickup.userapp.helper.Utils;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Admin on 7/7/2017.
 */

public class RatingActivity extends AppCompatActivity implements View.OnClickListener,
        RatingBar.OnRatingBarChangeListener {

    AlertDialog alertDialog;
    private Context mContext;
    private ImageView mImageViewCancel;
    private TextView mTextViewName, mTextViewDate, mTextViewTripId, mTextViewPaymentMode,
            mTextViewPrice, mTextViewLocation, mTextViewRating, mTextViewLogComplaint;
    private RatingBar mRatingBar;
    private Button mButtonSubmit;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;
    private int mBookingId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        initObjects();
        initCallbacks();
        initRating();
        setRatingText(5);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mImageViewCancel) {
            mImageViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else if (v == mButtonSubmit) {
            processRating();
        }
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        setRatingText(rating);
    }

    private void initObjects() {
        mImageViewCancel = findViewById(R.id.img_cancel);
        mTextViewName = findViewById(R.id.txt_name);
        mTextViewDate = findViewById(R.id.txt_date);
        mTextViewTripId = findViewById(R.id.txt_trip_id);
        mTextViewPaymentMode = findViewById(R.id.txt_payment_mode);
        mTextViewPrice = findViewById(R.id.txt_price);
        mTextViewLocation = findViewById(R.id.txt_location);
        mTextViewRating = findViewById(R.id.txt_rating);
        mRatingBar = findViewById(R.id.rating);
        mButtonSubmit = findViewById(R.id.btn_submit);

        mContext = this;
        mPreferenceManager = new PreferenceManager(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mImageViewCancel.setOnClickListener(this);
        mButtonSubmit.setOnClickListener(this);
        mRatingBar.setOnRatingBarChangeListener(this);
    }

    private void initRating() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            CompletedBookingItem completedBookingItem =
                    (CompletedBookingItem) bundle.getSerializable(Constant.EXTRA_BOOKING);
            if (completedBookingItem != null) populateData(completedBookingItem);
        } else {
            populateData();
        }
    }

    private void populateData(CompletedBookingItem completedBookingItem) {
        mBookingId = completedBookingItem.getId();
        String name = completedBookingItem.getName();
       /* String paymentMode = String.format(Locale.getDefault(),
                getString(R.string.format_payment_mode));
              *//*  completedBookingItem.getPaymentMode());*/
        String address = String.format(Locale.getDefault(), getString(R.string.format_pickup_location), completedBookingItem.getAddress());
        String cost = String.valueOf("\u20b9 " + completedBookingItem.getPrice());
        String createdOn = completedBookingItem.getDate();

        mTextViewTripId.setText(String.format(Locale.getDefault(), getString(R.string.format_trip_id),
                mBookingId));
        mTextViewName.setText(name);
      /*  mTextViewPaymentMode.setText(paymentMode);*/
        mTextViewLocation.setText(address);
        mTextViewPrice.setText(cost);
        mTextViewDate.setText(createdOn);
    }

    private void populateData() {
        String booking = mPreferenceManager.getBooking();
        if (booking != null) {
            try {
                JSONObject bookingObject = new JSONObject(booking);
                mBookingId = bookingObject.getInt(Api.KEY_ID);
                String name = bookingObject.getString(Api.KEY_FIRST_NAME);
                String createdOn = Utils.parseCreatedDate(bookingObject.getString("created_on"));
                String cost = String.format(Locale.getDefault(), getString(R.string.format_price), bookingObject.get(Api.KEY_DELIVERY_COST));

                String address = String.format(Locale.getDefault(), getString(R.string.format_pickup_location), bookingObject.getString(Api.KEY_DELIVER_LOC));
              /*  String paymentMode = String.format(Locale.getDefault(),
                        getString(R.string.format_payment_mode));*/
                     /*   bookingObject.getString(KEY_PAYMENT_MODE))*/

                mTextViewTripId.setText(
                        String.format(Locale.getDefault(), getString(R.string.format_trip_id),
                                mBookingId));
                mTextViewName.setText(name.trim());
              /*  mTextViewPaymentMode.setText(paymentMode);*/
                mTextViewLocation.setText(address);
                mTextViewPrice.setText(cost);
                mTextViewDate.setText(createdOn);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void processRating() {

        View otpView = LayoutInflater.from(mContext).inflate(R.layout.dialog_description, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(otpView);
        alertDialog = builder.create();
        alertDialog.show();
        final EditText txt_Otp = otpView.findViewById(R.id.input_otp);
        final Button startBtn = otpView.findViewById(R.id.start_btn);
        ImageView imageViewClose = otpView.findViewById(R.id.img_close1);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = txt_Otp.getText().toString().trim();
                float rating = mRatingBar.getRating();
                    showProgressDialog("Submitting..");

                if (NetworkError.getInstance(mContext).isOnline()) {
                    submitRating(getRatingRequestJson(rating, otp));
                } else {
                    ToastBuilder.build(mContext, " No Internet Connection");
                }



            }
        });
    }

    private JSONObject getRatingRequestJson(float rating, String comments) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Api.KEY_RATING, rating);
            jsonObject.put(Api.KEY_COMMENTS, comments);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void submitRating(JSONObject ratingRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, Api.RATING + mBookingId + "/submit_rating/", ratingRequestJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();
                        mPreferenceManager.setBooking(null);
                        ToastBuilder.build(mContext, "Thanks for your valuable feedback");
                        alertDialog.dismiss();

                        Activity.launchClearStack(mContext, PickUpActivity.class);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreferenceManager.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "rating");
    }

    private void setRatingText(float rating) {
        int position = 3;
        if (rating >= 0 && rating <= 1) {
            position = 0;
        } else if (rating > 1 && rating <= 2) {
            position = 1;
        } else if (rating > 2 && rating <= 3) {
            position = 2;
        } else if (rating > 3 && rating <= 4) {
            position = 3;
        } else if (rating > 4 && rating <= 5) {
            position = 4;
        }
        String[] ratingArray = getResources().getStringArray(R.array.rating);
        mTextViewRating.setText(ratingArray[position]);
    }


    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}


package in.mrpickup.userapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUp extends AppCompatActivity {
    private static final int REQUEST_READ_SMS = 4;
    String phone, name;
    private Button mButtonSignUp;
    private Context mContext;
    private Toolbar mToolbar;
    private EditText mEditTextName;
    private EditText mEditTextPhone;
    private EditText mEditTextPass;
    private EditText mEditTextEmail;
    private ProgressDialog mProgressDialog;
    private PreferenceManager mPreferenceManager;
    private CheckBox mCheckboxConditions;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initObjects();
        setupToolbar();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClick(View view) {
        if (view == mButtonSignUp) {
            processSignUp();
        }
    }

    private void initObjects() {
        mButtonSignUp = findViewById(R.id.btn_sign_up);
        mEditTextName = findViewById(R.id.input_name);
        mEditTextEmail = findViewById(R.id.input_email);
        mEditTextPhone = findViewById(R.id.input_phone);
        mEditTextPass = findViewById(R.id.input_pass);
        mCheckboxConditions = findViewById(R.id.checkbox);
        mContext = this;
        mProgressDialog = new ProgressDialog(mContext);
        mPreferenceManager = new PreferenceManager(this);
        mToolbar = findViewById(R.id.toolbar);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Sign Up");
        mToolbar.setNavigationIcon(R.drawable.ic_left_arrow_white);
        mToolbar.setTitleTextColor(ContextCompat.getColor(mContext, R.color.shade_white_cc));
    }

    private void processSignUp() {
        name = mEditTextName.getText().toString().trim();
        String email = mEditTextEmail.getText().toString().trim();
        phone = mEditTextPhone.getText().toString().trim();
        String pass = mEditTextPass.getText().toString().trim();
        if (validateInput(name, email, phone, pass)) {
            if (mCheckboxConditions.isChecked()) {
                if (hasReadSmsPermission()) {
                    showProgressDialog("Creating account..");
                    if (NetworkError.getInstance(this).isOnline()) {
                        signUpUser(getSignUpRequestJson(name, email, phone, pass));
                    } else {
                        ToastBuilder.build(this, " No Internet Connection");
                    }
                } else {
                    requestReadSmsPermission();
                }

            } else {
                ToastBuilder.build(mContext, "Please Accept the terms and Conditions");
            }
        }

    }

    private JSONObject getSignUpRequestJson(String name, String email, String phone, String pass) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Api.KEY_FIRST_NAME, name);
            jsonObject.put(Api.KEY_EMAIL, email);
            jsonObject.put(Api.KEY_USERNAME, phone);
            jsonObject.put(Api.KEY_PASSWORD, pass);
            Log.d("request", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    //TODO: add password regex validation
    private boolean validateInput(String name, String email, String phone, String pass) {
        if (TextUtils.isEmpty(name)) {
            mEditTextName.requestFocus();
            mEditTextName.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Name"));
            return false;
        } else if (TextUtils.isEmpty(email)) {
            mEditTextEmail.requestFocus();
            mEditTextEmail.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Email"));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEditTextEmail.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_invalid), "Email"));
            mEditTextEmail.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(phone)) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Phone Number"));
            return false;
        } else if (phone.length() < 10) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "Phone Number", 10, "digits"));
            return false;
        } else if (TextUtils.isEmpty(pass)) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Password"));
            return false;
        } else if (pass.length() < 8) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(getString(R.string.error_pass_length));
            return false;
        }
        return true;
    }

    private void signUpUser(JSONObject signUpRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Api.REGISTER_URL,
                signUpRequestJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                try {
                    String name1 = response.getString(Api.KEY_FIRST_NAME);
                    String phone = response.getString(Api.KEY_USERNAME);
                    mPreferenceManager.setName(name1);
                    mPreferenceManager.setPhone(phone);

                    ToastBuilder.build(mContext, "Account created");
                    Activity.launch(mContext, OtpActivity.class);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "sign_up");
    }

    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestReadSmsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS},
                REQUEST_READ_SMS);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_SMS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    processSignUp();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}

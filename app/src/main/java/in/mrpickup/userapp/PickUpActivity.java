package in.mrpickup.userapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.mrpickup.userapp.Adapter.RateCardListAdapter;
import in.mrpickup.userapp.Model.FcmToken;
import in.mrpickup.userapp.Model.Rates;
import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;
import in.mrpickup.userapp.callback.ProductCallback;
import in.mrpickup.userapp.helper.ErrorHandler;
import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.userapp.app.Constant.SUPPORT_PHONE;
import static in.mrpickup.userapp.helper.MacAddress.getMacAddress;


public class PickUpActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, View.OnClickListener, ProductCallback {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final int REQUEST_ENABLE_LOCATION = 4;
    private static final int REQUEST_SOURCE = 1;
    private static final int REQUEST_DESTINATION = 2;
    public Double lLat, lLng;
    public List<Fragment> mFragmentList;
    SupportMapFragment mMapFragment;
    Marker mCurrLocationMarker, mDestination;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    ArrayList<HashMap<String, String>> Category = new ArrayList<>();
    String parsedDistance;
    String response;
    String distancekm;
    private ImageView mPencil;
    private NavigationView mNavigationView;
    private EditText mEditText;
    private DrawerLayout mDrawerLayout;
    private GoogleMap mGoogleMap;
    private EditText mTextViewLocation;
    private ActionBarDrawerToggle mDrawerToggle;
    private Context mContext;
    private Button mpickup;
    private Button mbuy;
    private String mAddress;
    private ProgressDialog mProgressDialog;
    private GoogleApiClient mGoogleApiClient;
    private Toolbar mToolbar;
    private PreferenceManager mPreferenceManager;
    private FloatingActionButton locButtom;
    private RateCardListAdapter mAdapter;
    private RateCardListAdapter mAdapter1;
    private ArrayList<Rates> mMyPhotoList;
    private ArrayList<Rates> mMyPhotoList1;

    public PickUpActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_up);
        initObjects();
        initCallbacks();
        initMap();
        setupToolbar();
        setFooterTextSpan();
        setupDrawerToggle();
        //  fcmToken();

        if (NetworkError.getInstance(this).isOnline()) {
            updateFcm();
        } else {
            ToastBuilder.build(this, " No Internet Connection");
        }

    }

    private void initObjects() {
        mPreferenceManager = new PreferenceManager(this);
        mEditText = findViewById(R.id.input_destination);
        mTextViewLocation = findViewById(R.id.txt_location);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mbuy = findViewById(R.id.buy);
        mbuy.setEnabled(false);
        mpickup = findViewById(R.id.upickup);
        mpickup.setEnabled(false);
        mToolbar = findViewById(R.id.toolbar);
        locButtom = findViewById(R.id.locBtn);
        mProgressDialog = new ProgressDialog(this);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mContext = this;
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close);
        mFragmentList = new ArrayList<>();
        mNavigationView = findViewById(R.id.nav_view);
        mPencil = findViewById(R.id.pencil);
        Log.e("fcmresponse", "" + mPreferenceManager.getToken());
    }

    private void initCallbacks() {
        mNavigationView.setNavigationItemSelectedListener(this);
        mbuy.setOnClickListener(this);
        mEditText.setOnClickListener(this);
        mpickup.setOnClickListener(this);
        mTextViewLocation.setOnClickListener(this);
        locButtom.setOnClickListener(this);
        mPencil.setOnClickListener(this);
    }

    private void initMap() {
        mMapFragment.getMapAsync(this);
    }

    private void initLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @SuppressWarnings("deprecation")
    private void displayLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest)
                        .setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(PickUpActivity.this,
                                    REQUEST_ENABLE_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.orders:
                Activity.launch(this, TripsActivity.class);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawers();
        return onNavClick(item.getItemId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.nav_orders, menu);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Home");
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_button);
    }

    private void setupDrawerToggle() {
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    private void setFooterTextSpan() {
        View viewBilliontags = mNavigationView.getMenu().findItem(
                R.id.nav_billiontags).getActionView();
        TextView textViewBilliontags = viewBilliontags.findViewById(
                R.id.txt_billiontags);
        SpannableString string = new SpannableString("Copy rights by Mr.Pickup");

        string.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.colorAccent)),
                12, 24, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewBilliontags.setText(string);
    }

    private boolean onNavClick(int itemId) {
        switch (itemId) {
            case R.id.nav_home:
                Activity.launch(mContext, PickUpActivity.class);
                return false;
            case R.id.nav_profile:
                if (NetworkError.getInstance(mContext).isOnline()) {
                    getRates();
                } else {
                    ToastBuilder.build(mContext, " No Internet Connection");
                }

                processratecard();
                return false;
            case R.id.nav_viewProfile:
                Activity.launch(mContext, ProfileActivity.class);
                return true;
            case R.id.nav_offers:
                Activity.launch(mContext, OffersActivity.class);
                return true;
            case R.id.nav_orders:
                Activity.launch(mContext, TripsActivity.class);
                return true;
            case R.id.nav_share_app:
                shareApp();
                return false;
            case R.id.nav_call_us_now:
                openDialPad(SUPPORT_PHONE);
                return false;
            case R.id.nav_about_us:
                Activity.launch(mContext, AboutUs.class);
                return false;
            case R.id.nav_privacypolicy:
                Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://mrpickup.in/privacy.html"));
                startActivity(browserIntent);
                return false;
            case R.id.nav_terms:
                Intent browserIntent1 = new Intent("android.intent.action.VIEW", Uri.parse("http://mrpickup.in/terms.html"));
                startActivity(browserIntent1);
                return false;
            case R.id.nav_logout:
                promptLogoutDialog();
                return false;
            default:
                return false;
        }
    }

    private void processratecard() {
        @SuppressLint("InflateParams")
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_rate_card_recycler, null);


        ImageView imageViewClose = mView.findViewById(R.id.img_close);

        RecyclerView recyclerView = mView.findViewById(R.id.ratecard);
        RecyclerView recyclerView1 = mView.findViewById(R.id.ratecardurgent);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView1.setLayoutManager(mLayoutManager1);
        mMyPhotoList = new ArrayList<>();
        mMyPhotoList1 = new ArrayList<>();
        mAdapter = new RateCardListAdapter(mContext, mMyPhotoList, this);
        mAdapter1 = new RateCardListAdapter(mContext, mMyPhotoList1, this);
        recyclerView.setAdapter(mAdapter);
        recyclerView1.setAdapter(mAdapter1);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(mView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void shareApp() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                getString(R.string.app_name) + " https://play.google.com/store/apps/details?id="
                        + getPackageName());
        startActivity(Intent.createChooser(shareIntent, "Share App"));
    }

    private void openDialPad(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }

    private void promptLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to Logout?");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog("Logging out..");


            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void logoutUser() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.LOGOUT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleLogoutResponse();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreferenceManager.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "logout");
    }

    private void handleLogoutResponse() {
        mPreferenceManager.clearUser();
        ToastBuilder.build(mContext, "You\'ve been logged out");
        Activity.launchClearStack(this, ChooseActivity.class);
    }

    @Override
    public void onClick(View v) {
        if (v == mEditText) {
            if (NetworkError.getInstance(this).isOnline()) {
                placePicker(REQUEST_DESTINATION);
            } else {
                ToastBuilder.build(this, " No Internet Connection");
            }

        } else if (v == mbuy) {
            if (NetworkError.getInstance(this).isOnline()) {
                bundleBuy();
            } else {
                ToastBuilder.build(this, " No Internet Connection");
            }

        } else if (v == mpickup) {
            LatLng src = mCurrLocationMarker.getPosition();
            LatLng dest = mDestination.getPosition();
            try {
                getDistance(src.latitude, src.longitude, dest.latitude, dest.longitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (v == locButtom) {
            getCurrentLocation();
        } else if (v == mPencil) {
            placePicker(REQUEST_SOURCE);
        }
    }

    public void getDistance(final double lat1, final double lon1, final double lat2, final double lon2) throws JSONException {
        String sourcelat = String.valueOf(lat1) + "," + String.valueOf(lon1);
        String destlat = String.valueOf(lat2) + "," + String.valueOf(lon2);
        JSONObject locationJsonObject = new JSONObject();
        locationJsonObject.put("origin", sourcelat);
        locationJsonObject.put("destination", destlat);
        LatlngCalc(locationJsonObject);
    }

    private void LatlngCalc(JSONObject locationJsonObject) throws JSONException {

        RequestQueue queue = Volley.newRequestQueue(PickUpActivity.this);
        String url = "http://maps.googleapis.com/maps/api/distancematrix/" +
                "json?origins=" + locationJsonObject.getString("origin") + "&destinations=" + locationJsonObject.getString("destination") + "&mode=driving&" +
                "language=en-EN&sensor=false";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray userObj = jsonObject.getJSONArray("rows");
                            for (int i = 0; i < userObj.length(); i++) {
                                JSONObject userObj22 = userObj.getJSONObject(i);
                                JSONArray userObj11 = userObj22.getJSONArray("elements");
                                for (int j = 0; j < userObj11.length(); j++) {
                                    JSONObject userObj111 = userObj11.getJSONObject(i);
                                    JSONObject distance12 = userObj111.getJSONObject("distance");
                                    distancekm = distance12.getString("text");
                                    distancekm = distancekm.replace(" km", "");
                                    distancekm = distancekm.replace(" m", "");
                                    Log.e("text", String.format("%s", distancekm));
                                    processBundle(distancekm);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  mTextView.setText("That didn't work!");
                Log.e("weroor", "error");
            }
        });
        queue.add(stringRequest);
    }


    private void processBundle(String distancekm1) {
        LatLng src = mCurrLocationMarker.getPosition();
        LatLng dest = mDestination.getPosition();
        Intent intent = new Intent(getBaseContext(), CatergoryActivity.class);
        intent.putExtra("SOURCE_LAT", src.latitude);
        intent.putExtra("DESTINATION_LAT", dest.latitude);
        intent.putExtra("SOURCE_LNG", src.longitude);
        intent.putExtra("DESTINATION_LNG", dest.longitude);
        intent.putExtra("DistanceKm", distancekm1);
        startActivity(intent);

    }

    private void bundleBuy() {
        LatLng src = mCurrLocationMarker.getPosition();
        LatLng dest = mDestination.getPosition();
        Intent intent = new Intent(getBaseContext(), BuyActivity.class);
        intent.putExtra("SOURCE_LAT", src.latitude);
        intent.putExtra("DESTINATION_LAT", dest.latitude);
        intent.putExtra("SOURCE_LNG", src.longitude);
        intent.putExtra("DESTINATION_LNG", dest.longitude);
        startActivity(intent);

    }

    private void placePicker(int request) {
        AutocompleteFilter filter = new AutocompleteFilter.Builder()
                .setCountry("IN")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();

        PlaceAutocomplete.IntentBuilder builder = new PlaceAutocomplete.IntentBuilder(
                PlaceAutocomplete.MODE_OVERLAY);
        builder.setFilter(filter);
        try {
            Intent intent = builder.build(this);
            startActivityForResult(intent, request);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException
                e) {
            ToastBuilder.build(mContext, e.getMessage());
        }

    }

    private void applyMapStyle() {
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json);
        mGoogleMap.setMapStyle(style);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            Place place = PlaceAutocomplete.getPlace(this, data);
            if (place != null) {
                Double pLat = place.getLatLng().latitude;
                Double pLon = place.getLatLng().longitude;
                mAddress = place.getAddress().toString();
                if (requestCode == REQUEST_DESTINATION) {
                    setMarkerDest(pLat, pLon);
                    moveCamera(pLat, pLon);

                    displayAddress();
                } else if (requestCode == REQUEST_SOURCE) {
                    setMarker(pLat, pLon);
                    moveCamera(pLat, pLon);

                    displaySourceAddr();
                }
            }
        }
    }

    private void moveCamera(Double lat, Double lng) {
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 17));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @SuppressWarnings("deprecation")
    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @SuppressWarnings("deprecation")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        } else {
            requestLocationPermission();
        }
    }

    private void displayAddress() {
        if (mAddress != null) mEditText.setText(mAddress);

    }

    private void displaySourceAddr() {
        if (mAddress != null) mTextViewLocation.setText(mAddress);

    }

    @SuppressLint("StaticFieldLeak")
    private void displayAddressAsync(final double latitude, final double longitude) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                Log.e("display", "" + longitude + "," + latitude);
                return getCompleteAddress(latitude, longitude);

            }

            @Override
            protected void onPostExecute(String result) {
                Log.e("onPostExecute", "" + result);
                super.onPostExecute(result);
                mTextViewLocation.setText(result);


            }
        }.execute();
    }

    private String getCompleteAddress(double latitude, double longitude) {
        mAddress = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {

                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");

                    Log.e("result12", "" + strReturnedAddress.toString());

                }
                mAddress = strReturnedAddress.toString();

                Log.e("mAddress", "" + mAddress);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mAddress.isEmpty() ? "Service not available here" : mAddress;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        getCurrentLocation();


    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        ToastBuilder.build(mContext, connectionResult.getErrorMessage());
    }

    private void setMarker(Double mlat, Double mlng) {
        LatLng latLng = new LatLng(mlat, mlng);
        if (mCurrLocationMarker == null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);
            //move map camera
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));

        } else mCurrLocationMarker.setPosition(latLng);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));

    }

    private void setMarkerDest(Double mlat, Double mlng) {
        LatLng latLng = new LatLng(mlat, mlng);
        if (mDestination == null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mDestination = mGoogleMap.addMarker(markerOptions);
        } else mDestination.setPosition(latLng);
        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        mbuy.setEnabled(true);
        mpickup.setEnabled(true);
        mbuy.setBackgroundResource(R.color.colorPrimaryDark);
        mpickup.setBackgroundResource(R.color.colorPrimaryDark);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        lLat = location.getLatitude();
        lLng = location.getLongitude();
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Log.e("methodlocation", "" + longitude + "," + latitude);

            displayAddressAsync(latitude, longitude);
            setMarker(latitude, longitude);
            moveCamera(latitude, longitude);
            //moving the map to location
        } else movecameradefault();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        enablemyLocation();
        applyMapStyle();
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission


                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                checkLocationPermission();
            }
        } else {
            mGoogleMap.setMyLocationEnabled(true);
        }

        initLocationRequest();
        buildGoogleApiClient();
        displayLocationSettingsRequest();

    }

    private void enablemyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && mGoogleApiClient != null) {
            mGoogleMap.setMyLocationEnabled(true);
        } else {
            requestLocationPermission();
        }
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                4);
    }

    private void movecameradefault() {
        double lat = Double.parseDouble(mPreferenceManager.getLat());
        double lng = Double.parseDouble(mPreferenceManager.getLng());
        moveCamera(lat, lng);
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(PickUpActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);

                                getCurrentLocation();
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }

    }


    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void updateFcm() {
        Log.e("fcmToken", "" + mPreferenceManager.getFcmToken());
        if (mPreferenceManager.isTokenUploaded() || mPreferenceManager.getFcmToken() == null)
            return;

        ApiInterface authService = Api.getClient().create(ApiInterface.class);
        Call<Void> call = authService.updateFcm("Token " + mPreferenceManager.getToken(),
                new FcmToken(getMacAddress(), mPreferenceManager.getFcmToken()));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull retrofit2.Response<Void> response) {
                if (response.isSuccessful()) {
                    mPreferenceManager.setTokenUploaded(true);
                } else {

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {

            }
        });
    }


    private void getRates() {
        ApiInterface apiInterface = Api.getClient().create(ApiInterface.class);
        Call<ArrayList<Rates>> call = apiInterface.getRates("Token " + mPreferenceManager.getToken());
        call.enqueue(new retrofit2.Callback<ArrayList<Rates>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<Rates>> call, @NonNull retrofit2.Response<ArrayList<Rates>> response) {
                List<Rates> rates = response.body();
                if (response.isSuccessful() && rates != null) {

                    mMyPhotoList.clear();
                    mMyPhotoList1.clear();
                    for (int i = 0; i < rates.size(); i++) {
                        if (rates.get(i).getmDeliveryType().equals("OR")) {
                            mMyPhotoList.add(rates.get(i));
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mMyPhotoList1.add(rates.get(i));
                            mAdapter1.notifyDataSetChanged();
                        }
                    }

                } else {

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Rates>> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    @Override
    public void onItemRemoveClick(int position) {

    }
}

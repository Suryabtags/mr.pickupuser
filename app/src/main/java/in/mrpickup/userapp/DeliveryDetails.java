package in.mrpickup.userapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.ToastBuilder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeliveryDetails extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_IMAGE = 4;
    private static final int REQUEST_WRITE_STORAGE = 4;

    private Toolbar mToolbar;
    private Context mContext;
    private TextInputLayout mcontact;
    private ImageView mImageViewMedia;
    private FloatingActionButton fab;
    private Button mPickNow;
    private Button mPickLater;
    private LinearLayout mLayoutMediaContainer;
    private TextInputEditText mcost;
    private Spinner spinner1, spinner2;
    private ProgressDialog mProgressDialog;
    private TextInputEditText mtime;
    private List<File> mImageArrayList;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_details);
        mToolbar = findViewById(R.id.toolbar);
        setupToolbar();
      /*  addListenerOnButton();
        addListenerOnSpinnerItemSelection();*/
        mContext = this;
        initObjects();

    }

    private void initObjects() {
        mPickNow = findViewById(R.id.btn_pick_now);
        mPickNow.setOnClickListener(this);
        mPickLater = findViewById(R.id.btn_pick_later);
        mPickLater.setOnClickListener(this);
        mcontact = findViewById(R.id.contact_deliver_number);
        mcontact.setOnClickListener(this);
       /* mdate =(AutoCompleteTextView)findViewById(R.id.input_date);
        mdate.setOnClickListener(this);*/
       /* mImageViewMedia = (ImageView) findViewById(R.id.img_media);
        mImageViewMedia.setOnClickListener(this);
        mLayoutMediaContainer = (LinearLayout) findViewById(R.id.media_container);
        mImageArrayList = new ArrayList<>();
        mProgressDialog = new ProgressDialog(mContext);
        mcost = (TextInputEditText) findViewById(R.id.input_cost);
        mcost.setOnClickListener(this);*/
        /*mdate =(TextInputEditText) findViewById(R.id.input_date);
        mdate.setOnClickListener(this);
        mtime =(TextInputEditText)findViewById(R.id.input_time);
        mtime.setOnClickListener(this);*/
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Delivery Details");
        mToolbar.setNavigationIcon(R.drawable.ic_left_arrow_white);
        mToolbar.setTitleTextColor(getColor(R.color.shade_white_cc));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    

    /*public void addItemsOnSpinner2() {
        List<String> list = new ArrayList<String>();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    public void addListenerOnSpinnerItemSelection() {
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    public void addListenerOnButton() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source,
                                       int type) {
                mImageArrayList = imageFiles;
                showMedia();
            }
        });
    }
*/

    @Override
    public void onClick(View v) {
        if (v == mPickNow) {
            promptBookingDialog();
        } else if (v == mPickLater) {
            processPickLaterDialog();
        }
        if (v == mcontact) {
            processContact();
        }
        /*if (v == mcost) {
            processratecard();
        }
        if (v == mImageViewMedia) {
            processPickImages();
        }
        if (v == mtime) {
//            promptDatetimePickerDialog();
        }*/
    }

    private void processContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

    }

    private void processratecard() {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_rate_card, null);
        ImageView imageViewClose = mView.findViewById(R.id.img_close);
       /* final TextView editTextDuration = (EditText) mView.findViewById(R.id.input_duration);
        final EditText editTextDate = (EditText) mView.findViewById(R.id.input_date);
        final EditText editTextDesc = (EditText) mView.findViewById(R.id.input_comments);*/

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(mView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void processPickLaterDialog() {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_media, null);
        ImageView imageViewClose = mView.findViewById(R.id.img_close);
        final EditText editTextDuration = mView.findViewById(R.id.input_duration);
        final EditText editTextDate = mView.findViewById(R.id.input_date);


        final Button buttonAdd = mView.findViewById(R.id.btn_ad);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(mView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();


        editTextDate.setText(getCurrentDate());
        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptDatePickerDialog(editTextDate);
            }
        });

        editTextDuration.setText(getCurrentTime());
        editTextDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptTimePickerDialog(editTextDuration);
            }
        });
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String time = editTextDuration.getText().toString().trim();

                String duration = editTextDuration.getText().toString().trim();
                String date = editTextDate.getText().toString().trim();

                String dateh = date + " " + duration + ":00";


                if (dateh.isEmpty()) {
                    ToastBuilder.build(mContext,
                            String.format(Locale.getDefault(), getString(R.string.error_empty),
                                    "Date"));
                } else if (duration.isEmpty()) {
                    ToastBuilder.build(mContext,
                            String.format(Locale.getDefault(), getString(R.string.error_empty),
                                    "Duration"));
                } else {
                    /*showProgressDialog("Adding..");*/
                    Activity.launch(mContext, TripsActivity.class);
                  /*  placeOrder(getOrderRequestJson(dateh, desc, id));*/


                }
            }
        });
    }

    private void promptDatePickerDialog(EditText editDate) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        openDatePickerDialog(editDate, year, month, dayOfMonth);
    }

    private void promptTimePickerDialog(EditText editTextDuration) {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY) + 1;
        int minute = calendar.get(Calendar.MINUTE);
        openTimePickerDialog(editTextDuration, hourOfDay, minute);


    }

    private void openDatePickerDialog(final EditText editDate, int year, int month,
                                      int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        editDate.setText(getDate(year, month + 1, dayOfMonth));
                    }
                }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void openTimePickerDialog(final EditText editDuration, int hourOfDay, int minute) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(mContext,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        editDuration.setText(getTime(hourOfDay, minute));
                    }
                }, hourOfDay, minute, false);
        timePickerDialog.show();
    }


    private String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        return getDate(year, month + 1, dayOfMonth);
    }

    private String getDate(int year, int month, int dayOfMonth) {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (dayOfMonth < 10 ? "0"
                + dayOfMonth : dayOfMonth);
    }

    private String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY) + 1;
        int minute = calendar.get(Calendar.MINUTE);
        return getTime(hourOfDay, minute);
    }

    private String getTime(int hourOfDay, int minute) {
        return (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute
                : minute);
    }

    /* private void checkImage() {
         int imageCount = mImageArrayList.size();
         if (imageCount == 0) {
             mImageViewMedia.setVisibility(View.VISIBLE);
             mLayoutMediaContainer.setVisibility(View.GONE);
         } else {
             mImageViewMedia.setVisibility(View.VISIBLE);
             mLayoutMediaContainer.setVisibility(View.VISIBLE);
         }
     }

     private boolean hasWriteStoragePermission() {
         return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                 == PackageManager.PERMISSION_GRANTED;
     }

     private void requestWriteStoragePermission() {
         ActivityCompat.requestPermissions(this,
                 new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
     }

     private void showMedia() {
         mLayoutMediaContainer.removeAllViews();

         checkImage();

         int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,
                 getResources().getDisplayMetrics());
         int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,
                 getResources().getDisplayMetrics());

         FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, height);

         for (int i = 0; i < mImageArrayList.size(); i++) {
             final String path = mImageArrayList.get(i).getPath();
             final View viewImageHolder = LayoutInflater.from(this).inflate(R.layout.item_media,
                     null);
             final ImageView imageViewThumbnail = (ImageView) viewImageHolder.findViewById(
                     R.id.img_media);
             final ImageView imageViewRemove = (ImageView) viewImageHolder.findViewById(
                     R.id.img_remove);

             Glide.with(this).load(path).fitCenter().into(imageViewThumbnail);

             imageViewThumbnail.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     displayImage(path);
                 }
             });

             imageViewRemove.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     int index = ((LinearLayout) viewImageHolder.getParent()).indexOfChild(
                             viewImageHolder);
                     mImageArrayList.remove(index);
                     mLayoutMediaContainer.removeViewAt(index);
                     checkImage();
                 }
             });

             imageViewThumbnail.setLayoutParams(layoutParams);
             mLayoutMediaContainer.addView(viewImageHolder);
         }
     }

     private void processPickImages() {
         if (hasWriteStoragePermission()) {
             pickImages();
         } else {
             requestWriteStoragePermission();
         }
     }

     private void pickImages() {
         EasyImage.configuration(mContext)
                 .setImagesFolderName(getString(R.string.app_name))
                 .setAllowMultiplePickInGallery(true);
         EasyImage.openChooserWithGallery(this, "Select Images", REQUEST_IMAGE);
     }

     @SuppressLint("InflateParams")
     private void displayImage(String path) {
         View mediaView = LayoutInflater.from(mContext).inflate(R.layout.dialog_media, null);

         ImageView imageViewGrievance = (ImageView) mediaView.findViewById(R.id.img_media);

         AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
         alertDialog.setView(mediaView);
         alertDialog.show();

         Glide.with(this).load(path).into(imageViewGrievance);
     }
 */
    private void promptBookingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to Pick?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog("Booking...");
                bookNow();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void bookNow() {
        hideProgressDialog();
        Activity.launch(mContext, TripsActivity.class);
    }


    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}

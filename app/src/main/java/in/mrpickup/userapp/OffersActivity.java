package in.mrpickup.userapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import in.mrpickup.userapp.Adapter.OffersAdapter;
import in.mrpickup.userapp.Model.OffersList;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.helper.ErrorHandler;
import retrofit2.Call;
import retrofit2.Response;

public class OffersActivity extends AppCompatActivity implements View.OnClickListener, Runnable, SwipeRefreshLayout.OnRefreshListener {

    private Context mContext;
    private RecyclerView mRecyclerView;
    private List<OffersList> mOffersList;
    private OffersAdapter mAdapter;
    private PreferenceManager mPreference;
    private ProgressDialog mProgressDialog;
    private String mUrl;
    private ImageView mImageBackArrow;
    private SwipeRefreshLayout mRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();


        if (NetworkError.getInstance(mContext).isOnline()) {

            showProgressDialog("Loading...");
            setUrl();
        } else {
            ToastBuilder.build(mContext, " No Internet Connection");
        }

    }


    private void initObjects() {
        mRecyclerView = findViewById(R.id.recyclerview);
        mImageBackArrow = findViewById(R.id.img_back);
        mRefreshLayout = findViewById(R.id.refresh);
        mContext = this;
        mPreference = new PreferenceManager(mContext);
        mProgressDialog = new ProgressDialog(mContext);
        mOffersList = new ArrayList<>();
        mAdapter = new OffersAdapter(mOffersList);


    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.post(this);
        mImageBackArrow.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        if (v == mImageBackArrow) {
            onBackPressed();
        }

    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent);
        mRefreshLayout.post(this);
    }


    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setUrl() {
        mUrl = Api.URL + "rates/offers/";
        getListService();
    }

    private void getListService() {


        ApiInterface categoryService = Api.getClient().create(ApiInterface.class);
        Call<ArrayList<OffersList>> call = categoryService.getListService(mUrl, "Token " + mPreference.getToken());

        call.enqueue(new retrofit2.Callback<ArrayList<OffersList>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<OffersList>> call, @NonNull Response<ArrayList<OffersList>> response) {
                mRefreshLayout.setRefreshing(false);
                List<OffersList> CategoryListResponse = response.body();
                if (response.isSuccessful() && CategoryListResponse != null) {
                    hideProgressDialog();

                    if (CategoryListResponse.size() > 0) {
                        mOffersList.clear();
                        mOffersList.addAll(CategoryListResponse);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        ToastBuilder.build(mContext, "No Offers Yet");
                    }
                } else {
                    mRefreshLayout.setRefreshing(false);

                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<OffersList>> call, @NonNull Throwable t) {
                hideProgressDialog();
                mRefreshLayout.setRefreshing(false);

                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    public void onRefresh() {
        setUrl();
    }

    @Override
    public void run() {

    }
}

package in.mrpickup.userapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.mrpickup.userapp.app.Activity;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.Constant;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;
import in.mrpickup.userapp.helper.MacAddress;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.userapp.app.Api.KEY_USERNAME;

public class SignIn extends AppCompatActivity
        implements View.OnClickListener {

    private Context mContext;
    private Toolbar mToolbar;
    private EditText mEditTextPhone, mEditTextPass;
    private TextView mTextViewForgotPass;
    private Button mButtonSignIn;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        initObjects();
        initCallbacks();
        setupToolbar();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mTextViewForgotPass) {
            Activity.launch(mContext, ResetPassActivity.class);
        } else if (v == mButtonSignIn) {
            processSignIn();
        }
    }

    private void initObjects() {
        mToolbar = findViewById(R.id.toolbar);
        mEditTextPhone = findViewById(R.id.input_phone);
        mEditTextPass = findViewById(R.id.input_pass);
        mTextViewForgotPass = findViewById(R.id.txt_forgot_pass);
        mButtonSignIn = findViewById(R.id.btn_sign_in);

        mContext = this;
        mPreferenceManager = new PreferenceManager(this);
        mProgressDialog = new ProgressDialog(this);
    }

    private void initCallbacks() {
        mTextViewForgotPass.setOnClickListener(this);
        mButtonSignIn.setOnClickListener(this);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Sign In");
        mToolbar.setNavigationIcon(R.drawable.ic_left_arrow_white);
        mToolbar.setTitleTextColor(ContextCompat.getColor(mContext, R.color.shade_white_cc));
    }

    private void processSignIn() {
        String phone = mEditTextPhone.getText().toString().trim();
        String pass = mEditTextPass.getText().toString().trim();
        if (validateInput(phone, pass)) {
            showProgressDialog("Signing in..");

            if (NetworkError.getInstance(this).isOnline()) {
                signInUser(getSignInRequestJson(phone, pass));
            } else {
                ToastBuilder.build(this, " No Internet Connection");
            }
        }
    }

    private JSONObject getSignInRequestJson(String phone, String pass) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_USERNAME, phone);
            jsonObject.put(Api.KEY_PASSWORD, pass);
            jsonObject.put(Api.KEY_CLIENT, MacAddress.getMacAddress());
            jsonObject.put(Api.KEY_ROLE, Constant.ROLE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    //TODO: add password regex validation
    private boolean validateInput(String phone, String pass) {
        if (TextUtils.isEmpty(phone)) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Phone Number"));
            return false;
        } else if (phone.length() < 10) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "Phone Number", 10, "digits"));
            return false;
        } else if (TextUtils.isEmpty(pass)) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Password"));
            return false;
        } else if (pass.length() < 6) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(getString(R.string.error_pass_length));
            return false;
        }
        return true;
    }

    private void signInUser(JSONObject signInRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Api.LOGIN_URL,
                signInRequestJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("responnse", "" + response.toString());
                hideProgressDialog();
                handleSignInResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);

            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "sign_in");
    }

    private void handleSignInResponse(JSONObject response) {
        try {
            String token = response.getString(Api.KEY_TOKEN);
            String pickPhone = response.getString(KEY_USERNAME);
            String email = response.getString(Api.KEY_EMAIL);
            String name = response.getString(Api.KEY_FIRST_NAME);
            mPreferenceManager.setToken(token);
            mPreferenceManager.setPhone(pickPhone);
            mPreferenceManager.setEmail(email);
            mPreferenceManager.setName(name);

            ToastBuilder.build(mContext, "Welcome");
            Activity.launchClearStack(mContext, PickUpActivity.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}

package in.mrpickup.userapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.mrpickup.userapp.Adapter.CompletedImageAdapter;
import in.mrpickup.userapp.Adapter.ProductCompleteListAdapter;
import in.mrpickup.userapp.Model.CompletedBookingItem;
import in.mrpickup.userapp.app.Constant;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.callback.ProductCallback;

public class CompletedOrderDetailsActivity extends AppCompatActivity implements View.OnClickListener, ProductCallback {


    public TextView orderId, pickAddr, dropAddr, date, name, category, delType,
            mStore, mOrderStatus, mMobile, mDelName, mDesc, mdist, mdcost, mProductAmount, mWaitingAmount;

    public String pickMob, dropLoc,
            pickAddr1, otp, name1, date11, Category1, Storename, mOrderStatusType, mDeliveryName,
            Description, mDistance, mdeliveryCost;
    public Context mContext;
    public PreferenceManager mPreference;
    int Oid;
    double mProductCost, mWaitngCost;
    int status;
    ImageView img_back;
    ProductCompleteListAdapter productEditListAdapter;
    ArrayList<CompletedBookingItem> mOrderArray = new ArrayList<>();
    ArrayList<CompletedBookingItem> mOrders = new ArrayList<>();
    CompletedImageAdapter pickupImageAdapter;
    private boolean pickType;
    private LinearLayout mStoreLayouts, mobile_layout, product_layout, descriptionLayout, ProductCostlayout, WaitingCostLayout;
    private RecyclerView mProductsListview, mPickupListView, Deliverylistview;
    private CardView PickIamgecard, delImagecard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compeleted_order_details);
        processBundle();
        initObjects();
        initCallbacks();
    }

    private void initObjects() {

        mContext = this;
        mProductsListview = findViewById(R.id.products_listview);
        mPickupListView = findViewById(R.id.Pickuplistview);
        Deliverylistview = findViewById(R.id.Deliverylistview);
        mProductAmount = findViewById(R.id.order_ProductCost);
        mWaitingAmount = findViewById(R.id.order_WaitingCost);
        ProductCostlayout = findViewById(R.id.productCostLayout);
        WaitingCostLayout = findViewById(R.id.WaitingCostLayout);
        PickIamgecard = findViewById(R.id.cardpickimage);
        delImagecard = findViewById(R.id.deliveryImagecard);
        orderId = findViewById(R.id.order_id);
        orderId = findViewById(R.id.order_id);
        img_back = findViewById(R.id.img_back);
        pickAddr = findViewById(R.id.pickupaddress);
        dropAddr = findViewById(R.id.dropaddress);
        date = findViewById(R.id.created_date);
        name = findViewById(R.id.name);
        category = findViewById(R.id.category);
        delType = findViewById(R.id.type);
        mStore = findViewById(R.id.store);
        mdist = findViewById(R.id.order_distance);
        mdcost = findViewById(R.id.order_deliverycost);
        mOrderStatus = findViewById(R.id.order_status);
        mStoreLayouts = findViewById(R.id.storelayout);
        mobile_layout = findViewById(R.id.mobile_layout);
        mMobile = findViewById(R.id.mobilenumber);
        product_layout = findViewById(R.id.product_layout);
        descriptionLayout = findViewById(R.id.descriptionLayout);
        mDesc = findViewById(R.id.txt_descr);
        mDelName = findViewById(R.id.del_name);
        mPreference = new PreferenceManager(mContext);
        initSetText();
        mProducts();
    }

    private void mProducts() {
        if (mOrders != null) {
            mProductsListview.setVisibility(View.VISIBLE);
            productEditListAdapter = new ProductCompleteListAdapter(mContext, mOrders, this);
            mProductsListview.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true));
            mProductsListview.setHasFixedSize(true);
            mProductsListview.setScrollBarSize(0);
            mProductsListview.setAdapter(productEditListAdapter);
            productEditListAdapter.notifyDataSetChanged();

            PickIamgecard.setVisibility(View.VISIBLE);
            mPickupListView.setVisibility(View.VISIBLE);
            pickupImageAdapter = new CompletedImageAdapter(mContext, mOrders);
            mPickupListView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true));
            mPickupListView.setHasFixedSize(true);
            mPickupListView.setScrollBarSize(0);
            mPickupListView.setAdapter(pickupImageAdapter);
            pickupImageAdapter.notifyDataSetChanged();

            delImagecard.setVisibility(View.VISIBLE);
            Deliverylistview.setVisibility(View.VISIBLE);
            pickupImageAdapter = new CompletedImageAdapter(mContext, mOrders);
            Deliverylistview.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true));
            Deliverylistview.setHasFixedSize(true);
            Deliverylistview.setScrollBarSize(0);
            Deliverylistview.setAdapter(pickupImageAdapter);
            pickupImageAdapter.notifyDataSetChanged();


        }

    }

    @SuppressLint("SetTextI18n")
    private void initSetText() {

        orderId.setText(String.valueOf(Oid));
        pickAddr.setText(pickAddr1);
        dropAddr.setText(dropLoc);
        date.setText(date11);
        name.setText(name1);
        mDelName.setText(mDeliveryName);

        float dist = Float.valueOf(mDistance);
        mdist.setText(String.format("%.1f", dist) + " km");
        mdcost.setText(" \u20b9 " + mdeliveryCost);

        if (pickMob != null) {
            mMobile.setText(pickMob);
            mobile_layout.setVisibility(View.VISIBLE);
        } else {
            mobile_layout.setVisibility(View.GONE);
        }
        if (Category1 != null) {
            category.setText(Category1);
            product_layout.setVisibility(View.VISIBLE);
        } else {
            product_layout.setVisibility(View.GONE);
        }

        if (status == 1) {
            mOrderStatus.setText("Order Placed");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (status == 2) {
            mOrderStatus.setText("Employee Assigned");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (status == 4) {
            mOrderStatus.setText("Completed");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (status == 5) {
            mOrderStatus.setText("Trip End");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (status == 6) {
            mOrderStatus.setText(" User Cancelled");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        } else if (status == 7) {
            mOrderStatus.setText("Trip Started");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (status == 8) {
            mOrderStatus.setText("Employee Rejected");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        } else if (status == 9) {
            mOrderStatus.setText("Pending");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (status == 10) {
            mOrderStatus.setText("Completed");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (status == 11) {
            mOrderStatus.setText("Paid");
            mOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        }
        if (pickType) {
            delType.setText("PickUp & Delivery");
            mStoreLayouts.setVisibility(View.GONE);
        } else {
            delType.setText("Buy & Delivery");

            if (Storename != null && !Storename.isEmpty()) {
                mStoreLayouts.setVisibility(View.VISIBLE);
                mStore.setText(Storename);
            }

        }
        if (!Description.isEmpty() && !Description.equals("null")) {
            mDesc.setText(Description);
            descriptionLayout.setVisibility(View.VISIBLE);
        } else {
            descriptionLayout.setVisibility(View.GONE);
        }
        if (mProductCost > 0) {
            ProductCostlayout.setVisibility(View.VISIBLE);
            mProductAmount.setText(String.valueOf("\u20b9 " + mProductCost));
        }

        if (mWaitngCost > 0) {
            WaitingCostLayout.setVisibility(View.VISIBLE);
            mWaitingAmount.setText(String.valueOf("\u20b9 " + mWaitngCost));
        }
    }

    private void initCallbacks() {
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            onBackPressed();
        }

    }

    private void processBundle() {
        name1 = getIntent().getExtras().getString("Pick_NAME");
        pickAddr1 = getIntent().getExtras().getString("pickaddrs");
        Oid = getIntent().getExtras().getInt("ORDER_ID");
        pickMob = getIntent().getExtras().getString("PICK_MOB");
        dropLoc = getIntent().getExtras().getString("dropaddres");
        status = getIntent().getExtras().getInt("STATUS");
        date11 = getIntent().getExtras().getString("Date");
        mOrderStatusType = getIntent().getExtras().getString("PaymentType");
        pickType = getIntent().getExtras().getBoolean("PICK_TYPE");
        Category1 = getIntent().getExtras().getString("PRODUCT_NAME");
        mDeliveryName = getIntent().getExtras().getString("Del_NAME");
        Storename = getIntent().getExtras().getString("StoreName");
        Description = getIntent().getExtras().getString("Description");
        mDistance = getIntent().getExtras().getString("Approximatedist");
        mdeliveryCost = getIntent().getExtras().getString("deliveryCost");
        mProductCost = getIntent().getExtras().getDouble("productCost");
        mWaitngCost = getIntent().getExtras().getDouble("waitingCost");
        Log.e("Description", "" + Description);
        mOrderArray = getIntent().getParcelableArrayListExtra(Constant.TRIP_ARRAY);
        for (int i = 0; i < mOrderArray.size(); i++) {
            if (Oid == mOrderArray.get(i).getId()) {
                mOrders.add(mOrderArray.get(i));
                Log.e("djfgjdf", "" + mOrders.size());

            }
        }
    }


    @Override
    public void onItemRemoveClick(int position) {


    }


}

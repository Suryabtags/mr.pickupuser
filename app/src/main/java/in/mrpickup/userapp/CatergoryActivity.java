package in.mrpickup.userapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.mrpickup.userapp.Adapter.ImageAdapter;
import in.mrpickup.userapp.Adapter.ProductListAdapter;
import in.mrpickup.userapp.Adapter.RateAdapter;
import in.mrpickup.userapp.Model.Productdata;
import in.mrpickup.userapp.Model.Rates;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.NetworkError;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.callback.ProductCallback;
import in.mrpickup.userapp.helper.ErrorHandler;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.userapp.app.Activity.launch;

public class CatergoryActivity extends AppCompatActivity implements View.OnClickListener, ProductCallback {
    private static final int REQUEST_IMAGE = 4;
    private static final int REQUEST_WRITE_STORAGE = 4;
    private static final int RESULT_PICK_CONTACT = 123;
    private static final int RESULT_PICK_CONTACT1 = 1234;
    private static final int RESULT_CODE = 143;
    public ImageAdapter imageAdapter;
    public Spinner spinnerWeight, spinPaymentType, deliveryType;
    public TextView mTotalCost, addproduct;
    protected TextInputLayout layoutContact, layoutName,
            layoutDate, layoutTime, pickNamelayout, pickContactlayout, layoutdeliveyname;
    protected RecyclerView recyclerView;
    protected String mDistanceCost;
    LinearLayout mTotalCostLayout;
    ArrayList<HashMap<String, String>> Category = new ArrayList<>();
    String catid = "";
    String productId = "";
    int categorysize;
    ProductListAdapter productListAdapter;
    String mPaymentoption, mDeliverType;
    ArrayList<Productdata> myList;
    double slat, slng, dlat, dlng;
    String DistanceKm;
    String extrakmcharges, maxKm;
    private Toolbar mToolbar;
    private Context mContext;
    private ImageView mImageViewMedia, mContact, mPickContactimg;
    private Button mPickNow;
    private PreferenceManager mPreferenceManager;
    private LinearLayout mLayoutMediaContainer;
    private TextInputEditText mInputContact, mInputName,
            mInputDate, mInputTime, mPickContact, mPickname;
    private EditText mInputDesc;
    private ProgressDialog mProgressDialog;
    private List<File> mImageArrayList;
    private String name, contact, desc, mDeliveryname, mdeliverycontact;
    private String timeh, dateh;
    private RecyclerView mProductsListview;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catergory);
        initObjects();
        initCallBack();
        setupToolbar();
        addListenerOnSpinnerItemSelection();
    }

    private void initObjects() {
        mPickNow = findViewById(R.id.btn_pick_now);
        mImageViewMedia = findViewById(R.id.img_media);
        recyclerView = findViewById(R.id.recylerview);
        mToolbar = findViewById(R.id.toolbar);
        mTotalCost = findViewById(R.id.txt_totalcost);
        mLayoutMediaContainer = findViewById(R.id.media_container);
        mContext = this;
        mImageArrayList = new ArrayList<>();
        mProgressDialog = new ProgressDialog(mContext);
        mContact = findViewById(R.id.contact);
        mPickContactimg = findViewById(R.id.pick_contact);
        mTotalCostLayout = findViewById(R.id.totalcostlayout);
        layoutdeliveyname = findViewById(R.id.dname);

        slat = getIntent().getExtras().getDouble("SOURCE_LAT");
        slng = getIntent().getExtras().getDouble("SOURCE_LNG");
        dlat = getIntent().getExtras().getDouble("DESTINATION_LAT");
        dlng = getIntent().getExtras().getDouble("DESTINATION_LNG");
        DistanceKm = getIntent().getExtras().getString("DistanceKm");
        Log.e("DistanceKm", "" + DistanceKm);
        mInputName = findViewById(R.id.input_name);
        mInputContact = findViewById(R.id.input_contact_number);

        mInputDesc = findViewById(R.id.input_description);
        layoutDate = findViewById(R.id.layout_date);
        layoutTime = findViewById(R.id.layout_time);
        pickContactlayout = findViewById(R.id.pickup_number);
        pickNamelayout = findViewById(R.id.pick_name);
        mInputDate = findViewById(R.id.input_date);
        mInputDate.setText(getCurrentDate());
        mInputTime = findViewById(R.id.input_time);
        addproduct = findViewById(R.id.addproduct);
        mProductsListview = findViewById(R.id.products_listview);
        mInputTime.setText(getCurrentTime());
        layoutContact = findViewById(R.id.contact_number);
        mPreferenceManager = new PreferenceManager(this);
        mPickname = findViewById(R.id.input_pick_name);
        mPickname.setText(mPreferenceManager.getName());
        mPickContact = findViewById(R.id.input_pickup_number);
        mPickContact.setText(mPreferenceManager.getPhone());
        mInputContact.setText(mPreferenceManager.getPhone());

        mPickContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mInputContact.setText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mInputName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 3) {
                    layoutdeliveyname.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });





    }

    private void initCallBack() {
        mPickNow.setOnClickListener(this);
        mImageViewMedia.setOnClickListener(this);
        mContact.setOnClickListener(this);
        mPickContactimg.setOnClickListener(this);
        mInputDate.setOnClickListener(this);
        mInputTime.setOnClickListener(this);
        addproduct.setOnClickListener(this);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Pickup & Delivery");
        mToolbar.setNavigationIcon(R.drawable.ic_left_arrow_white);
        mToolbar.setTitleTextColor(ContextCompat.getColor(mContext, R.color.shade_white_cc));
    }

    @Override
    public void onClick(View v) {
        if (v == mPickNow) {
            disableError();

            if (NetworkError.getInstance(mContext).isOnline()) {
                processOrder();
            } else {
                ToastBuilder.build(mContext, " No Internet Connection");
            }


        } else if (v == mImageViewMedia) {
            processPickImages();
        } else if (v == mContact) {
            pickContact(RESULT_PICK_CONTACT);

        } else if (v == mPickContactimg) {
            pickContact(RESULT_PICK_CONTACT1);

        } else if (v == mInputDate) {
            promptDatePickerDialog(mInputDate);
        } else if (v == mInputTime) {
            promptTimePickerDialog(mInputTime);
        } else if (v == addproduct) {
            if (NetworkError.getInstance(mContext).isOnline()) {
                Intent intent = new Intent(getBaseContext(), AddpickupproductActivity.class);
                intent.putExtra("SOURCE_LAT", slat);
                intent.putExtra("DESTINATION_LAT", dlat);
                intent.putExtra("SOURCE_LNG", slng);
                intent.putExtra("DESTINATION_LNG", dlng);
                intent.putExtra("DistanceKm", DistanceKm);
                startActivityForResult(intent, RESULT_CODE);
            } else {
                ToastBuilder.build(mContext, " No Internet Connection");
            }

        }
    }


    private void mProducts(ArrayList<Productdata> data) {

        myList = new ArrayList<>();
        myList = data;
        Log.e("myList", "" + myList.size());
        if (myList != null) {
            if (myList.size() > 0) {
                mProductsListview.setVisibility(View.VISIBLE);
                productListAdapter = new ProductListAdapter(mContext, myList, this);
                mProductsListview.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true));
                mProductsListview.setHasFixedSize(true);
                mProductsListview.setScrollBarSize(0);
                mProductsListview.setAdapter(productListAdapter);
                productListAdapter.notifyDataSetChanged();
            }
        }

    }

    @Override
    public void onItemRemoveClick(int position) {
        myList.remove(position);
        productListAdapter.notifyItemRemoved(position);
        productListAdapter.notifyItemRangeChanged(position, myList.size());
        if (myList.size() == 0) {
            mProductsListview.setVisibility(View.GONE);
        } else {
            mProductsListview.setVisibility(View.VISIBLE);
        }
    }

    private void disableError() {
        layoutContact.setErrorEnabled(false);
        layoutDate.setErrorEnabled(false);
        layoutTime.setErrorEnabled(false);

    }

    private void promptDatePickerDialog(TextInputEditText editDate) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        openDateDialog(editDate, year, month, dayOfMonth);
    }

    private void promptTimePickerDialog(TextInputEditText editTextDuration) {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY) + 1;
        int minute = calendar.get(Calendar.MINUTE);
        openTimeDialog(editTextDuration, hourOfDay, minute);


    }

    private String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        dateh = getDate(year, month + 1, dayOfMonth);
        return getDate(year, month + 1, dayOfMonth);
    }

    private String getDate(int year, int month, int dayOfMonth) {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (dayOfMonth < 10 ? "0"
                + dayOfMonth : dayOfMonth);
    }

    private String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY) + 1;
        int minute = calendar.get(Calendar.MINUTE);
        timeh = getTime(hourOfDay, minute);
        return changeTo12hr(hourOfDay, minute);
    }

    private String getTime(int hourOfDay, int minute) {
        return (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute
                : minute);
    }

    public String changeTo12hr(int hourOfDay, int minute) {
        return (hourOfDay > 12 ? hourOfDay - 12 : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute) + (hourOfDay > 12 ? "pm" : "am");

    }

    private void openTimeDialog(final TextInputEditText mInputTime, int hourOfDay, int minute) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(mContext, R.style.DatePickerTheme,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        timeh = getTime(hourOfDay, minute);
                        mInputTime.setText((changeTo12hr(hourOfDay, minute)));
                    }
                }, hourOfDay, minute, false);
        timePickerDialog.show();

    }

    private void openDateDialog(final TextInputEditText editDate, int year, int month,
                                int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.DatePickerTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        dateh = getDate(year, month + 1, dayOfMonth);
                        Log.e("detset", "" + dateh);
                        editDate.setText(getDate(year, month + 1, dayOfMonth));
                    }
                }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void processOrder() {
        name = mPickname.getText().toString().trim();
        contact = mPickContact.getText().toString().trim();
        mDeliveryname = mInputName.getText().toString().trim();
        mdeliverycontact = mInputContact.getText().toString().trim();
        desc = mInputDesc.getText().toString().trim();
        if (validateInput(name, contact, mDeliveryname, desc)) {
            if (myList != null) {
                promptBookingDialog();
            } else {
                ToastBuilder.build(mContext, "Please Add the Products ");
            }
        }
    }

    private Boolean validateInput(String name, String number, String mDeliveryname, String desc) {

        if (TextUtils.isEmpty(name)) {
            mInputName.requestFocus();
            layoutName.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Contact Name"));
            return false;
        } else if (TextUtils.isEmpty(number)) {
            mInputContact.requestFocus();
            layoutContact.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Contact Number"));
            return false;
        } else if (number.length() < 10) {
            mInputContact.requestFocus();
            layoutContact.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "Contact Number", 10, "digits"));
            return false;
        } else if (TextUtils.isEmpty(mDeliveryname)) {
            mInputName.requestFocus();
            layoutdeliveyname.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Delivery Name"));
            return false;
        }

        return true;

    }

    private void promptBookingDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to Book?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog("Booking...");
                try {
                    getOrderRequest(name, contact, mDeliveryname, mdeliverycontact, desc);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getOrderRequest(String name, String contact, String delname, String delcontact, String desc) throws JSONException {
        String dateT = dateh + " " + timeh + ":00";


        ArrayList<Part> partArrayList = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();

        for (int j = 0; j < myList.size(); j++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", myList.get(j).getSubdata());
            jsonObject.put("category_id", myList.get(j).getmId());
            jsonObject.put("quantity", myList.get(j).getmQuantity());
            jsonObject.put("units_id", myList.get(j).getmUnitId1());
            jsonArray.put(jsonObject);

        }
        Log.e("contact", "" + contact);
        partArrayList.add(new StringPart(Api.KEY_CATEGORY, productId));
        partArrayList.add(new StringPart(Api.KEY_PRODUCT_WEIGHT, catid));
        partArrayList.add(new StringPart(Api.KEY_PRODUCT, jsonArray.toString()));
        partArrayList.add(new StringPart(Api.KEY_DESC, desc));
        partArrayList.add(new StringPart(Api.KEY_PICKUP_TYPE, "1"));
        partArrayList.add(new StringPart(Api.KEY_PICKUP_MOB, contact));
        partArrayList.add(new StringPart(Api.KEY_PICKUP_NAME, name));
        partArrayList.add(new StringPart(Api.KEY_PICKLAT, String.valueOf(slat)));
        partArrayList.add(new StringPart(Api.KEY_PICKLONG, String.valueOf(slng)));
        partArrayList.add(new StringPart(Api.KEY_DELIVERY_NAME, delname));
        partArrayList.add(new StringPart(Api.KEY_DELIVER_LAT, String.valueOf(dlat)));
        partArrayList.add(new StringPart(Api.KEY_DELIVER_LONG, String.valueOf(dlng)));
        partArrayList.add(new StringPart(Api.KEY_DELIVER_MOB, delcontact));
        partArrayList.add(new StringPart(Api.KEY_PAYMENTTYPE, mPaymentoption));
        partArrayList.add(new StringPart(Api.KEY_DELIVERYTYPE, mDeliverType));
        partArrayList.add(new StringPart(Api.KEY_PICKUP_TIME, dateT));

        for (int i = 0; i < mImageArrayList.size(); i++) {
            partArrayList.add(new FilePart(Api.KEY_IMAGE, new File(mImageArrayList.get(i).getPath())));
        }


        Log.e("category", "" + productId);
        Log.e("product_weight", "" + catid);
        Log.e("product", "" + jsonArray.toString());
        Log.e("description", "" + desc);
        Log.e("type", "" + String.valueOf(1));
        Log.e("pickup_mob", "" + contact);
        Log.e("pickup_name", "" + name);
        Log.e("pickup_time", "" + dateT);
        Log.e("pickup_lat", "" + String.valueOf(slat));
        Log.e("pickup_long", "" + String.valueOf(slng));
        Log.e("del_name", "" + delname);
        Log.e("del_lat", "" + String.valueOf(dlat));
        Log.e("del_long", "" + String.valueOf(dlng));
        Log.e("del_mob", "" + delcontact);

        Log.e("image", "" + mImageArrayList.size());
        Ion.with(mContext)
                .load(Api.CREATE_ORDER)
                .addHeader("Authorization", "Token " + mPreferenceManager.getToken())

                .addMultipartParts(partArrayList)
                .asJsonObject()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<JsonObject>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<JsonObject> result) {
                        hideProgressDialog();
                        if (e == null) {
                            int code = result.getHeaders().code();

                            Log.e("ewewew", "" + code);
                            if (code == 200 || code == 201) {
                                ToastBuilder.build(mContext, "Booked Order");
                                launch(mContext, TripsActivity.class);
                                onBackPressed();
                            } else {
                                Log.e("result122", "" + result.getHeaders().message());


                            }
                        } else {

                            ToastBuilder.build(mContext, e.getMessage());

                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addItemsOnSpinner2() {
        List<String> list = new ArrayList<>();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    public void addListenerOnSpinnerItemSelection() {
        spinnerWeight = findViewById(R.id.spinWeight);
        spinPaymentType = findViewById(R.id.spinPaymentType);
        deliveryType = findViewById(R.id.deliveryType);

        spinnerWeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                TextView selectedText = (TextView) adapterView.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    selectedText.setTextSize(14);
                }
                mTotalCostLayout.setVisibility(View.VISIBLE);

                catid = Category.get(i).get("catid");
                mDistanceCost = Category.get(i).get("cost");
                extrakmcharges = Category.get(i).get("extra_km");
                maxKm = Category.get(i).get("maxkm");
                float km = Float.valueOf(DistanceKm);
                float extraamount = Float.valueOf(extrakmcharges);
                float cost = Float.valueOf(mDistanceCost);

                float maxkm1 = Float.valueOf(maxKm);
                if (km > maxkm1) {
                    Log.e("km", "" + km);
                    float TotalKilometer = km - maxkm1;
                    float total = TotalKilometer * extraamount + cost;
                    String s = String.format("%.2f", total);
                    mTotalCost.setText("\u20b9 " + s);
                } else {

                    mTotalCost.setText("\u20b9 " + mDistanceCost);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext,
                R.array.paymenttype, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPaymentType.setAdapter(adapter);

        spinPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                TextView selectedText = (TextView) arg0.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    selectedText.setTextSize(14);
                }

                if (position == 0) {
                    mPaymentoption = "CD";
                } else {
                    mPaymentoption = "ON";
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(mContext,
                R.array.deliverytype, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deliveryType.setAdapter(adapter1);

        deliveryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                //    ((TextView) deliveryType.getSelectedView()).setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
                TextView selectedText = (TextView) arg0.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    selectedText.setTextSize(14);
                }
                if (position == 0) {
                    mDeliverType = "OR";
                } else {
                    mDeliverType = "UR";
                }

                if (NetworkError.getInstance(mContext).isOnline()) {
                    getRates(mDeliverType);
                } else {
                    ToastBuilder.build(mContext, " No Internet Connection");
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
                case RESULT_PICK_CONTACT1:
                    contactPicked1(data);
                    break;
            }
        } else if (requestCode == RESULT_CODE) {
            if (data != null) {
                mProducts(data.<Productdata>getParcelableArrayListExtra("ProductsLits"));
            }
        }
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source,
                                       int type) {
                mImageArrayList.addAll(imageFiles);
                showMedia();
            }
        });
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
            mInputContact.setText(phoneNo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void contactPicked1(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
            mPickContact.setText(phoneNo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void pickContact(int resultPickContact) {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, resultPickContact);

    }

    private boolean hasWriteStoragePermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestWriteStoragePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
    }

    private void showMedia() {
        imageAdapter = new ImageAdapter(this, mImageArrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(imageAdapter);
    }

    private void processPickImages() {
        if (hasWriteStoragePermission()) {
            pickImages();
        } else {
            requestWriteStoragePermission();
        }
    }

    private void pickImages() {
        EasyImage.configuration(mContext)
                .setImagesFolderName(getString(R.string.app_name))
                .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(this, "Select Images", REQUEST_IMAGE);
    }

    @SuppressLint("InflateParams")
    private void displayImage(String path) {
        View mediaView = LayoutInflater.from(mContext).inflate(R.layout.dialog_media, null);

        ImageView imageViewGrievance = mediaView.findViewById(R.id.img_media);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setView(mediaView);
        alertDialog.show();

        Glide.with(this).load(path).into(imageViewGrievance);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void getRates(final String mdeliverytype) {
        ApiInterface apiInterface = Api.getClient().create(ApiInterface.class);
        Call<ArrayList<Rates>> call = apiInterface.getRates("Token " + mPreferenceManager.getToken());
        call.enqueue(new retrofit2.Callback<ArrayList<Rates>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<Rates>> call, @NonNull Response<ArrayList<Rates>> response) {
                List<Rates> rates = response.body();
                ArrayList<Rates> mrates = new ArrayList<>();
                if (response.isSuccessful() && rates != null) {
                    mrates.clear();
                    for (int i = 0; i < rates.size(); i++) {
                        if (rates.get(i).getmDeliveryType().equals(mdeliverytype)) {
                            mrates.add(rates.get(i));
                            Category.clear();
                            for (int j = 0; j < mrates.size(); j++) {
                                HashMap<String, String> categorymap = new HashMap<>();
                                categorymap.put("catid", String.valueOf(mrates.get(j).getmId()));
                                String fromweight = String.valueOf(mrates.get(j).getmFromWeight()) + " kg";
                                String toweight = String.valueOf(mrates.get(j).getmToWeight()) + " kg";
                                categorymap.put("catname", fromweight + "-" + toweight);
                                categorymap.put("maxkm", String.valueOf(mrates.get(j).getmMaximumKilometer()));
                                categorymap.put("extra_km", String.valueOf(mrates.get(j).getmExtraKm()));
                                categorymap.put("cost", String.valueOf(mrates.get(j).getmDistanceCost()));
                                Category.add(categorymap);
                            }

                        }
                    }
                    spinnerWeight.setAdapter(new RateAdapter(mContext, Category));

                } else {
                    try {
                        Log.e("fdfdfdfd", "" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Rates>> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }


}



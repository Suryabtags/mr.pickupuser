package in.mrpickup.userapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.mrpickup.userapp.Adapter.ProductAdapter;
import in.mrpickup.userapp.Adapter.UnitsAdapter;
import in.mrpickup.userapp.Model.EditProduct;
import in.mrpickup.userapp.Model.Productdata;
import in.mrpickup.userapp.Model.Products;
import in.mrpickup.userapp.app.Api;
import in.mrpickup.userapp.app.ApiInterface;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.helper.ErrorHandler;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddEditProductActivity extends AppCompatActivity implements View.OnClickListener {
    EditText add_products, quantity;
    ArrayList<Productdata> myList;
    ArrayList<HashMap<String, String>> ListProducts = new ArrayList<>();
    ArrayList<HashMap<String, String>> ListUnits = new ArrayList<>();
    TextView mBtn_Add, mSave;
    int productListsize;
    int mUnitsize;
    String mUnitId, unitName, product, mQuantity, subcat, orderid, productId1;
    String productId = "";
    String productname = "";
    TextView mTextProductLabel;
    ImageView mImageClose;
    String ProductName, ProductQuantity, ProductCategoryId, ProductUnitsID;
    private Spinner spinner1, spinner2;
    private Context mContext;
    private PreferenceManager MyPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editproduct);

        product = getIntent().getExtras().getString("mCategory");
        subcat = getIntent().getExtras().getString("mSubCategory");
        mQuantity = getIntent().getExtras().getString("mQuantity");
        orderid = getIntent().getExtras().getString("id");
        productId1 = getIntent().getExtras().getString("productId");
        initObjects();
        initCallbacks();


    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initCallbacks() {
        mImageClose.setOnClickListener(this);
        mBtn_Add.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mImageClose) {
            onBackPressed();
        } else if (v == mBtn_Add) {

            ProductName = add_products.getText().toString();
            ProductQuantity = quantity.getText().toString();
            setUrl();
        }

    }

    private void initObjects() {
        mContext = this;
        spinner1 = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        mBtn_Add = findViewById(R.id.Apply_btn);
        mTextProductLabel = findViewById(R.id.txt_productlist);
        add_products = findViewById(R.id.add_products);
        quantity = findViewById(R.id.quantity);
        mSave = findViewById(R.id.img_save);
        mImageClose = findViewById(R.id.img_close);
        MyPreference = new PreferenceManager(mContext);
        add_products.setText(subcat);
        quantity.setText(mQuantity);
        getProducts();
        getUnits();
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                TextView selectedText = (TextView) adapterView.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                }
                productId = ListProducts.get(i).get("productid");
                productname = ListProducts.get(i).get("productname");
                ProductCategoryId = productId;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                mUnitId = ListUnits.get(i).get("unitsId");
                unitName = ListUnits.get(i).get("unitsName");
                ProductUnitsID = mUnitId;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void getProducts() {
        ApiInterface apiInterface = Api.getClient().create(ApiInterface.class);
        Call<ArrayList<Products>> call = apiInterface.getProductList();
        call.enqueue(new retrofit2.Callback<ArrayList<Products>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<Products>> call, @NonNull Response<ArrayList<Products>> response) {
                List<Products> products = response.body();
                if (response.isSuccessful() && products != null) {
                    productListsize = products.size();
                    Log.e("products", "" + response.toString());
                    ListProducts.clear();
                    for (int i = 0; i < productListsize; i++) {
                        HashMap<String, String> categorymap = new HashMap<>();
                        categorymap.put("productid", String.valueOf(products.get(i).getmId()));
                        categorymap.put("productname", products.get(i).getmName());
                        ListProducts.add(categorymap);
                    }
                    spinner1.setAdapter(new ProductAdapter(mContext, ListProducts));

                } else {

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Products>> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void getUnits() {
        ApiInterface apiInterface = Api.getClient().create(ApiInterface.class);
        Call<ArrayList<Products>> call = apiInterface.getUnits();
        call.enqueue(new retrofit2.Callback<ArrayList<Products>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<Products>> call, @NonNull Response<ArrayList<Products>> response) {
                List<Products> products = response.body();
                if (response.isSuccessful() && products != null) {
                    mUnitsize = products.size();

                    ListUnits.clear();
                    for (int i = 0; i < mUnitsize; i++) {
                        HashMap<String, String> categorymap = new HashMap<>();
                        categorymap.put("unitsId", String.valueOf(products.get(i).getmId()));
                        categorymap.put("unitsName", products.get(i).getmName());
                        ListUnits.add(categorymap);
                    }
                    spinner2.setAdapter(new UnitsAdapter(mContext, ListUnits));

                } else {

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Products>> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void setUrl() {
        String mUrl = Api.URL + "/orders/" + orderid + "/edit_product/";

        JSONArray jsonArray = new JSONArray();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", ProductName);
            jsonObject.put("category_id", ProductCategoryId);
            jsonObject.put("quantity", ProductQuantity);
            jsonObject.put("units_id", ProductUnitsID);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void mEditProducts(String mUrl, EditProduct editProduct) {
        ApiInterface apiInterface = Api.getClient().create(ApiInterface.class);
        Call<EditProduct> call = apiInterface.UpdateProducts("Token " + MyPreference.getToken(), mUrl, editProduct);
        call.enqueue(new retrofit2.Callback<EditProduct>() {

            @Override
            public void onResponse(@NonNull Call<EditProduct> call, @NonNull Response<EditProduct> response) {
                EditProduct editProduct = response.body();
                if (response.isSuccessful() && editProduct != null) {

                } else {

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EditProduct> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }


}

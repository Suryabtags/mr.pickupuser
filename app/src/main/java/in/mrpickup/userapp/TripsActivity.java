package in.mrpickup.userapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.mrpickup.userapp.Adapter.MyPagerAdapter;
import in.mrpickup.userapp.Model.PaymentSuccessEvent;
import in.mrpickup.userapp.app.AppController;
import in.mrpickup.userapp.app.PreferenceManager;
import in.mrpickup.userapp.app.ToastBuilder;
import in.mrpickup.userapp.app.VolleyErrorHandler;
import in.mrpickup.userapp.fragment.ActiveTripFragment;
import in.mrpickup.userapp.fragment.CompletedTripFragment;
import in.mrpickup.userapp.fragment.PendingFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.mrpickup.userapp.app.Api.KEY_RAZORPAY_ORDER_ID;
import static in.mrpickup.userapp.app.Api.KEY_RAZORPAY_PAYMENT_ID;
import static in.mrpickup.userapp.app.Api.KEY_RAZORPAY_SIGNATURE;
import static in.mrpickup.userapp.app.Api.PAYMENT_SUCCESS;


public class TripsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener,
        PaymentResultWithDataListener, View.OnClickListener {

    private Context mContext;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private List<Fragment> mFragmentList;
    private MyPagerAdapter mPagerAdapter;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;
    private FloatingActionButton mFabCall;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips);
        initObjects();
        setupToolbar();
        initCallbacks();
        populateTabs();
        changeTabsFont();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        String orderId = paymentData.getOrderId();
        String paymentId = paymentData.getPaymentId();
        String signature = paymentData.getSignature();
        showProgressDialog("Processing payment..");
        paymentSuccess(orderId, paymentId, signature);
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        ToastBuilder.build(mContext, s);
    }


    private void initObjects() {
        mToolbar = findViewById(R.id.toolbar);
        mTabLayout = findViewById(R.id.tab_layout);
        mViewPager = findViewById(R.id.pager_trips);
        //  mFabCall = findViewById(R.id.fab);

        mContext = this;
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragmentList);
        mPreferenceManager = new PreferenceManager(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_left_arrow);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Trips");
    }

    private void initCallbacks() {
        mTabLayout.addOnTabSelectedListener(this);
        //   mFabCall.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

//        if (v == mFabCall) {
//            String phone = "9715718698";
//            Activity.launchDialPad(mContext, phone);
//        }

    }

    private void populateTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Pending"));
        mFragmentList.add(new PendingFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("Active"));
        mFragmentList.add(new ActiveTripFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("Completed"));
        mFragmentList.add(new CompletedTripFragment());
        mPagerAdapter.notifyDataSetChanged();

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(
                            Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf"),
                            Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(
                            getResources().getDimension(R.dimen.small_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    private void paymentSuccess(String orderId, String paymentId, String signature) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_RAZORPAY_ORDER_ID, orderId);
            jsonObject.put(KEY_RAZORPAY_PAYMENT_ID, paymentId);
            jsonObject.put(KEY_RAZORPAY_SIGNATURE, signature);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                PAYMENT_SUCCESS, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();

                EventBus.getDefault().post(new PaymentSuccessEvent("Paid successfully"));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Token " + mPreferenceManager.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "payment_success");
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}

